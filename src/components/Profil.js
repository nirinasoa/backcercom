import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import  { encrypt , decrypt } from 'react-crypt-gsm';
import {Link} from 'react-router-dom';

export default class Profil extends Component {
  constructor(props) {
    super(props)
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangenewpassword=this.onChangenewpassword.bind(this)
    this.onChangeoldpassword=this.onChangeoldpassword.bind(this)
    
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangecontact=this.onChangecontact.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.state={
      idAdmin:sessionStorage.getItem("idAdmin"),
      nom:'',
      prenom:'',
      email:'',
      contact:'',
      photo:'',
      file:null,
      filename:null,
      newpassword:'',
      oldpassword:'',
      password:'',
      okpassword:false,
      messagepassword:''
      

    }
  }
  onChangeoldpassword(e){
    const user={
      email:this.state.email,
      motdepasse:e.target.value

    }
    this.setState({
     
      oldpassword:e.target.value

    });
    Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/login.php',user)
    .then(response=>{
      if(response.data.message=='Successful login.'){
        console.log(response.data.message)
        this.setState({
          okpassword:true,
          messagepassword:''

        });
      }
      else{
       console.log('ato')
        this.setState({
          okpassword:false,
          newpassword:'',
          messagepassword:'->Mot de passe incorrect'
        });
      }
    })
    .catch(error => {
      this.setState({
        okpassword:false,
        newpassword:'',
        messagepassword:'->Mot de passe incorrect'
      });
  });
  
  }
  onChangenewpassword(e){
    this.setState({
      newpassword:e.target.value
    });
  }
  onChangenom(e){
    this.setState({
      nom:e.target.value
    });
  }
  onChangeprenom(e){
    this.setState({
      prenom:e.target.value
    });
  }
  onChangeemail(e){
    this.setState({
      email:e.target.value
    });
  }
  onChangecontact(e){
    this.setState({
      contact:e.target.value
    });
  }
  componentDidMount(){
   
    Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/single_read.php/?idAdmin='+this.state.idAdmin)
    .then(response=>{
    this.setState({
      nom:response.data.nom,
      prenom:response.data.prenom,
      email:response.data.email,
      contact:response.data.contact,
      photo:response.data.photo,
      password:response.data.motdepasse
    })

    })
  }
  UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
  async uploadFile(file){
    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      data.append("public_id", originalfilename[0]);
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
    
  }
  onChangeFile(e) {
    this.setState({file:e.target.files[0]})
    this.setState({filename:e.target.files[0].name})
  }
  onSubmit(e){
    e.preventDefault()
 
    if(this.state.newpassword==''){
     // alert('Aucun changement de password')-----------
        if(this.state.filename==null){
          const userAdmin={
            idAdmin:this.state.idAdmin,
            nom:this.state.nom,
            prenom:this.state.prenom,
            email:this.state.email,
            contact:this.state.contact,
            photo:this.state.photo,
            motdepasse:this.state.password
          }
          console.log(userAdmin)
          Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/update.php',userAdmin)
          .then(res=>console.log(res.data));
          alert('L\'utilisateur Admin a été modifié avec succès! Veuillez vous re-connecter pour la mise à jour.')
        }
        else{
          let res =  this.uploadFile(this.state.file);
          const userAdmin={
            idAdmin:this.state.idAdmin,
            nom:this.state.nom,
            prenom:this.state.prenom,
            email:this.state.email,
            contact:this.state.contact,
            photo:this.state.filename,
            motdepasse:this.state.password
      
          }
          console.log(userAdmin)
        Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/update.php',userAdmin)
          .then(res=>console.log(res.data));
          alert('utilisateur Admin a été modifié avec succès!')
          // window.location.href='/dadhboard'
        }
     //fin-----------------------------------
    }
    if(this.state.newpassword!=''){
      //alert('oldpassword='+this.state.oldpassword+'_'+"newpassword="+this.state.newpassword)
      if(this.state.filename==null){
        const userAdmin={
          idAdmin:this.state.idAdmin,
          nom:this.state.nom,
          prenom:this.state.prenom,
          email:this.state.email,
          contact:this.state.contact,
          photo:this.state.photo,
          motdepasse:this.state.newpassword
        }
        console.log(userAdmin)
        Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/update.php',userAdmin)
        .then(res=>console.log(res.data));
        alert('utilisateur Admin a été modifié avec succès!')
      }
      else{
        let res =  this.uploadFile(this.state.file);
        const userAdmin={
          idAdmin:this.state.idAdmin,
          nom:this.state.nom,
          prenom:this.state.prenom,
          email:this.state.email,
          contact:this.state.contact,
          photo:this.state.filename,
          motdepasse:this.state.newpassword
    
        }
        console.log(userAdmin)
      Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/update.php',userAdmin)
        .then(res=>console.log(res.data));
        alert('utilisateur Admin a été modifié avec succès!')
        // window.location.href='/dadhboard'
      }
    }
  
   /* 
  */
  }
  render(){
    let inputnewpassword;
    if(this.state.okpassword==true){
      inputnewpassword=<div><label>Entrer le nouveau mot de passe</label>
             <input required type="password" className="form-control" id="numero"  value={this.state.newpassword} onChange={this.onChangenewpassword}/>
        </div>
    }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">

      <h1>
        Mon profil
       
      </h1>
      <ol className="breadcrumb">
        <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
      </ol>
    </section>

   
    <section className="content">
      <div className="row">
        
        <div className="col-md-4">
       
            <div className="box box-warning" >
                <h4 style={{"textAlign":"center"}}>{sessionStorage.getItem("nomAdmin")}&nbsp;{sessionStorage.getItem("prenom")}</h4><hr/>
              
                   <div style={{marginLeft:'10%'}}>
                     <Avatar  style={{width:120,height:120}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${sessionStorage.getItem("photo")}`}  />
                
                  &nbsp;<label>Nom:</label> {sessionStorage.getItem("nomAdmin")}<br/>
                                &nbsp;<label>Prénom(s):</label> {sessionStorage.getItem("prenom")}<br/>
                                &nbsp;<label>Email:</label> {sessionStorage.getItem("email")}<br/>
                                &nbsp;<label>Numéro:</label> {sessionStorage.getItem("contact")}<br/>
                                &nbsp;<label>Date d'entrée:</label> {sessionStorage.getItem("created")}
                      
                <br/>   <br/>
                </div>
             
                          
                     
                            
                <div className="box box-warning"></div>
            </div>
           
        </div>
        <div className="col-md-8">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Modifier mon profil:</h3>
            </div>
            
            <div className="box-body">
                <form onSubmit={this.onSubmit}>
                <div className="form-group">
                      <label>Nom</label>
                      <input type="text" className="form-control" id="nom"  value={this.state.nom} onChange={this.onChangenom}/>
                </div>
                <div className="form-group">
                      <label>Prénom(s)</label>
                      <input type="text" className="form-control" id="prenom"  value={this.state.prenom} onChange={this.onChangeprenom}/>
                </div>
                <div className="form-group">
                      <label>Email:</label>
                      <input type="email" className="form-control" id="email"  value={this.state.email} onChange={this.onChangeemail}/>
                </div>
                <div className="form-group">
                      <label>Numéro</label>
                      <input type="text" className="form-control" id="numero"  value={this.state.contact} onChange={this.onChangecontact}/>
                </div>
                <div className="form-group" >
                      <label>Changer mon mot de passe</label>
                      <span style={{color:'#cf4040',"fontFamily":"Arial"}}>{this.state.messagepassword}</span>
                      <input required placeholder="Tapez ici l'ancien mot de passe"  type="password" className="form-control" id="numero"  value={this.state.oldpassword} onChange={this.onChangeoldpassword}/>
                      {inputnewpassword}
                </div>
                <div className="form-group">
                      <label for="exampleInputFile">Modifier mon photo de profil</label>
                    <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>

                    </div>
                <div class="box-footer">
                    
                    <button type="submit" class="btn btn-success">Enregistrer la modification</button>
                  </div>
            </form>
             
            </div>
          </div>
        </div>


      </div>
    </section>
  </div>
   
  );
}
}
