import React,{Component} from 'react';
import axios from 'axios';

export default class Login extends Component{
  constructor(props){
    super(props)
    this.onChangemotdepasse=this.onChangemotdepasse.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    let loggedIn= false;
    this.state={
      email:'',
      motdepasse:'',
      loggedIn,
      erreur:'',
      
  }
    this.onSubmit=this.onSubmit.bind(this)

}
componentDidMount(){
  if(localStorage.getItem("loggedIn")==="true"){
      this.setState({
          loggedIn:true
      })
  }
}
onChangemotdepasse(e){
  this.setState({
    motdepasse:e.target.value
  });
}
onChangeemail(e){
  this.setState({
    email:e.target.value
  });
  //window.sessionStorage.setItem("numeroId",e.target.value );
}
  onSubmit(e){
    e.preventDefault();
    const data={
      email:this.state.email,
      motdepasse:this.state.motdepasse
      

     }
    console.log(data)
    axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/login.php',data)
    .then(res=> {
      if(res.data.message=="Successful login."){
        console.log("prenom=",res.data.prenom);
        console.log("pht=",res.data.photo);
        sessionStorage.setItem("loggedIn","true");
        sessionStorage.setItem("jwt",res.data.jwt);
       sessionStorage.setItem("nomAdmin",res.data.nom);
       sessionStorage.setItem("idAdmin",res.data.idAdmin);
        sessionStorage.setItem("prenom",res.data.prenom);
        sessionStorage.setItem("email",res.data.email);
       sessionStorage.setItem("photo",res.data.photo);
       sessionStorage.setItem("created",res.data.created);
       sessionStorage.setItem("contact",res.data.contact);
       window.location="/dashboard"
      }
      else{
        this.setState({erreur:'Erreur de mot de passe ou identifiant invalide'})
      }
     
    
    })
    .catch(error => {
      this.setState({erreur:'Erreur de mot de passe ou identifiant invalide'})
      console.log(error);
  });
   // window.sessionStorage.setItem("loggedIn","true");
  // window.location="/dashboard"
   
   
  }
  render(){
  return (
    <div className="login-page">
        <br/>
    <div className="login-box">
       <div className="login-logo">
       <a href="#"><img src="assets/dist/img/iserazo.png" style={{borderRadius:'3px'}} height="45px" width="45px"/>
       <img src="assets/dist/img/logo-cercom.jpg" width="50px"/><b> CERCOM</b>|Admin</a>
    </div>
    <div className="login-box-body">
    <p className="login-box-msg"style={{"fontFamily":"Arial","fontSize":"14px"}} >Veuillez-remplir les champs suivants:</p>
    <b style={{"fontFamily":"Arial","fontSize":"13px","color":"red","textAlign":"center"}} >{this.state.erreur}</b>
      <form role="form" onSubmit={this.onSubmit}>
        <div className="form-group has-feedback">
            <label style={{"fontFamily":"Arial","fontSize":"13px"}}>Votre email</label>
            <input type="email" id="motdepasse" className="form-control" placeholder="Votre adresse email"  value={this.state.email} onChange={this.onChangeemail} required/>
            <span className="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div className="form-group has-feedback">
          <label style={{"fontFamily":"Arial","fontSize":"13px"}}>Votre mot de passe</label>
          <input type="password" className="form-control" placeholder="Mot de passe" value={this.state.motdepasse} onChange={this.onChangemotdepasse} required/>
          <span className="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div className="row">
          <div className="col-xs-8">    
            <div className="checkbox icheck">
              <label></label>
            </div>                        
          </div>
          <div className="col-xs-4">
            <button type="submit" className="btn btn-primary btn-block btn-flat">Connecter</button>
          </div>
        </div>
      </form>
    
    </div>
    </div>
    <br/><br/><br/><br/><br/><br/><br/>
    <br/>
    <br/>
    <br/>

     
    </div>
  );
}
}

