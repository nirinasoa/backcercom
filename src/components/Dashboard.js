import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Pagination from '@material-ui/lab/Pagination';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import Divider from '@material-ui/core/Divider';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Barcode from 'react-barcode';
import Avatar from '@material-ui/core/Avatar';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ChatIcon from '@material-ui/icons/Chat';
import ViewDayIcon from '@material-ui/icons/ViewDay';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import MicIcon from '@material-ui/icons/Mic';
import Autocomplete from '@material-ui/lab/Autocomplete';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import RefreshIcon from '@material-ui/icons/Refresh';
import ReactToExcel from 'react-html-table-to-excel';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import CloseIcon from '@material-ui/icons/Close';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true
recognition.lang = 'fr-FR'

export default class Dashboard extends Component{
  constructor(props) {
    super(props)
    this.delete=this.delete.bind(this); 
    
    this.handlePageClick = this.handlePageClick.bind(this);
    this.searchHandler=this.searchHandler.bind(this); 
    this.reset=this.reset.bind(this); 
    this.searchHandlerRM=this.searchHandlerRM.bind(this); 
    this.toggleListen = this.toggleListen.bind(this)
    this.handleListen = this.handleListen.bind(this)
    this.onChangetitre=this.onChangetitre.bind(this)
    this.onChangenomauteur=this.onChangenomauteur.bind(this)

		this.state={
      ouvrages:[],
      ouvragesNA:[],
      message:'',
      offset: 0,
		  perPage: 10,
      currentPage: 0,
      listening: false,
      finalTranscript:'',
      bgColor: '#4b63db',
      auteurs:[],
      nomAuteur:null,
      titre:'',
      hidden:false,
      ouvragesHidden:[]
		 
    };

    }
    reset(){
      this.setState({finalTranscript:'',message:''})
      this.componentDidMount()
    }
    onChangenomauteur(e){
   
      if(this.state.nomAuteur==null){
        console.log("null="+this.state.nomAuteur)
        this.setState({
          nomAuteur:e.target.value
        });
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
        .then(response=>{
          this.setState({ouvragesNA:response.data})
        })
      }
      else{
        this.setState({
          nomAuteur:e.target.value
        });
      }
    }
      onChangetitre(e){
       this.setState({
        titre:e.target.value
      });
      if(this.state.nomAuteur==null){
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
        .then(response=>{
          this.setState({ouvragesNA:response.data})
        })
      }
      if(this.state.nomAuteur!=null){
       
         Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteurByNA.php/?nomAuteur='+this.state.nomAuteur)
        .then(response=>{
          if(response.data.length>0){
            this.setState({ouvragesNA:response.data})
          }
          else{
            this.setState({ouvragesNA:[]})
          }
        
          
        })
        .catch(err=>{
          this.setState({ouvragesNA:[]})
        })
        }
    }
    toggleListen() {
      this.setState({
        bgColor: '#e82a33',
        listening: !this.state.listening
      }, this.handleListen)
    }
    
    handleListen(){
      if (this.state.listening) {
          recognition.start()
    
        let finalTranscript = ''
        recognition.onresult = event => {
          let interimTranscript = ''
    
          for (let i = event.resultIndex; i < event.results.length; i++) {
            const transcript = event.results[i][0].transcript;
            if (event.results[i].isFinal) finalTranscript += transcript + ' ';
            else interimTranscript += transcript;
          }
          this.setState({finalTranscript:finalTranscript});
        if(this.state.finalTranscript!=''){
          this.setState({bgColor:'#4b63db'});
            this.searchHandler1(this.state.finalTranscript)
        }
      }
    }
    else{
      recognition.stop()
      alert('Une erreur est survenue!')
     
    }
  }
    handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState({
          currentPage: selectedPage,
          offset: offset
      }, () => {
          this.componentDidMount()
      });

  };
  searchHandler1(e){
     


       var test=' résultat(s) trouvé(s)'
     Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/search.php/',{wordToSearch:e})
     .then(response=>{
       if(response.data.message!='Could not find data.'){
        this.searchFonction(response)
      }
       else{
         this.setState({message:'Aucun'+test,ouvrages:[]})
       }
      
       
     
       //this.setState({posts:response.data})
     })
   
     }
     searchFonction(response){
      var test=' résultat(s) trouvé(s)'
      const data = response.data;
      const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
      const postdata = slice.map(ouvrage=>
        <div style={{backgroundColor:'white',borderRadius:'5px'}}>
        <div className="row" >
          
    <div className="col-md-1">
      <br/>
        <a href={decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}><Avatar variant="rounded" src={ decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} style={{"height":"140px","width":"100px"}}/></a>
      </div>
      <div className="col-md-10">
        <h4><Link style={{"color":"#383737"}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><b>{ouvrage.titre}</b></Link></h4>
        <h5 className="text-muted"><Link to={`/detailAuteur/${ouvrage.idAuteur}`}>{ouvrage.nomAuteur}</Link>|{ouvrage.nomGenre}| {ouvrage.nombrePage} pages| {ouvrage.descriptionGenre}</h5>
        <p style={{textOverflow: 'ellipsis', overflow: 'hidden',whiteSpace: 'nowrap',fontFamily:'Arial'}}>{ouvrage.description} <br/> </p>
        <Link style={{}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><ViewDayIcon/> Générer code-barres</Link>
        &nbsp; &nbsp; &nbsp;<Link to={`detailOuvrage/${ouvrage.idOuvrage}`}><ChatIcon/>  Voir les commentaires </Link>
                             
      </div>
    
       <div className="col-md-1"><br/>
       <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
       <Link style={{"color":"#383737"}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><EditIcon fontSize="large"  style={{"color":"#4a4d49"}}  /></Link> 
          </Fab><br/><br/>
          <Fab size="small" color="default" aria-label="add" title="Suppimer cette ligne">
          <DeleteIcon fontSize="large"  style={{"color":"#4a4d49"}}  onClick={() => this.delete(ouvrage.idOuvrage)} /> 
        </Fab>
       </div>
     
   
     
    </div>
    <hr/>
    </div>)
  
      this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
   
     }
    searchHandlerRM(e){
      var test=' résultat(s) trouvé(s)'
      if(this.state.nomAuteur!=null && this.state.titre!=''){
        console.log('ato mis daoly ny state '+this.state.nomAuteur+"_titre_"+this.state.titre)
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteurByNaTt.php/?nomAuteur='+this.state.nomAuteur+'&titre='+this.state.titre)
        .then(response=>{
          if(response.data.message!='Could not find data.'){
            this.searchFonction(response)
         }
          else{
            this.setState({message:'Aucun'+test,ouvrages:[]})
          }
         }) 

      }
      else if(this.state.nomAuteur!=null && this.state.titre==''){
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteurByNaTtOr.php/?nomAuteur='+this.state.nomAuteur)
        .then(response=>{
          if(response.data.message!='Could not find data.'){
            this.searchFonction(response)
         }
          else{
            this.setState({message:'Aucun'+test,ouvrages:[]})
          }
         }) 
      }
      else if(this.state.nomAuteur==null && this.state.titre!=''){
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteurByNaTtOr.php/?nomAuteur='+this.state.titre)
        .then(response=>{
          if(response.data.message!='Could not find data.'){
            this.searchFonction(response)
         }
          else{
            this.setState({message:'Aucun'+test,ouvrages:[]})
          }
         }) 
      
      }
      else{
        console.log('afaafa')
      }
    }
    searchHandler(e){
     this.setState({finalTranscript:e.target.value})

        var test=' résultat(s) trouvé(s)'
      Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/search.php/',{wordToSearch:e.target.value})
      .then(response=>{
        if(response.data.message!='Could not find data.'){
          this.searchFonction(response)
       }
        else{
          this.setState({message:'Aucun'+test,ouvrages:[]})
        }
       })
    
      }
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/deleteOne.php',{idOuvrage:id})
      .then(res => console.log(res.data));
     this.componentDidMount()
       
     } 
     else{}
    } 
    componentDidMount(){
      Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
      .then(response=>{
         this.setState({ouvragesHidden:response.data})
      })
      if(this.state.nomAuteur==null){
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
        .then(response=>{
          this.setState({ouvragesNA:response.data})
        })
      }
      if(this.state.nomAuteur!=null){
       
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteurByNA.php/?nomAuteur='+this.state.nomAuteur)
        .then(response=>{
          if(response.data.length>0){
            this.setState({ouvragesNA:response.data})
          }
          else{
            this.setState({ouvragesNA:[]})
          }
        
          
        })
        .catch(err=>{
          this.setState({ouvragesNA:[],message:'Aucun ouvrrage enregistré!'})
        })
      }
     
      Axios.get('https://phpapiserver.herokuapp.com/api/auteur/read.php')
    .then(response=>{
      this.setState({auteurs:response.data})
    })
      Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrage.php')
      .then(response=>{
        const data = response.data;
			const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
			const postdata = slice.map(ouvrage=>
        <div style={{backgroundColor:'white',borderRadius:'5px'}}>
            <div className="row" >
              
        <div className="col-md-1">
          <br/>
            <a href={ decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}><Avatar variant="rounded" src={ decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} style={{"height":"140px","width":"100px"}}/></a>
          </div>
          <div className="col-md-10">
             <h4><Link style={{"color":"#383737"}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><b>{ouvrage.titre}</b></Link></h4>
            <h5 className="text-muted"><Link to={`/detailAuteur/${ouvrage.idAuteur}`}>{ouvrage.nomAuteur}</Link>|{ouvrage.nomGenre}| {ouvrage.nombrePage} pages| {ouvrage.descriptionGenre}</h5>
            <p style={{textOverflow: 'ellipsis', overflow: 'hidden',whiteSpace: 'nowrap',fontFamily:'Arial'}}>{ouvrage.description} <br/> </p>
            <Link style={{}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><ViewDayIcon/> Générer code-barres</Link>
            &nbsp; &nbsp; &nbsp;<Link to={`detailOuvrage/${ouvrage.idOuvrage}`}><ChatIcon/>  Voir les commentaires </Link>
                                 
          </div>
        
           <div className="col-md-1"><br/>
           <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
           <Link style={{"color":"#383737"}} to={`/detailOuvrage/${ouvrage.idOuvrage}`}><EditIcon fontSize="large"  style={{"color":"#4a4d49"}}  /></Link> 
              </Fab><br/><br/>
              <Fab size="small" color="default" aria-label="add" title="Suppimer cette ligne">
              <DeleteIcon fontSize="large"  style={{"color":"#4a4d49"}}  onClick={() => this.delete(ouvrage.idOuvrage)} /> 
            </Fab>
           </div>
         
       
         
        </div>
        <hr/>
        </div>)
     this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
        
      })
      .catch(error=>{
        console.log(error)
      })
     }
  render(){
    let tablehidden;
    if(this.state.hidden==false){
     tablehidden=<Accordion style={{padding:'5px 5px'}}>
                     <AccordionSummary
                           expandIcon={<ExpandMoreIcon />}
                           aria-controls="panel1a-content"
                           id="panel1a-header"
                         >
                         <Typography ><span style={{fontSize:'13px',fontFamily:'Arial'}}>©RYN 2020&nbsp;&nbsp;&nbsp;Voir  liste complète des ouvrages</span></Typography>
                     </AccordionSummary>
                     <AccordionDetails>
                         <Typography>
                         <table style={{width:'100%',fontFamily:'Arial',fontSize:'13px'}} id="tableOuvrageHidden"  className="table table-bordered table-striped" >
                             <thead>
                                 <tr>
                                     <th style={{width:'3%'}} scope="col">ID</th>
                                     <th  scope="col">Titre</th>
                                     <th  scope="col">Auteur</th>
                                     <th  scope="col">Genre</th>
                                     <th scope="col">Editeur</th>
                                     <th scope="col">Nombre page</th>
                                     <th scope="col">Prix</th>
                                     <th scope="col">Année d'édition</th>
                                     <th scope="col">Quantité</th>
                                     <th scope="col">Lieu d'édition</th>
                                     <th scope="col">Origine</th>
                                     <th scope="col">Date d'entrée</th>
                                     <th scope="col">Description</th>
                                     <th scope="col">Langue</th>
                                 </tr>
                             </thead>
                             <tbody>
                             {this.state.ouvragesHidden.map(ouv=>
                                 <tr>
                                     <td data-label="Identification:"><span class="label label-warning">{ouv.idOuvrage}</span></td>
                                     <td data-label="Titre:"> {ouv.titre}</td>
                                     <td data-label="Auteur:"> {ouv.nomAuteur}</td>
                                     <td data-label="Genre:">{ouv.nomGenre}</td>
                                     <td data-label="editeur:">{ouv.editeur}</td>
                                     <td data-label="Nombre de page:">{ouv.nombrePage}</td>
                                     <td data-label="prix:">{ouv.prix}</td>
                                     <td data-label="Année d'édition">{ouv.anneeEdition}</td>
                                     <td data-label="quantité:">{ouv.quantite}</td>
                                     <td data-label="Lieu d'édition:">{ouv.lieuEdition}</td>
                                     <td data-label="origine:">{ouv.origine}</td>
                                     <td data-label="Date d'entrée:">{ouv.dateEntree}</td>
                                     <td data-label="Description:">{ouv.description}</td>
                                     <td data-label="Langue:">{ouv.langue}</td>
                                 </tr>)}
                             </tbody>
                         </table>
                         </Typography>
                     </AccordionDetails>
                 </Accordion>
    }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>Liste des ouvrages enrengistrés</h1>
      <ol className="breadcrumb">
      
         
      <li>

      <ReactToExcel 
         className="excel"
         sheet="Sheet"
          table="tableOuvrageHidden" 
          filename="Liste_Ouvrages_CERCOM"
          submit="submit 1"
          buttonText="Exporter la liste vers Excel"
          
          />&nbsp;&nbsp;
      </li>
     
    <li><Link to="/dashboard"> Accueil</Link></li>

   
  </ol>
    </section>

    <section className="content">
            <div className="row" >
            
                <div className="col-md-5">
               
                  <div className="input-group">
                    <input value={this.state.finalTranscript} 
                      onChange={this.searchHandler}
                      id="search" type="text" className="form-control" 
                      placeholder="Effectuer une recherche..." 
                      style={{"borderRadius":"10%","backgroundColor":"#ffffff","border":"1px solid #827f7f"}} 
                     />
                    <span className="input-group-btn">
                    <span  id='search-btn'  onClick={this.reset} className="btn btn-flat" style={{cursor:'pointer',"border":"1px solid #827f7f"}}><i className="fa fa-close"></i></span>
                    <button onClick={this.toggleListen}  className="btn btn-flat" style={{color:this.state.bgColor,"border":"1px solid #827f7f"}}><i className="fa fa-microphone"></i></button>
                    <button id='search-btn' className="btn btn-flat" style={{"border":"1px solid #827f7f"}}><i className="fa fa-search"></i></button>
                    
                    </span>
                  </div>
                
                </div>
                <div className="col-md-2" >
               
                <Autocomplete   
                        id="combo-box-demo"
                        options={this.state.auteurs}
                      
                        getOptionLabel={(option) => option.nomAuteur}
                        fullWidth
                        renderInput={(params) =>  <TextField required fullWidth id="outlined-basic" {...params} 
                        placeholder="Choisir l'auteur"  
                         InputProps={{  ...params.InputProps, type: 'search',style: { fontSize: 13,backgroundColor:'transparent' }}}
                        
                        variant="outlined"
                        value={this.state.nomAuteur}
                         onChange={this.onChangenomauteur}
                         onSelect={this.onChangenomauteur}
                       
                     
                        />
                      
                      }
                     />
                  
                </div>
                <div className="col-md-2">
                <Autocomplete   
                        id="combo-box-demo"
                        options={this.state.ouvragesNA}
                      
                        getOptionLabel={(option) => option.titre}
                        fullWidth
                        renderInput={(params) =>  <TextField required fullWidth id="outlined-basic" {...params} placeholder="Choisir le titre"  
                         InputProps={{  ...params.InputProps, type: 'search',style: { fontSize: 13 ,backgroundColor:'transparent'}}}
                        
                        variant="outlined"
                        value={this.state.titre}
                         onChange={this.onChangetitre}
                         onSelect={this.onChangetitre}
                       
                     
                        />
                      
                      }
                     />
                      
                </div>
                <div className="col-md-2">
                <IconButton onClick={this.searchHandlerRM}  style={{backgroundColor:'#c2bebe'}}>
                    <SearchIcon style={{color:'white'}} fontSize="medium"/>
                   
                </IconButton>&nbsp;&nbsp;
                <IconButton onClick={this.reset}  style={{backgroundColor:'#c2bebe'}}>
                    <RefreshIcon style={{color:'white'}} fontSize="medium"/>
                   
                </IconButton>
     
                </div>
                <hr/>
            </div>
            <div className="row">
               <div className="col-md-5">
                
                  <Link to="/apa" style={{display:'block'}}><button className="btn btn-default"><MenuBookIcon/>&nbsp;Voir la bibliographie aux normes APA</button></Link>
                  <br/>
               </div>
               <div className="col-md-5">
              
               </div>
               <div className="col-md-2">
               <Link  to="/ouvrage" style={{"fontSize":"14px"}}><button className="btn btn-success"><AddIcon />&nbsp; Ajouter un ouvrage</button></Link>
               </div>
            </div>
                
           
              <span style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
              {this.state.postdata}  
             
              <div style={{marginLeft:'40%'}}>
              <ReactPaginate 
                    previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                    nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
             </div>
            
      
        
   
    </section>
    {tablehidden}
  </div>
   
  );
}
}

