import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import './style.css';
import ImageUploader from 'react-images-upload';
import { createWorker } from 'tesseract.js';
import { FilePond, registerPlugin } from 'react-filepond';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import './filepond-plugin-image-preview.min.css';
import './filepond.min.css';
import './filepond.css';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import { Link } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
import MicIcon from '@material-ui/icons/Mic';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import Moment from 'moment';
import Avatar from '@material-ui/core/Avatar';
import 'moment/locale/fr';
import BarcodeScannerComponent from "react-webcam-barcode-scanner";
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import StopIcon from '@material-ui/icons/Stop';

registerPlugin(FilePondPluginImagePreview);

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()
const recognitionEdt = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true
recognition.lang = 'fr-FR'

recognitionEdt.continous = true
recognitionEdt.interimResults = true
recognitionEdt.lang = 'fr-FR'
export default class Ouvrage extends Component{
  constructor(props) {
    super(props)
    this.demarrer=this.demarrer.bind(this)  
        this.stop=this.stop.bind(this) 
     this.onChangeIsbn=this.onChangeIsbn.bind(this)
    this.searchBookByISBN=this.searchBookByISBN.bind(this)
    this.onChangeautrelangue=this.onChangeautrelangue.bind(this)
    this.onChangeauteur=this.onChangeauteur.bind(this)
    this.onChangeauteurOCR=this.onChangeauteurOCR.bind(this)
    this.onChangetitre=this.onChangetitre.bind(this)
    this.onChangeediteur=this.onChangeediteur.bind(this)
    this.onChangeediteur=this.onChangeediteur.bind(this)
    this.onChangeprix=this.onChangeprix.bind(this)
    this.onChangenombrepage=this.onChangenombrepage.bind(this)
    this.onChangegenre=this.onChangegenre.bind(this)
    this.onChangelangue=this.onChangelangue.bind(this)
    this.onChangeannee=this.onChangeannee.bind(this)
    this.onChangedateentree=this.onChangedateentree.bind(this)
    this.onChangequantite=this.onChangequantite.bind(this)
    this.onChangeorigine=this.onChangeorigine.bind(this)
    this.onChangelieu=this.onChangelieu.bind(this)
    this.onChangedescription=this.onChangedescription.bind(this)
    this.onChangenomauteur=this.onChangenomauteur.bind(this)
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
    this.onChangenationalite=this.onChangenationalite.bind(this)
    this.onSubmitOuvrageManuellement=this.onSubmitOuvrageManuellement.bind(this)
    this.onSubmitOuvrageAuto=this.onSubmitOuvrageAuto.bind(this)
    this.onSubmitAuteur=this.onSubmitAuteur.bind(this)
    this.onDrop = this.onDrop.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.fetchData = this.fetchData.bind(this);
    this.pond = React.createRef();
    this.worker = React.createRef();
    this.worker1 = React.createRef();
    this.workerAut = React.createRef();
    this.updateProgressAndLog = this.updateProgressAndLog.bind(this);
    this.updateProgressAndLog1 = this.updateProgressAndLog1.bind(this);
    this.updateProgressAndLogAut = this.updateProgressAndLogAut.bind(this);
    this.toggleListen = this.toggleListen.bind(this)
    this.handleListen = this.handleListen.bind(this)
    this.toggleListenEdt = this.toggleListenEdt.bind(this)
    this.handleListenEdt = this.handleListenEdt.bind(this)
    this.importerExcel = this.importerExcel.bind(this)
    this.handleClose1=this.handleClose1.bind(this);
    this.reset = this.reset.bind(this);
    this.state={
        auteur:'',
        auteurOCR:'',
        open1:'',
        isProcessingAut : false,
        pctgAut : '0.00',
        isProcessing : false,
        isProcessing1 : false,
        ocrText : '',
        pctg : '0.00',
        pctg1 : '0.00',
        titre:'',
        editeur:'',
        nombrepage:1,
        openCamera:false,
        prix:1,
        genre:'',
        langue:'',
        annee:'',
        photoIsbn:'',
        dateentree:Date.now(),
        quantite:1,
        origine:'',
        lieu:'',
        description:'',
        nomauteur:'',
        datenaissance:'',
        nationalite:'',
        autrelangue:'',
        genres:[],
        auteurs:[],
        langues:[{ typelangue: 'Anglais'},{ typelangue: 'Français'},{ typelangue: 'Italien'}],
        idGenreFromColumn:'',
        idAuteurFromColumn:'',
        idAuteurFromColumn1:'',
        idTemp:'',
        tempgenres:[],
        pictures: [],
        photoOuvrage:'',
        file:null,
        filename:null,
        listening: false,
        listeningEdt: false,
        bgColor: '#4b63db',
        bgColor1: '#4b63db',
        cols:[],
        rows:[],
        messageImport:'',
        messageImportError:'',
        bookInfo:'',
        isbn:'',
        imageurl:'',
        result:'',
        lieux:[
  { code: 'AD', label: 'Andorra', phone: '376' },
  { code: 'AE', label: 'United Arab Emirates', phone: '971' },
  { code: 'AF', label: 'Afghanistan', phone: '93' },
  { code: 'AG', label: 'Antigua and Barbuda', phone: '1-268' },
  { code: 'AI', label: 'Anguilla', phone: '1-264' },
  { code: 'AL', label: 'Albania', phone: '355' },
  { code: 'AM', label: 'Armenia', phone: '374' },
  { code: 'AO', label: 'Angola', phone: '244' },
  { code: 'AQ', label: 'Antarctica', phone: '672' },
  { code: 'AR', label: 'Argentina', phone: '54' },
  { code: 'AS', label: 'American Samoa', phone: '1-684' },
  { code: 'AT', label: 'Austria', phone: '43' },
  { code: 'AU', label: 'Australia', phone: '61', suggested: true },
  { code: 'AW', label: 'Aruba', phone: '297' },
  { code: 'AX', label: 'Alland Islands', phone: '358' },
  { code: 'AZ', label: 'Azerbaijan', phone: '994' },
  { code: 'BA', label: 'Bosnia and Herzegovina', phone: '387' },
  { code: 'BB', label: 'Barbados', phone: '1-246' },
  { code: 'BD', label: 'Bangladesh', phone: '880' },
  { code: 'BE', label: 'Belgium', phone: '32' },
  { code: 'BF', label: 'Burkina Faso', phone: '226' },
  { code: 'BG', label: 'Bulgaria', phone: '359' },
  { code: 'BH', label: 'Bahrain', phone: '973' },
  { code: 'BI', label: 'Burundi', phone: '257' },
  { code: 'BJ', label: 'Benin', phone: '229' },
  { code: 'BL', label: 'Saint Barthelemy', phone: '590' },
  { code: 'BM', label: 'Bermuda', phone: '1-441' },
  { code: 'BN', label: 'Brunei Darussalam', phone: '673' },
  { code: 'BO', label: 'Bolivia', phone: '591' },
  { code: 'BR', label: 'Brazil', phone: '55' },
  { code: 'BS', label: 'Bahamas', phone: '1-242' },
  { code: 'BT', label: 'Bhutan', phone: '975' },
  { code: 'BV', label: 'Bouvet Island', phone: '47' },
  { code: 'BW', label: 'Botswana', phone: '267' },
  { code: 'BY', label: 'Belarus', phone: '375' },
  { code: 'BZ', label: 'Belize', phone: '501' },
  { code: 'CA', label: 'Canada', phone: '1', suggested: true },
  { code: 'CC', label: 'Cocos (Keeling) Islands', phone: '61' },
  { code: 'CD', label: 'Congo, Democratic Republic of the', phone: '243' },
  { code: 'CF', label: 'Central African Republic', phone: '236' },
  { code: 'CG', label: 'Congo, Republic of the', phone: '242' },
  { code: 'CH', label: 'Switzerland', phone: '41' },
  { code: 'CI', label: "Cote d'Ivoire", phone: '225' },
  { code: 'CK', label: 'Cook Islands', phone: '682' },
  { code: 'CL', label: 'Chile', phone: '56' },
  { code: 'CM', label: 'Cameroon', phone: '237' },
  { code: 'CN', label: 'China', phone: '86' },
  { code: 'CO', label: 'Colombia', phone: '57' },
  { code: 'CR', label: 'Costa Rica', phone: '506' },
  { code: 'CU', label: 'Cuba', phone: '53' },
  { code: 'CV', label: 'Cape Verde', phone: '238' },
  { code: 'CW', label: 'Curacao', phone: '599' },
  { code: 'CX', label: 'Christmas Island', phone: '61' },
  { code: 'CY', label: 'Cyprus', phone: '357' },
  { code: 'CZ', label: 'Czech Republic', phone: '420' },
  { code: 'DE', label: 'Germany', phone: '49', suggested: true },
  { code: 'DJ', label: 'Djibouti', phone: '253' },
  { code: 'DK', label: 'Denmark', phone: '45' },
  { code: 'DM', label: 'Dominica', phone: '1-767' },
  { code: 'DO', label: 'Dominican Republic', phone: '1-809' },
  { code: 'DZ', label: 'Algeria', phone: '213' },
  { code: 'EC', label: 'Ecuador', phone: '593' },
  { code: 'EE', label: 'Estonia', phone: '372' },
  { code: 'EG', label: 'Egypt', phone: '20' },
  { code: 'EH', label: 'Western Sahara', phone: '212' },
  { code: 'ER', label: 'Eritrea', phone: '291' },
  { code: 'ES', label: 'Spain', phone: '34' },
  { code: 'ET', label: 'Ethiopia', phone: '251' },
  { code: 'FI', label: 'Finland', phone: '358' },
  { code: 'FJ', label: 'Fiji', phone: '679' },
  { code: 'FK', label: 'Falkland Islands (Malvinas)', phone: '500' },
  { code: 'FM', label: 'Micronesia, Federated States of', phone: '691' },
  { code: 'FO', label: 'Faroe Islands', phone: '298' },
  { code: 'FR', label: 'France', phone: '33', suggested: true },
  { code: 'GA', label: 'Gabon', phone: '241' },
  { code: 'GB', label: 'United Kingdom', phone: '44' },
  { code: 'GD', label: 'Grenada', phone: '1-473' },
  { code: 'GE', label: 'Georgia', phone: '995' },
  { code: 'GF', label: 'French Guiana', phone: '594' },
  { code: 'GG', label: 'Guernsey', phone: '44' },
  { code: 'GH', label: 'Ghana', phone: '233' },
  { code: 'GI', label: 'Gibraltar', phone: '350' },
  { code: 'GL', label: 'Greenland', phone: '299' },
  { code: 'GM', label: 'Gambia', phone: '220' },
  { code: 'GN', label: 'Guinea', phone: '224' },
  { code: 'GP', label: 'Guadeloupe', phone: '590' },
  { code: 'GQ', label: 'Equatorial Guinea', phone: '240' },
  { code: 'GR', label: 'Greece', phone: '30' },
  { code: 'GS', label: 'South Georgia and the South Sandwich Islands', phone: '500' },
  { code: 'GT', label: 'Guatemala', phone: '502' },
  { code: 'GU', label: 'Guam', phone: '1-671' },
  { code: 'GW', label: 'Guinea-Bissau', phone: '245' },
  { code: 'GY', label: 'Guyana', phone: '592' },
  { code: 'HK', label: 'Hong Kong', phone: '852' },
  { code: 'HM', label: 'Heard Island and McDonald Islands', phone: '672' },
  { code: 'HN', label: 'Honduras', phone: '504' },
  { code: 'HR', label: 'Croatia', phone: '385' },
  { code: 'HT', label: 'Haiti', phone: '509' },
  { code: 'HU', label: 'Hungary', phone: '36' },
  { code: 'ID', label: 'Indonesia', phone: '62' },
  { code: 'IE', label: 'Ireland', phone: '353' },
  { code: 'IL', label: 'Israel', phone: '972' },
  { code: 'IM', label: 'Isle of Man', phone: '44' },
  { code: 'IN', label: 'India', phone: '91' },
  { code: 'IO', label: 'British Indian Ocean Territory', phone: '246' },
  { code: 'IQ', label: 'Iraq', phone: '964' },
  { code: 'IR', label: 'Iran, Islamic Republic of', phone: '98' },
  { code: 'IS', label: 'Iceland', phone: '354' },
  { code: 'IT', label: 'Italy', phone: '39' },
  { code: 'JE', label: 'Jersey', phone: '44' },
  { code: 'JM', label: 'Jamaica', phone: '1-876' },
  { code: 'JO', label: 'Jordan', phone: '962' },
  { code: 'JP', label: 'Japan', phone: '81', suggested: true },
  { code: 'KE', label: 'Kenya', phone: '254' },
  { code: 'KG', label: 'Kyrgyzstan', phone: '996' },
  { code: 'KH', label: 'Cambodia', phone: '855' },
  { code: 'KI', label: 'Kiribati', phone: '686' },
  { code: 'KM', label: 'Comoros', phone: '269' },
  { code: 'KN', label: 'Saint Kitts and Nevis', phone: '1-869' },
  { code: 'KP', label: "Korea, Democratic People's Republic of", phone: '850' },
  { code: 'KR', label: 'Korea, Republic of', phone: '82' },
  { code: 'KW', label: 'Kuwait', phone: '965' },
  { code: 'KY', label: 'Cayman Islands', phone: '1-345' },
  { code: 'KZ', label: 'Kazakhstan', phone: '7' },
  { code: 'LA', label: "Lao People's Democratic Republic", phone: '856' },
  { code: 'LB', label: 'Lebanon', phone: '961' },
  { code: 'LC', label: 'Saint Lucia', phone: '1-758' },
  { code: 'LI', label: 'Liechtenstein', phone: '423' },
  { code: 'LK', label: 'Sri Lanka', phone: '94' },
  { code: 'LR', label: 'Liberia', phone: '231' },
  { code: 'LS', label: 'Lesotho', phone: '266' },
  { code: 'LT', label: 'Lithuania', phone: '370' },
  { code: 'LU', label: 'Luxembourg', phone: '352' },
  { code: 'LV', label: 'Latvia', phone: '371' },
  { code: 'LY', label: 'Libya', phone: '218' },
  { code: 'MA', label: 'Morocco', phone: '212' },
  { code: 'MC', label: 'Monaco', phone: '377' },
  { code: 'MD', label: 'Moldova, Republic of', phone: '373' },
  { code: 'ME', label: 'Montenegro', phone: '382' },
  { code: 'MF', label: 'Saint Martin (French part)', phone: '590' },
  { code: 'MG', label: 'Madagascar', phone: '261' },
  { code: 'MH', label: 'Marshall Islands', phone: '692' },
  { code: 'MK', label: 'Macedonia, the Former Yugoslav Republic of', phone: '389' },
  { code: 'ML', label: 'Mali', phone: '223' },
  { code: 'MM', label: 'Myanmar', phone: '95' },
  { code: 'MN', label: 'Mongolia', phone: '976' },
  { code: 'MO', label: 'Macao', phone: '853' },
  { code: 'MP', label: 'Northern Mariana Islands', phone: '1-670' },
  { code: 'MQ', label: 'Martinique', phone: '596' },
  { code: 'MR', label: 'Mauritania', phone: '222' },
  { code: 'MS', label: 'Montserrat', phone: '1-664' },
  { code: 'MT', label: 'Malta', phone: '356' },
  { code: 'MU', label: 'Mauritius', phone: '230' },
  { code: 'MV', label: 'Maldives', phone: '960' },
  { code: 'MW', label: 'Malawi', phone: '265' },
  { code: 'MX', label: 'Mexico', phone: '52' },
  { code: 'MY', label: 'Malaysia', phone: '60' },
  { code: 'MZ', label: 'Mozambique', phone: '258' },
  { code: 'NA', label: 'Namibia', phone: '264' },
  { code: 'NC', label: 'New Caledonia', phone: '687' },
  { code: 'NE', label: 'Niger', phone: '227' },
  { code: 'NF', label: 'Norfolk Island', phone: '672' },
  { code: 'NG', label: 'Nigeria', phone: '234' },
  { code: 'NI', label: 'Nicaragua', phone: '505' },
  { code: 'NL', label: 'Netherlands', phone: '31' },
  { code: 'NO', label: 'Norway', phone: '47' },
  { code: 'NP', label: 'Nepal', phone: '977' },
  { code: 'NR', label: 'Nauru', phone: '674' },
  { code: 'NU', label: 'Niue', phone: '683' },
  { code: 'NZ', label: 'New Zealand', phone: '64' },
  { code: 'OM', label: 'Oman', phone: '968' },
  { code: 'PA', label: 'Panama', phone: '507' },
  { code: 'PE', label: 'Peru', phone: '51' },
  { code: 'PF', label: 'French Polynesia', phone: '689' },
  { code: 'PG', label: 'Papua New Guinea', phone: '675' },
  { code: 'PH', label: 'Philippines', phone: '63' },
  { code: 'PK', label: 'Pakistan', phone: '92' },
  { code: 'PL', label: 'Poland', phone: '48' },
  { code: 'PM', label: 'Saint Pierre and Miquelon', phone: '508' },
  { code: 'PN', label: 'Pitcairn', phone: '870' },
  { code: 'PR', label: 'Puerto Rico', phone: '1' },
  { code: 'PS', label: 'Palestine, State of', phone: '970' },
  { code: 'PT', label: 'Portugal', phone: '351' },
  { code: 'PW', label: 'Palau', phone: '680' },
  { code: 'PY', label: 'Paraguay', phone: '595' },
  { code: 'QA', label: 'Qatar', phone: '974' },
  { code: 'RE', label: 'Reunion', phone: '262' },
  { code: 'RO', label: 'Romania', phone: '40' },
  { code: 'RS', label: 'Serbia', phone: '381' },
  { code: 'RU', label: 'Russian Federation', phone: '7' },
  { code: 'RW', label: 'Rwanda', phone: '250' },
  { code: 'SA', label: 'Saudi Arabia', phone: '966' },
  { code: 'SB', label: 'Solomon Islands', phone: '677' },
  { code: 'SC', label: 'Seychelles', phone: '248' },
  { code: 'SD', label: 'Sudan', phone: '249' },
  { code: 'SE', label: 'Sweden', phone: '46' },
  { code: 'SG', label: 'Singapore', phone: '65' },
  { code: 'SH', label: 'Saint Helena', phone: '290' },
  { code: 'SI', label: 'Slovenia', phone: '386' },
  { code: 'SJ', label: 'Svalbard and Jan Mayen', phone: '47' },
  { code: 'SK', label: 'Slovakia', phone: '421' },
  { code: 'SL', label: 'Sierra Leone', phone: '232' },
  { code: 'SM', label: 'San Marino', phone: '378' },
  { code: 'SN', label: 'Senegal', phone: '221' },
  { code: 'SO', label: 'Somalia', phone: '252' },
  { code: 'SR', label: 'Suriname', phone: '597' },
  { code: 'SS', label: 'South Sudan', phone: '211' },
  { code: 'ST', label: 'Sao Tome and Principe', phone: '239' },
  { code: 'SV', label: 'El Salvador', phone: '503' },
  { code: 'SX', label: 'Sint Maarten (Dutch part)', phone: '1-721' },
  { code: 'SY', label: 'Syrian Arab Republic', phone: '963' },
  { code: 'SZ', label: 'Swaziland', phone: '268' },
  { code: 'TC', label: 'Turks and Caicos Islands', phone: '1-649' },
  { code: 'TD', label: 'Chad', phone: '235' },
  { code: 'TF', label: 'French Southern Territories', phone: '262' },
  { code: 'TG', label: 'Togo', phone: '228' },
  { code: 'TH', label: 'Thailand', phone: '66' },
  { code: 'TJ', label: 'Tajikistan', phone: '992' },
  { code: 'TK', label: 'Tokelau', phone: '690' },
  { code: 'TL', label: 'Timor-Leste', phone: '670' },
  { code: 'TM', label: 'Turkmenistan', phone: '993' },
  { code: 'TN', label: 'Tunisia', phone: '216' },
  { code: 'TO', label: 'Tonga', phone: '676' },
  { code: 'TR', label: 'Turkey', phone: '90' },
  { code: 'TT', label: 'Trinidad and Tobago', phone: '1-868' },
  { code: 'TV', label: 'Tuvalu', phone: '688' },
  { code: 'TW', label: 'Taiwan, Province of China', phone: '886' },
  { code: 'TZ', label: 'United Republic of Tanzania', phone: '255' },
  { code: 'UA', label: 'Ukraine', phone: '380' },
  { code: 'UG', label: 'Uganda', phone: '256' },
  { code: 'US', label: 'United States', phone: '1', suggested: true },
  { code: 'UY', label: 'Uruguay', phone: '598' },
  { code: 'UZ', label: 'Uzbekistan', phone: '998' },
  { code: 'VA', label: 'Holy See (Vatican City State)', phone: '379' },
  { code: 'VC', label: 'Saint Vincent and the Grenadines', phone: '1-784' },
  { code: 'VE', label: 'Venezuela', phone: '58' },
  { code: 'VG', label: 'British Virgin Islands', phone: '1-284' },
  { code: 'VI', label: 'US Virgin Islands', phone: '1-340' },
  { code: 'VN', label: 'Vietnam', phone: '84' },
  { code: 'VU', label: 'Vanuatu', phone: '678' },
  { code: 'WF', label: 'Wallis and Futuna', phone: '681' },
  { code: 'WS', label: 'Samoa', phone: '685' },
  { code: 'XK', label: 'Kosovo', phone: '383' },
  { code: 'YE', label: 'Yemen', phone: '967' },
  { code: 'YT', label: 'Mayotte', phone: '262' },
  { code: 'ZA', label: 'South Africa', phone: '27' },
  { code: 'ZM', label: 'Zambia', phone: '260' },
  { code: 'ZW', label: 'Zimbabwe', phone: '263' },
]

      
      };
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';

    toggleListenEdt() {
      this.setState({
        bgColor1: '#e82a33',
        listeningEdt: !this.state.listeningEdt
      }, this.handleListenEdt)
    }
    fetchData(isbn) {
     
      fetch(`https://www.googleapis.com/books/v1/volumes?q=isbn:${encodeURIComponent(isbn)}`)
          .then(response => response.json() )
          .then(responseJson => {
         
             if(responseJson.items[0].volumeInfo.description!=null){
                this.setState({ description:responseJson.items[0].volumeInfo.description});
              }
              if(responseJson.items[0].volumeInfo.pageCount!=null){
                this.setState({ nombrepage:responseJson.items[0].volumeInfo.pageCount});
              }
              if(responseJson.items[0].volumeInfo.title!=null){
                this.setState({ titre:responseJson.items[0].volumeInfo.title});
              }
              if(responseJson.items[0].volumeInfo.publisher!=null){
                this.setState({ editeur:responseJson.items[0].volumeInfo.publisher});
              }
              if(responseJson.items[0].volumeInfo.publishedDate!=null){
                this.setState({ annee:Moment(responseJson.items[0].volumeInfo.publishedDate).format("YYYY")});
              }
              if(responseJson.items[0].volumeInfo.language!=null){
                this.setState({ langue:responseJson.items[0].volumeInfo.language});
              }
              if(responseJson.items[0].volumeInfo.authors[0]!=null){
                this.setState({ nomauteur:responseJson.items[0].volumeInfo.authors[0]});
              }
              if(responseJson.items[0].volumeInfo.categories[0]!=null){
                this.setState({ genre:responseJson.items[0].volumeInfo.categories[0]});
              }
              if(responseJson.items[0].volumeInfo.imageLinks.thumbnail!=null){
                this.setState({ photoIsbn:responseJson.items[0].volumeInfo.imageLinks.thumbnail});
              }
       })
      .catch(error => {
        console.error(error);
      });
    }
    handleListenEdt(){
      if (this.state.listeningEdt) {
        recognitionEdt.start()
  
      let finalTranscript = ''
      recognitionEdt.onresult = event => {
        let interimTranscript = ''
  
        for (let i = event.resultIndex; i < event.results.length; i++) {
          const transcript = event.results[i][0].transcript;
          if (event.results[i].isFinal) finalTranscript += transcript + ' ';
          else interimTranscript += transcript;
        }
        this.setState({editeur:finalTranscript});
      if(this.state.editeur!=''){
        this.setState({bgColor1:'#4b63db'});
         
      }
    }
  }
  else{
    recognitionEdt.stop()
    alert('Une erreur est survenue!')
   
  }
  }
  fileHandler = (event) => {
    this.setState({open1:true,fileupload:event.target.value})
    let fileObj = event.target.files[0];

    //just pass the fileObj as parameter
    ExcelRenderer(fileObj, (err, resp) => {
      if(err){
        console.log(err);            
      }
      else{
        this.setState({
          cols: resp.cols,
          rows: resp.rows
        });
      }
    });               

  }
    toggleListen() {
      this.setState({
        bgColor: '#e82a33',
        listening: !this.state.listening
      }, this.handleListen)
    }
    
    handleListen(){
      if (this.state.listening) {
        recognition.start()
  
      let finalTranscript = ''
      recognition.onresult = event => {
        let interimTranscript = ''
  
        for (let i = event.resultIndex; i < event.results.length; i++) {
          const transcript = event.results[i][0].transcript;
          if (event.results[i].isFinal) finalTranscript += transcript + ' ';
          else interimTranscript += transcript;
        }
        this.setState({titre:finalTranscript});
      if(this.state.titre!=''){
        this.setState({bgColor:'#4b63db'});
         
      }
    }
  }
  else{
    recognition.stop()
    alert('Une erreur est survenue!')
   
  }
  }
  demarrer(){
    this.setState({openCamera:true})
}
stop(){
     this.setState({openCamera:false,data:'Non trouvé'})
}
  handleClose1(){
    this.setState({open1:false,rows:[],cols:[],fileupload:''})
  };
    onChangeauteur(e){
      this.setState({
        auteur:e.target.value
      });
    }
    onChangeauteurOCR(e){
      this.setState({
        auteurOCR:e.target.value
      });
    }
    onChangeautrelangue(e){
      this.setState({
        autrelangue:e.target.value
      });
    }
    calcHeight(value) {
      let numberOfLineBreaks = (value.match(/\n/g) || []).length;
      // min-height + lines x line-height + padding + border
      let newHeight = 20 + numberOfLineBreaks * 20 + 12 + 2;
      return newHeight;
    }
    onChangetitre(e){
      console.log("titre="+e.target.value)
       this.setState({
        titre:e.target.value
      });
    }
    onChangeediteur(e){
      this.setState({
        editeur:e.target.value
      });
    }
    onChangenombrepage(e){
      this.setState({
        nombrepage:parseInt(e.target.value)
      });
     
    }
    onChangeprix(e){
      this.setState({
        prix:parseFloat(e.target.value)
      });
    }
    onChangegenre(e){
   
      this.setState({
        genre:e.target.value
      });
    }
    onChangelangue(e){
      this.setState({
        langue:e.target.value
      });
    }
    onChangeannee(e){
      this.setState({
        annee:e.target.value
      });
    }
    onChangedateentree(e){
      this.setState({
        dateentree:e.target.value
      });
    }
    onChangequantite(e){
      this.setState({
        quantite:e.target.value
      });
    }
    onChangeorigine(e){
      this.setState({
        origine:e.target.value
      });
    }
    onChangelieu(e){
      this.setState({
        lieu:e.target.value
      });
    }
    onChangedescription(e){
      console.log("desc="+e.target.value)
      this.setState({
        description:e.target.value
      });
    }
  
    onChangenomauteur(e){
      this.setState({
        nomauteur:e.target.value
      });
    }
    onChangedatenaissance(e){
      this.setState({
        datenaissance:Date.parse(e.target.value)
      });
    }
    onChangenationalite(e){
      this.setState({
        nationalite:e.target.value
      });
    }
    onSelect(e){
      this.setState({
        auteur:e.target.value
      });
    }
    onPaste(e){
      this.setState({
        auteur:e.target.value
      });
    }
    onChangePhotoOuvrage(e){
      this.setState({
        photoOuvrage:e.target.files
      });
    }
    onDrop(picture) {
      this.setState({
          pictures: this.state.pictures.concat(picture),
      });
  }
  onChangeFile(e) {
    this.setState({file:e.target.files[0]})

}
async doOCRAut(file) {
  this.setState({
      isProcessingAut : true,
      auteurOCR : '',
      pctgAut : '0.00'
  })
  // Loading tesseract.js functions
  await this.workerAut.load();
  // Loadingg language as 'English'
  await this.workerAut.loadLanguage('eng');
  await this.workerAut.initialize('eng');
  // Sending the File Object into the Recognize function to
  // parse the data
  const { data: { text } } = await this.workerAut.recognize(file.file);
  this.setState({
    isProcessingAut : false,
    auteurOCR : text
  })
};
async doOCR1(file) {
  this.setState({
      isProcessing1 : true,
      description : '',
      pctg1 : '0.00'
  })
  // Loading tesseract.js functions
  await this.worker1.load();
  // Loadingg language as 'English'
  await this.worker1.loadLanguage('eng');
  await this.worker1.initialize('eng');
  // Sending the File Object into the Recognize function to
  // parse the data
  const { data: { text } } = await this.worker1.recognize(file.file);
  this.setState({
      isProcessing1 : false,
      description : text
  })
};
async doOCR(file) {
  this.setState({
      isProcessing : true,
      titre : '',
      pctg : '0.00'
  })
  // Loading tesseract.js functions
  await this.worker.load();
  // Loadingg language as 'English'
  await this.worker.loadLanguage('eng');
  await this.worker.initialize('eng');
  // Sending the File Object into the Recognize function to
  // parse the data
  const { data: { text } } = await this.worker.recognize(file.file);
  this.setState({
      isProcessing : false,
      titre : text
  })
};
updateProgressAndLogAut(m){

  // Maximum value out of which percentage needs to be
  // calculated. In our case it's 0 for 0 % and 1 for Max 100%
  // DECIMAL_COUNT specifies no of floating decimal points in our
  // Percentage
  var MAX_PARCENTAGE = 1 ;
  var DECIMAL_COUNT = 2 ;

  if(m.status === "recognizing text"){
      var pctgAut = (m.progress / MAX_PARCENTAGE) * 100
      this.setState({
          pctgAut : pctgAut.toFixed(DECIMAL_COUNT)
      })

  }
}
updateProgressAndLog(m){

  // Maximum value out of which percentage needs to be
  // calculated. In our case it's 0 for 0 % and 1 for Max 100%
  // DECIMAL_COUNT specifies no of floating decimal points in our
  // Percentage
  var MAX_PARCENTAGE = 1 ;
  var DECIMAL_COUNT = 2 ;

  if(m.status === "recognizing text"){
      var pctg = (m.progress / MAX_PARCENTAGE) * 100
      this.setState({
          pctg : pctg.toFixed(DECIMAL_COUNT)
      })

  }
}
updateProgressAndLog1(m){

  // Maximum value out of which percentage needs to be
  // calculated. In our case it's 0 for 0 % and 1 for Max 100%
  // DECIMAL_COUNT specifies no of floating decimal points in our
  // Percentage
  var MAX_PARCENTAGE = 1 ;
  var DECIMAL_COUNT = 2 ;

  if(m.status === "recognizing text"){
      var pctg1 = (m.progress / MAX_PARCENTAGE) * 100
      this.setState({
          pctg1 : pctg1.toFixed(DECIMAL_COUNT)
      })

  }
}
async  onSubmitOuvrageManuellement(e){
      e.preventDefault();
      //if this.state.photoIsbn==''
    if(this.state.photoIsbn!=''){
      
        this.insererOuvrage(this.state.photoIsbn,this.state.titre,this.state.editeur,this.state.nombrepage,this.state.prix,this.state.langue,
          this.state.annee,this.state.dateEntree,this.state.quantite,this.state.nomauteur,this.state.origine,this.state.lieu,
          this.state.description,this.state.genre)
    }
    if(this.state.photoIsbn==''){
      let res =  this.uploadFile(this.state.file);
      if(this.state.auteurOCR!=''){
        Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+this.state.auteurOCR)
        .then(response=>{this.setState({idAuteurFromColumn:response.data.idAuteur})
        Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
        .then(res=>{
          if(res.data.message==null){
            //if genre miexiste=> getIdgenre
            this.setState({idGenreFromColumn:res.data.idGenre })
            const ouvrage={
              idAuteur:this.state.idAuteurFromColumn,
              titre:this.state.titre,
              editeur:this.state.editeur,
              nombrePage:this.state.nombrepage,
              prix:this.state.prix,
              idGenre:this.state.idGenreFromColumn,
              langue:this.state.langue,
              anneeEdition:this.state.annee,
              dateEntree:this.state.dateentree,
              quantite:this.state.quantite,
              etatActuel:'NL',
              origine:this.state.origine,
              lieuEdition:this.state.lieu,
              description:this.state.description,
              codeBarre:'',
              photo:this.state.filename
    
             }
            console.log(ouvrage)
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
            .then(res=>{ 
              if(res.data=='Ouvrage created successfully.'){
                    console.log(res.data)
                    window.alert('Ouvrage enregistré avec succès')
                    window.location.href="/dashboard"
                }
              else{
                window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
              }
          })
               
        }
        else{
          const genre={
            nomGenre:this.state.genre,
            description:''
           }
          console.log(genre)
          Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
          .then(resp=>{
                console.log(resp.data)
                Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
                .then(res1=>{
                    this.setState({idGenreFromColumn:res1.data.idGenre })
                    const ouvrage={
                      idAuteur:this.state.idAuteurFromColumn,
                      titre:this.state.titre,
                      editeur:this.state.editeur,
                      nombrePage:this.state.nombrepage,
                      prix:this.state.prix,
                      idGenre:this.state.idGenreFromColumn,
                      langue:this.state.langue,
                      anneeEdition:this.state.annee,
                      dateEntree:this.state.dateentree,
                      quantite:this.state.quantite,
                      etatActuel:'NL',
                      origine:this.state.origine,
                      lieuEdition:this.state.lieu,
                      description:this.state.description,
                      codeBarre:'',
                      photo:this.state.filename
            
                     }
                    console.log(ouvrage)
                    Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
                    .then(res=>{ 
                          if(res.data=='Ouvrage created successfully.'){
                                console.log(res.data)
                                window.alert('Ouvrage enregistré avec succès')
                                window.location.href="/dashboard"
                            }
                          else{
                            window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
                          }
                      })
                       
                       
                })
          });
        }
        
       
        })
      
        })
        .catch(error=>{
          console.log('auteur not found')
          const auteur={
            nomAuteur:this.state.auteurOCR,
            dateNaissance:'1900-01-01',
            nationalite:''
    
           }
          console.log(auteur)
          Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
          .then(res=>{
            console.log(res.data)
           //find idauteur where auteur
           Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+this.state.auteurOCR)
        .then(response=>{
          
          this.setState({idAuteurFromColumn:response.data.idAuteur})
        Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
        .then(res=>{
          if(res.data.message==null){
            //if genre miexiste=> getIdgenre
            this.setState({idGenreFromColumn:res.data.idGenre })
            const ouvrage={
              idAuteur:this.state.idAuteurFromColumn,
              titre:this.state.titre,
              editeur:this.state.editeur,
              nombrePage:this.state.nombrepage,
              prix:this.state.prix,
              idGenre:this.state.idGenreFromColumn,
              langue:this.state.langue,
              anneeEdition:this.state.annee,
              dateEntree:this.state.dateentree,
              quantite:this.state.quantite,
              etatActuel:'NL',
              origine:this.state.origine,
              lieuEdition:this.state.lieu,
              description:this.state.description,
              codeBarre:'',
              photo:this.state.filename
    
             }
            console.log(ouvrage)
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
            .then(res=>{ 
              if(res.data=='Ouvrage created successfully.'){
                    console.log(res.data)
                    window.alert('Ouvrage enregistré avec succès')
                    window.location.href="/dashboard"
                }
              else{
                window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
              }
          })
               
        }
        else{
          const genre={
            nomGenre:this.state.genre,
            description:''
           }
          console.log(genre)
          Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
          .then(resp=>{
                console.log(resp.data)
                Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
                .then(res1=>{
                    this.setState({idGenreFromColumn:res1.data.idGenre })
                    const ouvrage={
                      idAuteur:this.state.idAuteurFromColumn,
                      titre:this.state.titre,
                      editeur:this.state.editeur,
                      nombrePage:this.state.nombrepage,
                      prix:this.state.prix,
                      idGenre:this.state.idGenreFromColumn,
                      langue:this.state.langue,
                      anneeEdition:this.state.annee,
                      dateEntree:this.state.dateentree,
                      quantite:this.state.quantite,
                      etatActuel:'NL',
                      origine:this.state.origine,
                      lieuEdition:this.state.lieu,
                      description:this.state.description,
                      codeBarre:'',
                      photo:this.state.filename
            
                     }
                    console.log(ouvrage)
                    Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
                    .then(res=>{ 
                          if(res.data=='Ouvrage created successfully.'){
                                console.log(res.data)
                                window.alert('Ouvrage enregistré avec succès')
                                window.location.href="/dashboard"
                            }
                          else{
                            window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
                          }
                      })
                       
                       
                })
          });
        }
        
       
        })
      
        })
          
          });
          //console.log(error)
        }) 
      }
      if(this.state.auteurOCR==''){
        Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+this.state.auteur)
        .then(response=>{this.setState({idAuteurFromColumn:response.data.idAuteur})
      
        Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
        .then(res=>{
          if(res.data.message==null){
              //if genre miexiste=> getIdgenre
              this.setState({idGenreFromColumn:res.data.idGenre })
              const ouvrage={
                idAuteur:this.state.idAuteurFromColumn,
                titre:this.state.titre,
                editeur:this.state.editeur,
                nombrePage:this.state.nombrepage,
                prix:this.state.prix,
                idGenre:this.state.idGenreFromColumn,
                langue:this.state.langue,
                anneeEdition:this.state.annee,
                dateEntree:this.state.dateentree,
                quantite:this.state.quantite,
                etatActuel:'NL',
                origine:this.state.origine,
                lieuEdition:this.state.lieu,
                description:this.state.description,
                codeBarre:'',
                photo:this.state.filename
      
               }
              console.log(ouvrage)
              Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
              .then(res=>{ 
                if(res.data=='Ouvrage created successfully.'){
                      console.log(res.data)
                      window.alert('Ouvrage enregistré avec succès')
                      window.location.href="/dashboard"
                  }
                else{
                  window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
                }
            })
          }
          else{
            const genre={
              nomGenre:this.state.genre,
              description:''
             }
            console.log(genre)
            Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
            .then(resp=>{
                  console.log(resp.data)
                  Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
                  .then(res1=>{
                      this.setState({idGenreFromColumn:res1.data.idGenre })
                      const ouvrage={
                        idAuteur:this.state.idAuteurFromColumn,
                        titre:this.state.titre,
                        editeur:this.state.editeur,
                        nombrePage:this.state.nombrepage,
                        prix:this.state.prix,
                        idGenre:this.state.idGenreFromColumn,
                        langue:this.state.langue,
                        anneeEdition:this.state.annee,
                        dateEntree:this.state.dateentree,
                        quantite:this.state.quantite,
                        etatActuel:'NL',
                        origine:this.state.origine,
                        lieuEdition:this.state.lieu,
                        description:this.state.description,
                        codeBarre:'',
                        photo:this.state.filename
              
                       }
                      console.log(ouvrage)
                      Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
                      .then(res=>{ 
                        if(res.data=='Ouvrage created successfully.'){
                              console.log(res.data)
                              window.alert('Ouvrage enregistré avec succès')
                              window.location.href="/dashboard"
                          }
                        else{
                          window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
                        }
                    })
                  })
            });
          }
         

        })
      
        })
        .catch(error=>{
          console.log('auteur not found')
          const auteur={
            nomAuteur:this.state.auteur,
            dateNaissance:'1900-01-01',
            nationalite:''
    
           }
          console.log(auteur)
          Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
          .then(res=>{
            console.log(res.data)
           //find idauteur where auteur
           Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+this.state.auteur)
        .then(response=>{
          
          this.setState({idAuteurFromColumn:response.data.idAuteur})
        Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
        .then(res=>{
          if(res.data.message==null){
            //if genre miexiste=> getIdgenre
            this.setState({idGenreFromColumn:res.data.idGenre })
            const ouvrage={
              idAuteur:this.state.idAuteurFromColumn,
              titre:this.state.titre,
              editeur:this.state.editeur,
              nombrePage:this.state.nombrepage,
              prix:this.state.prix,
              idGenre:this.state.idGenreFromColumn,
              langue:this.state.langue,
              anneeEdition:this.state.annee,
              dateEntree:this.state.dateentree,
              quantite:this.state.quantite,
              etatActuel:'NL',
              origine:this.state.origine,
              lieuEdition:this.state.lieu,
              description:this.state.description,
              codeBarre:'',
              photo:this.state.filename
    
             }
            console.log(ouvrage)
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
            .then(res=>{ 
              if(res.data=='Ouvrage created successfully.'){
                    console.log(res.data)
                    window.alert('Ouvrage enregistré avec succès')
                    window.location.href="/dashboard"
                }
              else{
                window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
              }
          })
        }
        else{
          const genre={
            nomGenre:this.state.genre,
            description:''
           }
          console.log(genre)
          Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
          .then(resp=>{
                console.log(resp.data)
                Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.genre)
                .then(res1=>{
                    this.setState({idGenreFromColumn:res1.data.idGenre })
                    const ouvrage={
                      idAuteur:this.state.idAuteurFromColumn,
                      titre:this.state.titre,
                      editeur:this.state.editeur,
                      nombrePage:this.state.nombrepage,
                      prix:this.state.prix,
                      idGenre:this.state.idGenreFromColumn,
                      langue:this.state.langue,
                      anneeEdition:this.state.annee,
                      dateEntree:this.state.dateentree,
                      quantite:this.state.quantite,
                      etatActuel:'NL',
                      origine:this.state.origine,
                      lieuEdition:this.state.lieu,
                      description:this.state.description,
                      codeBarre:'',
                      photo:this.state.filename
            
                     }
                    console.log(ouvrage)
                    Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
                    .then(res=>{ 
                      if(res.data=='Ouvrage created successfully.'){
                            console.log(res.data)
                            window.alert('Ouvrage enregistré avec succès')
                            window.location.href="/dashboard"
                        }
                      else{
                        window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
                      }
                  })
                      
                })
          });
        }
       
       
        })
      
        })
          
          });
          //console.log(error)
        }) 
      }
    }
      //if photoisbn!=''-->photo:photoisbn
      
    
}
    creerOuvrage(ouvrage,titre,nomAuteur){
      if((titre=='' )||(nomAuteur=='')){
        this.setState({messageImportError:'Le titre ou l\'auteur de l\'ouvrage ne peut pas être nul'})
      }
      else{
          Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
          .then(res=>{ 
            if(res.data=='Ouvrage created successfully.'){
                this.setState({messageImport:'Enregistrement effectué pour:'+''+titre})
                  this.componentDidMount()
                
              }
            else{
              this.setState({messageImportError:'Erreur d\'enregistrement pour:'+titre})
              //window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
            }
          })
      }
  }
    insererOuvrage(photo,titre,editeur,nombrePage,prix,langue,annee,dateEntree,quantite,nomAuteur,origine,lieuEdition,description,nomGenre){
      if(nomAuteur!='undefined' && nomGenre!='undefined'){
            Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+nomAuteur)
            .then(response=>{this.setState({idAuteurFromColumn:response.data.idAuteur})
          
            Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
            .then(res=>{
              if(res.data.message==null){
                  //if genre miexiste=> getIdgenre
                  this.setState({idGenreFromColumn:res.data.idGenre })
                  const ouvrage={
                    idAuteur:this.state.idAuteurFromColumn,
                    titre:titre,
                    editeur:editeur,
                    nombrePage:nombrePage,
                    prix:prix,
                    idGenre:this.state.idGenreFromColumn,
                    langue:langue,
                    anneeEdition:annee,
                    dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                    quantite:quantite,
                    etatActuel:'NL',
                    origine:origine,
                    lieuEdition:lieuEdition,
                    description:description,
                    codeBarre:'',
                    photo:photo
          
                  }
                  console.log(ouvrage)
                   this.creerOuvrage(ouvrage,titre,nomAuteur)
              }
              else{
                const genre={
                  nomGenre:nomGenre,
                  description:''
                }
                console.log(genre)
                Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
                .then(resp=>{
                      console.log(resp.data)
                      Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
                      .then(res1=>{
                          this.setState({idGenreFromColumn:res1.data.idGenre })
                          const ouvrage={
                              idAuteur:this.state.idAuteurFromColumn,
                              titre:titre,
                              editeur:editeur,
                              nombrePage:nombrePage,
                              prix:prix,
                              idGenre:this.state.idGenreFromColumn,
                              langue:langue,
                              anneeEdition:annee,
                              dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                              quantite:quantite,
                              etatActuel:'NL',
                              origine:origine,
                              lieuEdition:lieuEdition,
                              description:description,
                              codeBarre:'',
                              photo:photo
                  
                          }
                          console.log(ouvrage)
                          this.creerOuvrage(ouvrage,titre,nomAuteur)
                      })
                });
              }
            

            })
          
            })
            .catch(error=>{
              console.log('auteur not found')
              const auteur={
                nomAuteur:nomAuteur,
                dateNaissance:'1900-01-01',
                nationalite:''
        
              }
              console.log(auteur)
              Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
              .then(res=>{
                console.log(res.data)
              //find idauteur where auteur
              Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+nomAuteur)
            .then(response=>{
              
              this.setState({idAuteurFromColumn:response.data.idAuteur})
            Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
            .then(res=>{
              if(res.data.message==null){
                //if genre miexiste=> getIdgenre
                this.setState({idGenreFromColumn:res.data.idGenre })
                          const ouvrage={
                              idAuteur:this.state.idAuteurFromColumn,
                              titre:titre,
                              editeur:editeur,
                              nombrePage:nombrePage,
                              prix:prix,
                              idGenre:this.state.idGenreFromColumn,
                              langue:langue,
                              anneeEdition:annee,
                              dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                              quantite:quantite,
                              etatActuel:'NL',
                              origine:origine,
                              lieuEdition:lieuEdition,
                              description:description,
                              codeBarre:'',
                              photo:photo
        
                            }
                console.log(ouvrage)
                this.creerOuvrage(ouvrage,titre,nomAuteur)
            }
            else{
              const genre={
                nomGenre:nomGenre,
                description:''
              }
              console.log(genre)
              Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
              .then(resp=>{
                    console.log(resp.data)
                    Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
                    .then(res1=>{
                        this.setState({idGenreFromColumn:res1.data.idGenre })
                            const ouvrage={
                                  idAuteur:this.state.idAuteurFromColumn,
                                  titre:titre,
                                  editeur:editeur,
                                  nombrePage:nombrePage,
                                  prix:prix,
                                  idGenre:this.state.idGenreFromColumn,
                                  langue:langue,
                                  anneeEdition:annee,
                                  dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                                  quantite:quantite,
                                  etatActuel:'NL',
                                  origine:origine,
                                  lieuEdition:lieuEdition,
                                  description:description,
                                  codeBarre:'',
                                  photo:photo
                    
                            }
                        console.log(ouvrage)
                        this.creerOuvrage(ouvrage,titre,nomAuteur)
                          
                    })
              });
            }
          
          
            })
          
            })
              
              });
              //console.log(error)
            }) 
      }
     
      else{
        console.log('Misy tsisy na tsisy daoly ny ateur na genre')
      }
      //getIdAuteur
      //getIdGenre
      //inserer

    }
    importerExcel(){
      this.setState({open1:false})
      if(this.state.rows.length>0){
      let i=1;
      let j=0;
        for (i = 1; i <this.state.rows.length; i++) {
          var string =this.state.rows[i] + '';
          var tab = string.split(",");
          //tab->1 à 10
          let ouvrage={
            idAuteur:tab[2],
            titre:tab[1],
            editeur:tab[4],
            nombrePage:tab[5],
            prix:tab[6],
            idGenre:tab[3],
            langue:tab[12],
            anneeEdition:tab[7],
            dateEntree:tab[11],
            quantite:tab[8],
            etatActuel:'NL',
            origine:tab[10],
            lieuEdition:tab[9],
            description:tab[12],
            codeBarre:'',
            photo:''
    
           }
           this.insererOuvrage('',tab[1],tab[4],tab[5],tab[6],tab[12],tab[7],tab[11],tab[8],tab[2],tab[10],tab[9],tab[12],tab[3])
           
          }
      }
      else{
       alert('Veuillez choisir un fichier Excel avant d\'importer!')
      }
     
    }
    reset(){
      this.setState({finalTranscript:'',message:'',messageEmailUpload:'',messageSucess:'',fileupload:'',
      auteur:'',
      auteurOCR:'',
      photoIsbn:'',
      open1:'',
      isProcessingAut : false,
      pctgAut : '0.00',
      isProcessing : false,
      isProcessing1 : false,
      ocrText : '',
      pctg : '0.00',
      pctg1 : '0.00',
      titre:'',
      editeur:'',
      nombrepage:1,
      prix:1,
      genre:'',
      langue:'',
      annee:'',
      dateentree:Date.now(),
      quantite:1,
      origine:'',
      lieu:'',
      description:'',
      nomauteur:'',
      datenaissance:'',
      nationalite:'',
      autrelangue:'',
      genres:[],
      auteurs:[],
      langues:[{ typelangue: 'Anglais'},{ typelangue: 'Français'},{ typelangue: 'Italien'}],
      idGenreFromColumn:'',
      idAuteurFromColumn:'',
      idAuteurFromColumn1:'',
      idTemp:'',
      tempgenres:[],
      pictures: [],
      photoOuvrage:'',
      file:null,
      filename:null,
      listening: false,
      listeningEdt: false,
      bgColor: '#4b63db',
      bgColor1: '#4b63db',
      cols:[],
      rows:[],
      messageImport:'',
      messageImportError:'',
      bookInfo:'',
      isbn:'',
    
    
    })
      this.componentDidMount()
    }
    async uploadFile(file){
      if(file!=null){
        const data = new FormData();
        var originalfilename = file.name.split(".");
        data.append('file',file)
        data.append('upload_preset','insta-clone')
        data.append('cloud_name','ITUnivesity')
        
    
        fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
            method:'post',
            body:data
        })
        .then(res=>res.json())
        .then(data=>{
          this.setState({filename:data.url})
          console.log(data)
        })
        .catch(err=>{
          console.log(err)
        })
      }
    }
    onSubmitOuvrageAuto(e){
      e.preventDefault();
      window.location='/dashboard';
    }
    
    onChangeIsbn(e){
      console.log("isbn="+e.target.value)
       this.setState({
        isbn:e.target.value
      });
    }
 
    onSubmitAuteur(e){
      e.preventDefault();
      const auteur={
        nomAuteur:this.state.nomauteur,
        dateNaissance:this.state.datenaissance,
        nationalite:this.state.nationalite

       }
      console.log(auteur)
      Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
      .then(res=>console.log(res.data));
      alert('Le auteur a été crée avec succès!')
      window.location='/auteur';
    }
    searchBookByISBN(){
      this.fetchData(this.state.isbn)
    }
    componentDidMount(){
    
      this.worker = createWorker({
        logger: m => this.updateProgressAndLog(m),
    });
    this.worker1 = createWorker({
      logger: m => this.updateProgressAndLog1(m),
  });
  this.workerAut = createWorker({
    logger: m => this.updateProgressAndLogAut(m),
});
      Axios.get('https://phpapiserver.herokuapp.com/api/genre/read.php')
      .then(response=>{
        this.setState({genres:response.data})
        
      })
      Axios.get('https://phpapiserver.herokuapp.com/api/auteur/read.php')
      .then(response=>{
        this.setState({auteurs:response.data})
        
      })
    }
  render(){
    let messageUpload;
    let messageUpload1;
  
  let isbnAut;
  let isbnGenre;
  let inputgenreOCR;
    if(this.state.messageImportError!=''){
      messageUpload= <span style={{color:'red',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-exclamation-triangle"></i>&nbsp;{this.state.messageImportError}</span>
    }
    if(this.state.messageImport!=''){
      messageUpload1= <span style={{color:'green',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-check"></i>&nbsp;{this.state.messageImport}</span>
    }
    let inputauteurOCR;
    if(this.state.isbn!=''){
      isbnAut=<TextareaAutosize aria-label="minimum height" rowsMin={1}  style={{marginLeft:'5px',marginRight:'5px',display: 'block',width: '100%',overflow: 'hidden',resize: 'both',minHeight: '40px',lineHeight: '20px'}}  type="text" required className="form-control" id="auteur" placeholder="Auteur de l'ouvrage..."
       value={this.state.nomauteur} 
       onPaste={this.onChangenomauteur} 
       onChange={this.onChangenomauteur}/>
    }
    if(this.state.isbn!=''){
      isbnGenre=<TextareaAutosize aria-label="minimum height" rowsMin={1}  style={{marginLeft:'5px',marginRight:'5px',display: 'block',width: '100%',overflow: 'hidden',resize: 'both',minHeight: '40px',lineHeight: '20px'}}  type="text" required className="form-control" id="auteur" placeholder="Auteur de l'ouvrage..."
       value={this.state.genre} 
       onPaste={this.onChangegenre} 
       onChange={this.onChangegenre}/>
    }
    
    if(this.state.auteurOCR!=''){
      inputauteurOCR=<TextareaAutosize aria-label="minimum height" rowsMin={1}  style={{marginLeft:'5px',marginRight:'5px',display: 'block',width: '100%',overflow: 'hidden',resize: 'both',minHeight: '40px',lineHeight: '20px'}}  type="text" required className="form-control" id="auteur" placeholder="Auteur de l'ouvrage..." value={this.state.auteurOCR} onPaste={this.onChangeauteurOCR} onChange={this.onChangeauteurOCR}/>
    }
  
    if((this.state.auteurOCR=='')&& (this.state.isbn=='')){
      inputauteurOCR=<Autocomplete   
      id="combo-box-demo"
      freeSolo
      options={this.state.auteurs}
      getOptionLabel={(option) => option.nomAuteur}
      fullWidth
    
      renderInput={(params) =>  
      <TextField style={{marginLeft:'5px'}} required fullWidth id="outlined-basic" {...params} placeholder="Ajouter un auteur"  
          InputProps={{  ...params.InputProps, type: 'search',style: { fontSize: 13 }}}
          variant="outlined"
          value={this.state.auteur}
          onChange={(e)=>this.setState({auteur:e.target.value})}
          onSelect={(e)=>this.setState({auteur:e.target.value})}
      />
    
    }
   />
    }
    if(this.state.isbn==''){
      inputgenreOCR=<Autocomplete   
      id="combo-box-demo"
      options={this.state.genres}
      freeSolo
      getOptionLabel={(option) => option.nomGenre}
      fullWidth
      renderInput={(params) =>  <TextField required fullWidth id="outlined-basic" {...params} placeholder="Ajouter un genre"  
       InputProps={{  ...params.InputProps, type: 'search',style: { fontSize: 13 }}}
      
      variant="outlined"
      value={this.state.genre}
       onChange={(e)=>this.setState({genre:e.target.value})}
       onSelect={(e)=>this.setState({genre:e.target.value})}
     
   
      />
    
    }
   />
    }
    
  return (
    <div className="content-wrapper">
      <section className="content-header">
          <h1>
            Ajouter un ouvrage
          
          </h1>
          <ol className="breadcrumb">
            <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
          
          </ol>
    </section>
    <section className="content" >
        <div className="row" >
        <div className="col-md-5">
            <div className="box box-warning">
                <div className="box-header">
                {messageUpload}{messageUpload1}
                  <h4 style={{color:'#043f7a'}}>Via import Excel</h4>
                
                  <div style={{borderRadius:'5px',width:'500px',border:'1px solid #dedcdc',padding:'7px 7px',display:'inline-block'}}>
                        <span style={{fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}>Importer un fichier Excel (*.xlsx)</span>
                        
                        <input 
                        value={this.state.fileupload}
                          type="file" 
                          onChange={this.fileHandler.bind(this)} 
                          style={{"padding":"10px"}} 
                          accept=".xlsx" 
                        />
                         &nbsp; <button className="btn btn-danger" onClick={this.reset}><i className="fa fa-close"></i></button>&nbsp;
                         &nbsp; <button className="btn btn-primary" onClick={this.importerExcel}><i className="fa fa-upload"></i>&nbsp;Importer</button>
                        
                        <br/><br/>
                        <Link style={{fontWeight:'bold',fontFamily:'Arial',fontSize:'13px'}} to="/dashboard"><i className="fa fa-book"></i>&nbsp;Voir les ouvrages enregistrés</Link>
                  </div>
                
                </div>  
                <div className="box-header">
               
                  <h4 style={{color:'#043f7a'}}>Via ISBN ou scanner un code-barres</h4>
                
                  <div style={{borderRadius:'5px',border:'1px solid #dedcdc',padding:'7px 7px'}}>
                        <span style={{fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}>Entrer ici le code ISBN du livre à insérer</span>
                        <br/>
                        <input 
                          value={this.state.isbn}
                          type="text" 
                          onChange={this.onChangeIsbn} 
                          style={{"padding":"3px 3px"}} 
                          placeholder="Entrer ici le code ISBN..."
                          id="langue"
                        />
                            &nbsp;<button className="btn btn-success" onClick={this.searchBookByISBN}><i className="fa fa-check"></i></button>
                            &nbsp;<button className="btn btn-danger" onClick={this.reset}><i className="fa fa-close"></i></button>&nbsp;&nbsp;
                        
                       
                 
                  {this.state.result}
                  {(()=>{
                                if(this.state.openCamera==true){
                                    return(
                                            <div style={{padding:'10px 10px'}}>
                                                <Tooltip title="Arrêter le scanner" aria-label="Arrêter le scanner">
                                                    <Fab style={{backgroundColor:'#ed5d4a',color:'white'}} onClick={this.stop}>
                                                        <StopIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:400,height:400}}>
                                                 <BarcodeScannerComponent
                                                    width={500}
                                                    format="EAN-13"
                                                    height={500}
                                                    
                                                    onUpdate={(err, result) => {
                                                    if (result){
                                                       this.setState({isbn:result.text,result:'Ouvrage trouvé!'})
                                                       this.fetchData(result.text)
                                                    }
                                                    else {
                                                        this.setState({result:'Non trouvé'})
                                                    }
                                                    }}
                                                />
                                               </Avatar>
                                            </div>
                                        )
                                }  
                                if(this.state.openCamera==false){
                                    return(
                                            <div style={{padding:'10px 10px'}}>
                                                <Tooltip title="Démarrer le scanner" aria-label="Démarrer le scanner">
                                                    <Fab style={{backgroundColor:'#488a3b',color:'white'}} onClick={this.demarrer}>
                                                        <CameraAltIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:400,height:400}}>
                                                    <h4 style={{color:'#343634'}}>Démarrer le scanner et approcher le code-barres</h4>
                                                </Avatar>
                                            </div>
                                        )
                                }
                            }
                        )()}
                  <br/>
                  </div>
                </div>  
            </div>                    
        </div>
        
            <div className="col-md-6" style={{border:'1px solid #c3c5c7',borderRadius:'5px',backgroundColor:'white'}}>
            <div className="box box-warning">
            <div className="box-header">
              <h4 style={{color:'#043f7a'}}>Via Saisie manuelle ou avec OCR ou micro </h4>
            </div>
            <form role="form" onSubmit={this.onSubmitOuvrageManuellement}>
                  <div className="box-body">
                    <p className="text-muted">Pour ajouter un nouveau ouvrage, veuillez remplir les champs suivants:</p>
                    <div className="form-group" style={{borderRadius:'5px',border:'1px solid #e3e1e1'}}>
                     
                     <div className="row">
                        <div className="col-md-7">
                        <i className={"fas fa-sync fa-2x " + (this.state.isProcessingAut ? "fa-spin" : "")}></i> <span className="status-text">{this.state.isProcessingAut ? `Traitement de l'image ( ${this.state.pctgAut} % )` : ""} </span> 
                        <label style={{marginLeft:'5px'}}>Nom de l'auteur</label>
                       {inputauteurOCR}{isbnAut}
                        </div>
                        <div className="col-md-5">
                         
                        <label style={{marginLeft:'5px'}}>Via OCR</label>
                          <FilePond fontSize=""  style={{fontSize:'20px',marginRight:'5px'}}  ref={ref => this.pond = ref}
                                onaddfile={(err,file) =>{
                                    this.doOCRAut(file);

                                }}
                                onremovefile={(err,fiile) =>{
                                    this.setState({
                                      auteurOCR : ''
                                    })
                                }}
                                labelIdle='Faire glisser & déposez vos fichiers ou <u className="filepond--label-action">Parcourir</u>'
                                />
                         </div>
                     </div>
                      
                    </div>
                    <div>
                    <div className="form-group" style={{borderRadius:'5px',border:'1px solid #e3e1e1'}}>
                   
                      <div className="row" >
                        <div className="col-md-6">
                        <label style={{marginLeft:'5px'}}>Titre</label>
                       <i className={"fas fa-sync fa-2x " + (this.state.isProcessing ? "fa-spin" : "")}></i> <span className="status-text">{this.state.isProcessing ? `Traitement de l'image ( ${this.state.pctg} % )` : ""} </span> 
                         <TextareaAutosize aria-label="minimum height" rowsMin={1}  style={{marginLeft:'5px',marginRight:'5px',display: 'block',width: '100%',overflow: 'hidden',resize: 'both',minHeight: '40px',lineHeight: '20px'}}  type="text" required className="form-control" id="titre" placeholder="Titre de l'ouvrage..." value={this.state.titre} onPaste={this.onChangetitre} onChange={this.onChangetitre}/>
                        
                        </div>
                        <div className="col-md-1"><br/><br/>
                           <MicIcon onClick={this.toggleListen} style={{color:this.state.bgColor,cursor:'pointer'}} fontSize="large"></MicIcon>
                        </div>
                        <div className="col-md-5">
                       
                        <label style={{marginLeft:'5px'}}>Via OCR</label>
                          <FilePond fontSize=""  style={{fontSize:'20px',marginRight:'5px'}}  ref={ref => this.pond = ref}
                                onaddfile={(err,file) =>{
                                    this.doOCR(file);

                                }}
                                onremovefile={(err,fiile) =>{
                                    this.setState({
                                      titre : ''
                                    })
                                }}
                                labelIdle='Faire glisser & déposez vos fichiers ou <u className="filepond--label-action">Parcourir</u>'
                                />
                         </div>
                      </div>
                    </div>
                    </div>
                    <div className="form-group">
                      <label >Editeur</label>
                       <div className="row">
                         <div className="col-md-11">
                         <input type="text"  className="form-control" id="editeur" placeholder="Editeur..." value={this.state.editeur} onChange={this.onChangeediteur}/>   
                         </div>
                         <div className="col-md-1">
                         <MicIcon onClick={this.toggleListenEdt} style={{color:this.state.bgColor1,cursor:'pointer'}} fontSize="large"></MicIcon>
                         </div>
                       </div>
                     
                    </div>
                    <div className="form-group">
                      <label>Nombre de page</label>
                      <input type="number" required className="form-control" id="nombrepage" value={this.state.nombrepage} onChange={this.onChangenombrepage}/>
                    </div>
                    <div className="form-group">
                      <label >Prix</label>
                      <input type="number" className="form-control" id="prix" value={this.state.prix} onChange={this.onChangeprix}/>
                    </div>
                    <div class="form-group">
                      <label>Genre</label>
                     
                      {isbnGenre}{inputgenreOCR}
                   
                  
                    </div>
                    <div class="form-group">
                      <label>Langue</label>
                      <input type="text" className="form-control"
                        onChange={(e)=>this.setState({langue:e.target.value})}
                        value={this.state.langue}       
                      />
                   
                    
                    </div>

                    <div className="form-group">
                      <label >Année d'édition:</label>
                      <input type="number" min="1900" max="2099" step="1" className="form-control" id="annee" value={this.state.annee} onChange={this.onChangeannee}/>
                    </div>
                    <div className="form-group">
                      <label >Date d'entrée dans la bibliothèque:</label>
                      <input required type="datetime-local" className="form-control" id="dateentree"  onChange={(event) => this.setState({dateentree: event.target.value})}/>
                      
                    </div>
                    <div className="form-group">
                      <label>Quantité du livre:</label>
                      <input required type="number" min="1" className="form-control" id="quantite" value={this.state.quantite} onChange={this.onChangequantite} />
                    </div>
                    <div className="form-group">
                      <label >Origine</label>
                      <Autocomplete  
                         
                         freeSolo
                         id="free-solo-2-demo"
                         disableClearable
                         options={this.state.lieux.map((l) => l.label)}
                         renderInput={(params) => (
                           <TextField   className="form-control"   
                             {...params}
                             placeholder="Choisir ou saisir l'origine ..."
                     
                             margin="normal"
                             variant="outlined"
                             InputProps={{ ...params.InputProps, type: 'search',style: { fontSize: 13 }}}
                             value={this.state.origine}
                             onSelect={(e)=>this.setState({origine:e.target.value})}
                             onChange={(e)=>this.setState({origine:e.target.value})}
                             onPaste={(e) => this.setState({origine:e.target.value})}
                         />
                       )}
                     />
                    
                    </div>
                    <div className="form-group">
                      <label >Lieu d'édition</label>
                      <Autocomplete  
                         
                         freeSolo
                         id="free-solo-2-demo"
                         disableClearable
                         options={this.state.lieux.map((l) => l.label)}
                         renderInput={(params) => (
                           <TextField   className="form-control"   
                             {...params}
                             placeholder="Choisir ou saisir le lieu d'édition ..."
                             margin="normal"
                             variant="outlined"
                             InputProps={{ ...params.InputProps, type: 'search',style: { fontSize: 13 }}}
                             value={this.state.lieu}
                             onSelect={(e)=>this.setState({lieu:e.target.value})}
                             onChange={(e)=>this.setState({lieu:e.target.value})}
                             onPaste={(e) => this.setState({lieu:e.target.value})}
                         />
                       )}
                     />
                   
                    </div>
                   
                    <div className="form-group" style={{borderRadius:'5px',border:'1px solid #e3e1e1'}}>  
                   
                        <div className="row" >
                        <div className="col-md-7">
                        <label style={{marginLeft:'5px'}}>Description</label>
                       <i className={"fas fa-sync fa-2x " + (this.state.isProcessing1 ? "fa-spin" : "")}></i> <span className="status-text">{this.state.isProcessing1 ? `Traitement de l'image ( ${this.state.pctg1} % )` : ""} </span> 
                         <TextareaAutosize aria-label="minimum height" rowsMin={1}  placeholder="Mettre la description ici" style={{marginLeft:'5px',display: 'block',width: '100%',overflow: 'hidden',resize: 'both',minHeight: '40px',lineHeight: '20px'}} className="form-control" value={this.state.description} onChange={this.onChangedescription}/>
                        </div>
                        <div className="col-md-5">
                          <br/>
                        <label style={{marginLeft:'5px'}}>Via OCR</label>
                          <FilePond fontSize=""  style={{fontSize:'20px',marginRight:'5px'}}  ref={ref => this.pond = ref}
                                onaddfile={(err,file) =>{
                                    this.doOCR1(file);

                                }}
                                onremovefile={(err,fiile) =>{
                                    this.setState({
                                      description : ''
                                    })
                                }}
                                labelIdle='Faire glisser & déposez vos fichiers ou <u className="filepond--label-action">Parcourir</u>'
                                />
                         
                        </div>
                      </div>
              
                    </div>
                    <div className="form-group">
                      <label for="exampleInputFile">Photo de couverture du livre</label>
                      <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>

                    </div>
                    
                  </div>

                  <div class="box-footer">
                    
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
            </form>
            </div>
            </div>
           
            <div className="col-md-1"></div>
       
        

      </div>
    </section>
    <Dialog fullWidth={true}
          maxWidth = {'xl'} open={this.state.open1} onClose={this.handleClose1} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" style={{backgroundColor:'#016917',color:'white'}}>
          <h4> <CloseIcon style={{cursor:'pointer'}} onClick={this.handleClose1}/>&nbsp;&nbsp;Visualisation du fichier Excel </h4>
        
        </DialogTitle>
          <DialogContent dividers>
              <h5 style={{color:'#d16d0f',fontWeight:'bold',fontFamily:'Arial',fontSize:'13px'}}><i className="fa fa-exclamation-triangle"></i>&nbsp;Votre fichier Excel doit suivre les ordres des colonnes suivantes:</h5>
              <img src="assets/dist/img/modelOuvrageExcel.JPG"/>
              <h5 style={{color:'#525150',fontWeight:'bold',fontFamily:'Arial',fontSize:'13px'}}><i className="fa fa-arrow-right"></i>&nbsp;Le fichier Excel à importer dans la base:</h5>
             
              <OutTable data={this.state.rows} 
                  columns={this.state.cols} 
              
                 
              />  
          </DialogContent>
          <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose1} color="primary">
		  	    <h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          <Button onClick={this.importerExcel} style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
            <h6 style={{"color":"white","fontFamily":"Arial"}}>Importer</h6>
          </Button>
        </DialogActions>
      
       
          <br/>
      </Dialog>
  </div>
   
  );
}
}

