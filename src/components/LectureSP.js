import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Paper from '@material-ui/core/Paper';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import {Link} from 'react-router-dom';
import './style.css';
import Moment from 'moment';
import 'moment/locale/fr';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import BarcodeScannerComponent from "react-webcam-barcode-scanner";
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import StopIcon from '@material-ui/icons/Stop';
import ReactToExcel from 'react-html-table-to-excel';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

export default class LectureSP extends Component {
  constructor(props) {
    super(props)
    this.demarrer=this.demarrer.bind(this)  
    this.stop=this.stop.bind(this) 
    this.onChangeuserid=this.onChangeuserid.bind(this)
    this.onChangebookid=this.onChangebookid.bind(this)
    this.onChangedate=this.onChangedate.bind(this)
    this.onChangeheure=this.onChangeheure.bind(this)
    this.delete=this.delete.bind(this); 
    this.modifier=this.modifier.bind(this);
    this.enregistrer=this.enregistrer.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeamodifierMembreid=this.onChangeamodifierMembreid.bind(this)
    this.handleClose=this.handleClose.bind(this);
    this.onChangeamodifieridouvrage=this.onChangeamodifieridouvrage.bind(this)
    this.onChangeamodifierDateL=this.onChangeamodifierDateL.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this);
    this.searchHandler=this.searchHandler.bind(this); 

		this.state={
       userid:'',
       openCamera:false,
       data:'Veuillez cliquer sur l\'icône Caméra pour commencer le scan et son résulat',
       bookid:'',
       bookid1:'',
       date:'',
       heure:'',
       ouvrages:[],
       lectureSP:[],
       open:false,
      open1:false,
       amodifier:[],
       amodifierMembreid:'',
       amodifieridouvrage:'',
       amodifierDateL:'',
       titre:'',
       nom:'',
       getidlsp:'', 
       message:'',
       offset: 0,
       perPage: 10,
       currentPage: 0,
       idMembre:'',
       idOuvrage:'',
       erreur:'',
       hidden:false,
       lspHidden:[]

      };
    
    }
    demarrer(){
      this.setState({openCamera:true})
    }
    stop(){
       this.setState({openCamera:false,data:'Non trouvé'})
    }
    handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState({
          currentPage: selectedPage,
          offset: offset
      }, () => {
          this.componentDidMount()
      });

  };
  searchHandler(e){
    var test=' résultat(s) trouvé(s)'
    Axios.post('https://phpapiserver.herokuapp.com/api/lectureSurPlace/search.php',{wordToSearch:e.target.value})
    .then(response=>{
      if(response.data.message!='Could not find data.'){
        const data = response.data;
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
        const postdata = slice.map(lsp=>
          <tr>
              <td data-label="N°: " >&nbsp; <span class="label label-warning">{lsp.idMembre}</span></td>
            <td data-label="Photo:"  ><Avatar variant="rounded"  alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${lsp.photo}`} /></td>
            <td data-label="Nom: "  >&nbsp; <Link to={`/detailMembre/${lsp.idMembre}`}>{lsp.nom}&nbsp;{lsp.prenom}</Link></td>
            <td data-label="Auteur: ">&nbsp;{lsp.titre} par {lsp.nomAuteur}</td>
            <td  data-label="Date: " > &nbsp; {Moment(lsp.dateLecture).locale("de").format('dddd Do MMMM YYYY')}&nbsp;</td>
            <td data-label="Heure: ">&nbsp; {Moment(lsp.dateLecture).locale("de").format('HH:mm')}&nbsp;</td>
            <td >&nbsp;
                <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                  <EditIcon fontSize="large"  style={{"color":"#4a4d49"}}  onClick={() => this.modifier(lsp.idLectureSurPlace,lsp.titre,lsp.nom)}/> 
                </Fab> 
             </td>
              <td  >
                <Fab size="small" style={{"backgroundColor":"#e02f2f"}}aria-label="add" title="Suppimer cette ligne">
                  <DeleteIcon fontSize="large"  style={{"color":"#ffffff"}} onClick={() => this.delete(lsp.idLectureSurPlace)}/> 
                </Fab>
                
            </td>
          </tr>)
    
        this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
      }
      else{
        this.setState({message:'Aucun'+test,ouvrages:[]})
      }
    })
  }
    handleClose(){
      this.setState({open:false})
      };
    onChangeuserid(e){
      this.setState({
        userid:e.target.value
      });
    }
    onChangebookid(e){
      this.setState({
        bookid:e.target.value
      });
    }
    onChangedate(e){
      this.setState({
        date:e.target.value
      });
    }
    onChangeheure(e){
      this.setState({
        heure:e.target.value
      });
    }
    modifier(id,titre,nom){
      this.setState({open:true})
      console.log(id)
      Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/single_read.php/?idLectureSurPlace='+id)
      .then(response=>{
   
        this.setState({
          getidlsp:response.data.idLectureSurPlace,
          amodifierMembreid:response.data.idMembre,
          amodifieridouvrage:response.data.idOuvrage,
          amodifierDateL:response.data.DateLecture,
          titre:titre,
          nom:nom
        })
       
      })
      .catch(error=>{
        console.log(error)
      }) 
     
    };
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/lectureSurPlace/deleteOne.php',{idLectureSurPlace:id})
      .then(res =>{
        console.log(res.data)
        this.componentDidMount()
      });
     
       
     } 
     else{}
    }
    enregistrer(e){
      e.preventDefault();
      if(this.state.bookid1!=''){
        Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClm.php/?titre='+this.state.bookid1)
        .then(response=>{
      
          const lsp={
            idMembre:this.state.userid,
            idOuvrage:response.data.idOuvrage,
            DateLecture:this.state.dateentree
    
           }
          console.log(lsp)
       Axios.post('https://phpapiserver.herokuapp.com/api/lectureSurPlace/create.php',lsp)
          .then(res=>console.log(res.data));
          
          //Update etat actuel de l'ouvrage 
          const ouvrageUpdate={
            idOuvrage:response.data.idOuvrage,
            etatActuel: "L"
    
           }
          console.log(ouvrageUpdate)
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/updateEtatActuel.php',ouvrageUpdate)
          .then(res=>console.log(res.data));
          //afficher les fiches de lectures
          Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/ficheLectureSP.php')
          .then(response=>{
            this.setState({lectureSP:response.data})
            window.alert('enregistrement avec succès!')
            this.componentDidMount()
          })
        
         
          
        
         })
        .catch(error=>{
          window.alert('Echec d\'enregistrement!')
        })
      }
      if(this.state.bookid!=''){
        const lsp={
          idMembre:this.state.userid,
          idOuvrage:this.state.bookid,
          DateLecture:this.state.dateentree
  
         }
         Axios.post('https://phpapiserver.herokuapp.com/api/lectureSurPlace/create.php',lsp)
         .then(res=>console.log(res.data));
         
         //Update etat actuel de l'ouvrage 
         const ouvrageUpdate={
           idOuvrage:this.state.bookid,
           etatActuel: "L"
   
          }
         console.log(ouvrageUpdate)
       Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/updateEtatActuel.php',ouvrageUpdate)
         .then(res=>console.log(res.data));
         //afficher les fiches de lectures
         Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/ficheLectureSP.php')
         .then(response=>{
           this.setState({lectureSP:response.data})
           window.alert('enregistrement avec succès!')
           this.componentDidMount()
         })
        
         
      }
      if(this.state.bookid==''){
        this.setState({erreur:'Veuillez entrer un numéro de l\'ouvrage'})
      }
      if(this.state.bookid1==''){
        this.setState({erreur:'Veuillez choisir le titre'})
      }
     
     
    };
    onChangeamodifierMembreid(e){
      this.setState({
        amodifierMembreid:e.target.value
      });
    }
    onChangeamodifieridouvrage(e){
      this.setState({
        amodifieridouvrage:e.target.value
      });
    }
    onChangeamodifierDateL(e){
      this.setState({
        amodifierDateL:e.target.value
      });
    }
    componentDidMount(){
      Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/read.php')
      .then(response=>{
        this.setState({ouvrages:response.data})
        
      })
      Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/ficheLectureSP.php')
      .then(response=>{
        this.setState({lspHidden:response.data})
      })
      Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/ficheLectureSP.php')
      .then(response=>{
          const data = response.data;
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
        const postdata = slice.map(lsp=>
          <tr>
              <td data-label="Numéro Etudiant: ">&nbsp; <span class="label label-warning">{lsp.idMembre}</span></td>
            <td data-label="Photo:"  ><Avatar variant="rounded"  alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${lsp.photo}`} /></td>
            <td data-label="Nom: " >&nbsp; <Link to={`/detailMembre/${lsp.idMembre}`}>{lsp.nom}&nbsp;{lsp.prenom}</Link></td>
            <td data-label="Auteur: " >&nbsp;{lsp.titre} par {lsp.nomAuteur}</td>
            <td  data-label="Date: "> &nbsp; {Moment(lsp.dateLecture).locale("de").format('dddd Do MMMM YYYY')}&nbsp;</td>
            <td data-label="Heure: ">&nbsp; {Moment(lsp.dateLecture).locale("de").format('HH:mm')}&nbsp;</td>
            <td>&nbsp;
                <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                  <EditIcon fontSize="large"  style={{"color":"#4a4d49"}}  onClick={() => this.modifier(lsp.idLectureSurPlace,lsp.titre,lsp.nom)}/> 
                </Fab> 
             </td>
              <td>
                <Fab size="small" style={{"backgroundColor":"#e02f2f"}}aria-label="add" title="Suppimer cette ligne">
                  <DeleteIcon fontSize="large"  style={{"color":"#ffffff"}} onClick={() => this.delete(lsp.idLectureSurPlace)}/> 
                </Fab>
                
            </td>
          </tr>)
     this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
        
    })
    }
    onSubmit(e){
      e.preventDefault();
     // console.log("id="+this.state.getidAuteur+"nom"+this.state.amodifierNom+" desc="+this.state.amodifierNationalite+"date="+this.state.amodifierDateN)
      const amodifierLSP={
        idLectureSurPlace:this.state.getidlsp,
        idMembre:this.state.amodifierMembreid,
        idOuvrage:this.state.amodifieridouvrage,
        DateLecture:this.state.amodifierDateL
       
        
      }
     console.log(amodifierLSP)
      Axios.post('https://phpapiserver.herokuapp.com/api/lectureSurPlace/update.php',amodifierLSP)
      .then(response=>{
         if(response){
          Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/ficheLectureSP.php')
          .then(response=>{this.setState({lectureSP:response.data}) })
          this.setState({open:false})
         
          }
          else{
              this.setState({open:true}) 
          }
         })
        
       
  
    }
  render(){
    let tablehidden;
    if(this.state.hidden==false){
     tablehidden=<Accordion style={{padding:'5px 5px'}}>
                     <AccordionSummary
                           expandIcon={<ExpandMoreIcon />}
                           aria-controls="panel1a-content"
                           id="panel1a-header"
                         >
                         <Typography ><span style={{fontSize:'13px',fontFamily:'Arial'}}>©RYN 2020&nbsp;&nbsp;&nbsp;Voir  liste complète des ouvrages</span></Typography>
                     </AccordionSummary>
                     <AccordionDetails>
                         <Typography>
                         <table style={{width:'100%',fontFamily:'Arial',fontSize:'13px'}} id="tableHidden"  className="table table-bordered table-striped" >
                             <thead>
                                 <tr>
                                    <th style={{width:'7%'}}>N°</th>
                                    <th>Nom et prénom(s)</th>
                                    <th>Titre de l'ouvrage</th>
                                    <th style={{width:'20%'}} >Date de lecture</th>
                                    <th style={{width:'9%'}}>Heure</th>
                                  
                                 </tr>
                             </thead>
                             <tbody>
                             {this.state.lspHidden.map(lsp=>
                                <tr>
                                <td data-label="Numéro Etudiant: ">&nbsp; <span class="label label-warning">{lsp.idMembre}</span></td>
                               <td data-label="Nom et prénoms: " >&nbsp; <Link to={`/detailMembre/${lsp.idMembre}`}>{lsp.nom}&nbsp;{lsp.prenom}</Link></td>
                              <td data-label="Auteur: " >&nbsp;{lsp.titre} par {lsp.nomAuteur}</td>
                              <td  data-label="Date: "> &nbsp; {Moment(lsp.dateLecture).locale("de").format('dddd Do MMMM YYYY')}&nbsp;</td>
                              <td data-label="Heure: ">&nbsp; {Moment(lsp.dateLecture).locale("de").format('HH:mm')}&nbsp;</td>
                             
                            </tr>)}
                             </tbody>
                         </table>
                         </Typography>
                     </AccordionDetails>
                 </Accordion>
    }
    let autocomplete;
    let inputbook;
    if(this.state.bookid1!=''){
      inputbook= <></>
    }
    if(this.state.bookid1==''){
      inputbook=<input  style={{marginLeft:'10px',width:'380px'}} type="text" className="form-control" id="idOuvrage" placeholder="Numéro d'identification d'ouvrage..." value={this.state.bookid} onChange={this.onChangebookid}/>
    }
    if(this.state.bookid!=''){
      autocomplete= <></>
    }
    if(this.state.bookid==''){
      autocomplete= <><span style={{marginLeft:'4px',fontSize:'13px',fontFamily:'Arial'}}>Ou Choisir le titre</span><Autocomplete   
      id="combo-box-demo"
      options={this.state.ouvrages}
      getOptionLabel={(option) => option.titre}
      fullWidth
      renderInput={(params) =>  <TextField required fullWidth id="outlined-basic" {...params} placeholder="Choisir le titre de l'ouvrage"  
       InputProps={{  ...params.InputProps, type: 'search',style: { fontSize: 13 }}}
       defaultValue="test" 
      variant="outlined"
      value={this.state.bookid1}
      onChange={(e)=>this.setState({bookid1:e.target.value})}
      onSelect={(e)=>this.setState({bookid1:e.target.value})}
      style={{marginLeft:'10px',width:'380px'}}
      />}
    /></>
    }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
        Lecture sur place
       
      </h1>
      <ol className="breadcrumb">
      <li>

      <ReactToExcel 
         className="excel"
         sheet="Sheet"
          table="tableHidden" 
          filename="Liste_Lecture_CERCOM"
          submit="submit 1"
          buttonText="Exporter la liste vers Excel"
          
          />&nbsp;&nbsp;
      </li>
     
      <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>

   
      </ol>
    </section>

   
    <section className="content">
      <div className="row">
      <div className="col-md-4">
      <div className="box box-warning">
            <div className="box-header">
              <h3 className="box-title">Ajout d'une autre lecture  </h3>
            </div>
            {(()=>{
                                if(this.state.openCamera==true){
                                    return(
                                            <div style={{marginLeft:'50px'}}>
                                                <Tooltip title="Arrêter le scanner" aria-label="Arrêter le scanner">
                                                    <Fab style={{backgroundColor:'#ed5d4a',color:'white'}} onClick={this.stop}>
                                                        <StopIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:320,height:300}}>
                                                 <BarcodeScannerComponent
                                                    width={500}
                                                    height={500}
                                                    
                                                    onUpdate={(err, result) => {
                                                    if (result){
                                                       
                                                        var pieces = result.text.split("_");
                                                        console.log('tab0='+pieces[0]+' tab1='+pieces[1])
                                                        this.setState({data:result.text,estResult:pieces[0],message:''})
                                                        if(pieces[0]=='1OUVR000'){
                                                            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+pieces[1])
                                                            .then(response=>{
                                                                if(response.data.message!='Could not find data.'){
                                                                console.log(response.data)
                                                                
                                                                this.setState({ouvrages:response.data,
                                                                  bookid:response.data.idOuvrage
                                                                    })
                                                                }
                                                                else{
                                                                this.setState({message:'Aucun'+test,ouvrages:[]})
                                                                }
                                                            
                                                            })
                                                            .catch(error=>{
                                                                this.setState({message:'Aucun'+test,ouvrages:[]})
                                                                console.log(error)
                                                              }) 
                                                        }
                                                        else if(pieces[0]=='1mb'){
                                                            Axios.post('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+pieces[1])
                                                            .then(response=>{
                                                                if(response.data.message!='Could not find data.'){
                                                                console.log(response.data)
                                                                
                                                                this.setState({
                                                                    userid:response.data.idMembre
                                                                })
                                                                }
                                                                else{
                                                               //this.setState({message:'Aucun',ouvrages:[]})
                                                                }
                                                            })
                                                            .catch(error=>{
                                                                this.setState({message:'Aucun',ouvrages:[]})
                                                                console.log(error)
                                                              }) 
                                                        }
                                                        else{
                                                           
                                                           // this.setState({message:'Aucun',ouvrages:[]})
                                                            console.log('Message='+this.state.message)
                                                        }
                                                        
                                                        
                                                    }
                                                    else {
                                                        //this.setState({message:'Aucun',ouvrages:[]})
                                                    }
                                                    }}
                                                />
                                               </Avatar>
                                            </div>
                                        )
                                }  
                                if(this.state.openCamera==false){
                                    return(
                                            <div  style={{marginLeft:'50px'}}>
                                                <Tooltip title="Démarrer le scanner" aria-label="Démarrer le scanner">
                                                    <Fab style={{backgroundColor:'#488a3b',color:'white'}} onClick={this.demarrer}>
                                                        <CameraAltIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:320,height:300}}>
                                                    <h5 style={{color:'#343634'}}>Démarrer le scanner et approcher le code-barres</h5>
                                                </Avatar>
                                            </div>
                                        )
                                }
                            }
                        )()}
             
            <form role="form" onSubmit={this.enregistrer}>
                  <div className="box-body">
                    <p className="text-muted">Pour ajouter une, veuillez remplir les champs suivants ou scanner un code-barres en lançant le caméra:</p>
                    <div className="form-group">
                      <label>Numéro d'identification du membre</label>
                      <input required  type="text" className="form-control" id="idmembre" placeholder="Numéro d'identification..." value={this.state.userid} onChange={this.onChangeuserid}/>
                    </div>
                    <div className="form-group" style={{borderRadius:'5px',border:'1px solid #e8e3e3'}}>
                      <label style={{marginLeft:'4px'}}>Entrer le numéro de l'ouvrage ou titre</label>
                  
                      {inputbook}
                    
                     {autocomplete}
                     <br/>
                    </div>
                    <div className="form-group">
                      <label>Date et heure de lecture</label>
                      <input type="datetime-local" className="form-control" id="datelecture"  onChange={(event) => this.setState({dateentree: event.target.value})}/>
                    </div>
                    
                  </div>

                  <div class="box-footer">
                    
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
            </form>
            </div>
      </div>
        <div className="col-md-8">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Liste des lectures sur place  </h3>
            </div>
            <div class="box-body pad table-responsive">
            <p>Vous verrez ci-dessous la liste des membres qui effectuent une lecture sur place. 
            </p>
             
            </div>
            <div className="box-body">
          
                  <TextField  onChange={this.searchHandler} id="search" type="text" className="form-control" placeholder="Effectuer une recherche..." variant="outlined"  
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end" >
                          <SearchIcon fontSize="large"/>
                        </InputAdornment>
                      ),
                    }}
                    inputProps={{style:{fontSize:13}}}
                  />
                <br/> <br/>
                  <span style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
              <table class="table table-striped">
                <thead>
                  <tr>
                  <th style={{width:'7%'}}>Numéro Etudiant</th>
                    <th style={{width:'8%'}}></th>
                    <th>Nom et prénom(s) des membres</th>
                    <th>Titre de l'ouvrage</th>
                    <th style={{width:'20%'}} >Date de lecture</th>
                    <th style={{width:'9%'}}>Heure</th>
                    <th  style={{width:'8%'}}>Action</th>
                    <th  style={{width:'8%'}}></th>
                  </tr>
                </thead>
                <tbody>
                {this.state.postdata}  
                  
                </tbody>
                
              </table>
             
             
             <div style={{marginLeft:'40%'}}>
             <ReactPaginate 
                   previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                   nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                   breakLabel={"..."}
                   breakClassName={"break-me"}
                   pageCount={this.state.pageCount}
                   marginPagesDisplayed={2}
                   pageRangeDisplayed={5}
                   onPageChange={this.handlePageClick}
                   containerClassName={"pagination"}
                   subContainerClassName={"pages pagination"}
                   activeClassName={"active"}/>
            </div>
            </div>
          </div>
        </div>
     

      </div>
    </section>
    <Dialog fullWidth={true}
      maxWidth = {'xs'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"> <h4>Effectuer une modification:</h4></DialogTitle>
        <form onSubmit={this.onSubmit}>
         
          <DialogContent dividers>
            <p style={{fontSize: 14,fontFamily:"Arial"}}>{this.state.nom} a lu l'ouvrage <q><Link to="/dashboard">{this.state.titre}</Link></q>, vous pouvez modifier
             ces informations selon le numéro du membre et/ou le numéro d'identification de l'ouvrage:
            </p>
            <label style={{fontSize: 13,fontFamily:"Arial"}}>Identification de l'utilisateur:</label>
            <TextField
            variant="outlined"
            id="nomG"
            type="text"
            value={this.state.amodifierMembreid}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierMembreid}
          />
           <label style={{fontSize: 13,fontFamily:"Arial"}}>Numéro d'identification de l'ouvrage:</label>
         
           <TextField
            variant="outlined"
            id="nationaliteM"
            type="text"
            value={this.state.amodifieridouvrage}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifieridouvrage}
          />
         

            <label style={{fontSize: 13,fontFamily:"Arial"}}>Date de lecture:</label>
           <TextField
            variant="outlined"
            id="dateN"
            type="text"
            value={this.state.amodifierDateL}
            InputProps={{ style: {fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierDateL}
          />
        
      
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>
      {tablehidden}
  </div>
   
  );
}
}
