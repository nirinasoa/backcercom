import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';

export default class Header extends Component {
  constructor(props) {
    super(props);
    this.state={
      demandeAccesMembre:[],
      nombreDemande:'',
    };
    
    }

  deconnexion = () => {
    window.sessionStorage.clear()
    window.location="/"
  };
  componentDidMount(){
    Axios.get('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/nombreDemandeNV.php')
    .then(response=>{
      this.setState({nombreDemande:response.data[0].nombre})
      
    })
    .catch(error=>{
      console.log(error)
    })
    Axios.get('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/readNV.php')
    .then(response=>{
      this.setState({demandeAccesMembre:response.data})
      
    })
    .catch(error=>{
      console.log(error)
    })
  }

  render(){
  let demande;
  if(this.state.nombreDemande!=''){
    demande=<>{this.state.nombreDemande} demande en attente</>
  }
  if(this.state.nombreDemande==''){
    demande=<>{this.state.nombreDemande} Aucune demande</>
  }
  return (
    <header className="main-header">
       
        <Link to="/dashboard" className="logo"><b><img src="assets/dist/img/iserazo.png" height="45px" width="45px"/>
        <img src="assets/dist/img/logo-cercom.jpg" width="50px"/>CERCOM</b></Link>
         <nav className="navbar navbar-static-top" role="navigation">
      
          <a href="#" className="sidebar-toggle" data-toggle="offcanvas" >
            <span className="sr-only">Toggle navigation</span>
          </a>
          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              
             
              
              <li className="dropdown notifications-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                  <i className="fa fa-bell-o"></i>
                  <span className="label label-warning">{this.state.nombreDemande}</span>
                </a>
                <ul className="dropdown-menu">
                  <li className="header">Notification sur les demandes d'accès</li>
                  <li>
                    
                    <ul className="menu">
                      <li>
                        <Link to="/demandeAcces">
                          <i className="fa fa-users text-aqua"></i>{demande}
                        </Link>
                      </li>
                      
                     
                     
                    </ul>
                  </li>
                
                </ul>
              </li>
             
              
              <li className="dropdown user user-menu">
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <Avatar style={{width:30,height:30}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${sessionStorage.getItem("photo")}`} className="user-image"  />
                  <span className="hidden-xs">{sessionStorage.getItem("nomAdmin")}&nbsp;{sessionStorage.getItem("prenom")}</span>
                </a>
                <ul className="dropdown-menu">
                  
                  <li className="user-header">
                    <img src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${sessionStorage.getItem("photo")}`} className="img-circle" alt="User Image" />
                    <p>
                    {sessionStorage.getItem("nomAdmin")}&nbsp;{sessionStorage.getItem("prenom")} - ADMIN
                      <small>Membre à partir de  {sessionStorage.getItem("created")}</small>
                    </p>
                  </li>
                 
                 <li className="user-footer">
                    <div className="pull-left">
                      <Link to="/profil" className="btn btn-default btn-flat">Mon profil</Link>
                    </div>
                    <div className="pull-right">
                      <button onClick={this.deconnexion} className="btn btn-default btn-flat">Me déconnecter</button>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
  );
}
}
