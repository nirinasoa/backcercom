import React,{Component} from 'react';
import Axios from 'axios';
import EditIcon from '@material-ui/icons/Edit';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Button } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import CloseIcon from '@material-ui/icons/Close';
import MuiAlert from '@material-ui/lab/Alert';
import Moment from 'moment';
import 'moment/locale/fr';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';


export default class Auteur extends Component {
  
  constructor(props) {
    super(props)
    this.onChangenomauteur=this.onChangenomauteur.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this);
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
    this.onChangenationalite=this.onChangenationalite.bind(this)
    this.onSubmitAuteur=this.onSubmitAuteur.bind(this)
    this.delete=this.delete.bind(this); 
    this.modifier=this.modifier.bind(this);
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeamodifierNom=this.onChangeamodifierNom.bind(this)
    this.handleClose=this.handleClose.bind(this);
    this.onChangeamodifierNationalite=this.onChangeamodifierNationalite.bind(this)
    this.onChangeamodifierDateN=this.onChangeamodifierDateN.bind(this)
    this.searchHandler=this.searchHandler.bind(this); 
		this.state={
      auteurs:[],
      nomauteur:'',
      datenaissance:'',
      nationalite:'',
      open:false,
      open1:false,
      amodifier:[],
      amodifierNom:'',
      amodifierNationalite:'',
      amodifierDateN:'',
      getidAuteur:'',
      offset: 0,
		  perPage: 10,
      currentPage: 0,
      message:'',
   
		 
    };
 
    }
    searchHandler(e){
      console.log("search="+e.target.value)
   
        var test=' résultat(s) trouvé(s)'
        if(e.target.value==''){
           this.setState({message:'Aucun'+test})
          this.componentDidMount();
        }
        else{
      Axios.post('https://phpapiserver.herokuapp.com/api/auteur/search.php',{wordToSearch:e.target.value})
      .then(response=>{
        if(response.data.message!='Could not find data.'){
          const data = response.data;
      const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
      const postdata = slice.map(auteur=>
        <tr key={auteur.idAuteur}>
          <td><span class="label label-warning">{auteur.idAuteur}</span></td>
          <td><Link to={`/detailAuteur/${auteur.idAuteur}`}> {auteur.nomAuteur}</Link> </td>
          <td>{auteur.nationalite}</td>
          <td> {Moment(auteur.dateNaissance).locale("de").format('DD/MM/yyyy')}</td>
          <td> 
            <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                <EditIcon fontSize="large"  style={{"color":"#4a4d49"}} onClick={() => this.modifier(auteur.idAuteur)} /> 
            </Fab>
          </td>
          <td>
            <Fab size="small" style={{"backgroundColor":"#e02f2f"}}aria-label="add" title="Suppimer cette ligne">
              <DeleteIcon fontSize="large"  style={{"color":"#ffffff"}} onClick={() => this.delete(auteur.idAuteur)} /> 
            </Fab>
          </td>
        </tr>)
      
          this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
        }
        else{
          this.setState({message:'Aucun'+test,ouvrages:[]})
        }
       
        
      
        //this.setState({posts:response.data})
      })
    }
    
    }
    handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState({
          currentPage: selectedPage,
          offset: offset
      }, () => {
          this.componentDidMount()
      });

  };
    handleClose1 = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      this.setState({open1:false})
    };
    onChangenomauteur(e){
      this.setState({
        nomauteur:e.target.value
      });
    }
    onSubmitAuteur(e){
      e.preventDefault();
      const auteur={
        nomAuteur:this.state.nomauteur,
        dateNaissance:this.state.datenaissance,
        nationalite:this.state.nationalite

       }
      console.log(auteur)
      Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
      .then(res=>console.log(res.data));
      alert('Le auteur a été crée avec succès!')
      window.location='/auteur';
    }
    onChangedatenaissance(e){
      this.setState({
        datenaissance:Date.parse(e.target.value)
      });
    }
    onChangenationalite(e){
      this.setState({
        nationalite:e.target.value
      });
    }
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/auteur/deleteOne.php',{idAuteur:id})
      .then(res => {
         this.componentDidMount()
      });
     
       
     } 
     else{}
    }
   handleClose(){
    this.setState({open:false})
    };
    
   modifier(idAuteur){
      this.setState({open:true})
      console.log(idAuteur)
      Axios.get('https://phpapiserver.herokuapp.com/api/auteur/single_read.php/?idAuteur='+idAuteur)
      .then(response=>{
      
        this.setState({
          getidAuteur:response.data.idAuteur,
          amodifierNom:response.data.nomAuteur,
          amodifierNationalite:response.data.nationalite,
          amodifierDateN:response.data.dateNaissance
        })
      })
      .catch(error=>{
        console.log(error)
      }) 
      console.log(this.state.amodifierDateN)
    };
    componentDidMount(){
		Axios.get('https://phpapiserver.herokuapp.com/api/auteur/read.php')
    .then(response=>{
      
      const data = response.data;
      const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
      const postdata = slice.map(auteur=>
        <tr key={auteur.idAuteur}>
          <td><span class="label label-warning">{auteur.idAuteur}</span></td>
          <td><Link to={`/detailAuteur/${auteur.idAuteur}`}> {auteur.nomAuteur}</Link> </td>
          <td>{auteur.nationalite}</td>
          <td> {Moment(auteur.dateNaissance).locale("de").format('DD/MM/yyyy')}</td>
          <td> 
            <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                <EditIcon fontSize="large"  style={{"color":"#4a4d49"}} onClick={() => this.modifier(auteur.idAuteur)} /> 
            </Fab>
          </td>
          <td>
            <Fab size="small" style={{"backgroundColor":"#e02f2f"}}aria-label="add" title="Suppimer cette ligne">
              <DeleteIcon fontSize="large"  style={{"color":"#ffffff"}} onClick={() => this.delete(auteur.idAuteur)} /> 
            </Fab>
          </td>
        </tr>)
           this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),message:''})
      
		})
		.catch(error=>{
		  console.log(error)
		})
   }
   onChangeamodifierNom(e){
    this.setState({
      amodifierNom:e.target.value
    });
  }
  onChangeamodifierNationalite(e){
    this.setState({
      amodifierNationalite:e.target.value
    });
  }
  onChangeamodifierDateN(e){
    this.setState({
      amodifierDateN:e.target.value
    });
  }
    onSubmit(e){
    e.preventDefault();
    console.log("id="+this.state.getidAuteur+"nom"+this.state.amodifierNom+" desc="+this.state.amodifierNationalite+"date="+this.state.amodifierDateN)
    const amodifierAuteur={
      idAuteur:this.state.getidAuteur,
      nomAuteur:this.state.amodifierNom,
      dateNaissance:this.state.amodifierDateN,
      nationalite:this.state.amodifierNationalite
      
    }
   console.log(amodifierAuteur)
    Axios.post('https://phpapiserver.herokuapp.com/api/auteur/update.php',amodifierAuteur)
    .then(response=>{
       if(response){
        this.setState({open:false,open1:true})
       
        }
        else{
            this.setState({open:true}) 
        }
       })
      Axios.get('https://phpapiserver.herokuapp.com/api/auteur/read.php')
        .then(response=>{
          this.setState({auteurs:response.data})
          
        })
        .catch(error=>{
          console.log(error)
        })

  }
    render(){
  
  return (
    <div className="content-wrapper">
        
        <section className="content-header">
          <h1>Auteur</h1>
          <h4>Consultez les auteurs des ouvrages existants ou ajoutez un nouveau</h4>
          <ol className="breadcrumb">
            <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
          </ol>
        </section> 
        <section className="content">
           <div className="row">
            
            <div className="col-md-4">
            
            <div className="box box-success">
          
                      <div className="box-header">
                  <h3 className="box-title">Ajout d'un auteur</h3>
                </div>
                <form role="form" onSubmit={this.onSubmitAuteur}>
                      <div className="box-body">
                        <p className="text-muted">Pour ajouter un auteur, veuillez remplir les champs suivants:</p>
                        <div className="form-group">
                          <label>Nom et prénom(s) de l'auteur</label>
                          <input type="text" className="form-control" id="nomprenomsauteur" placeholder="Nom et prénom(s)..." value={this.state.nomauteur} onChange={this.onChangenomauteur} required/>
                        </div>
                        <div className="form-group">
                          <label>Date de naissance</label>
                          <input type="date" className="form-control" id="datenaissance" onChange={(event) => this.setState({datenaissance: event.target.value})}/>
                        </div>
                        <div className="form-group">
                          <label>Nationalité</label>
                          <input type="text" className="form-control" id="nationalite" placeholder="Nationalité..." value={this.state.nationalite} onChange={this.onChangenationalite}/>
                        </div>
                       
                      </div>
    
                      <div class="box-footer">
                        
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                      </div>
                </form>
                </div>
                </div>
                <div className="col-md-8">
                <div className="box box-primary">
                <div className="box-header">
                  <h3 className="box-title">Liste des auteurs enregistrés</h3>
                </div>
              <div className="box-body">
              <div className="input-group">
                    <input onChange={this.searchHandler} id="search" type="text" className="form-control" placeholder="Effectuer une recherche..." style={{"borderRadius":"10%","backgroundColor":"#ffffff","border":"1px solid #827f7f"}} />
                    <span className="input-group-btn">
                      <button id='search-btn' className="btn btn-flat" style={{"border":"1px solid #827f7f"}}><i className="fa fa-search"></i></button>
                    </span>
                  </div>
                  <span style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
            
              <table style={{backgroundColor:'white'}} className="table table-bordered table-striped">
                   <thead>
                      <tr>
                      <th  style={{width:'8%'}}>Id Auteur</th>
                        <th>Nom de l'auteur</th>
                        <th>Nationalité</th>
                        <th>Date de naissance</th>
                        <th style={{width:'8%'}}> Actions</th>
                        <th style={{width:'8%'}}></th>
                      </tr>
                    </thead>
                    <tbody>
                 
                    {this.state.postdata}  
                    <div style={{marginLeft:'40%'}}>
              <ReactPaginate 
                    previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                    nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
             </div>
                    </tbody>
                  </table>
              


            </div>
            </div>
            </div>
            </div>
    
        </section>
        <Dialog fullWidth={true}
          maxWidth = {'sm'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"> <h4>Effectuer une modification:</h4></DialogTitle>
        <form onSubmit={this.onSubmit}>
       
          <DialogContent dividers>
            <label>Nom de l'auteur:</label>
            <TextField
            variant="outlined"
            id="nomG"
            type="text"
            value={this.state.amodifierNom}
            InputProps={{ style: { border: "1px solid #838786",fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierNom}
          />
           <label>Nationalité:</label>
           <TextField
            variant="outlined"
            id="nationaliteM"
            type="text"
            value={this.state.amodifierNationalite}
            InputProps={{ style: { border: "1px solid #838786",fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierNationalite}
          />
            <label>Date de naissance:</label>
           <TextField
            variant="outlined"
            id="dateN"
            type="date"
            value={this.state.amodifierDateN}
            InputProps={{ style: { border: "1px solid #838786",fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierDateN}
          />
        
      
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>
      <Snackbar
        
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        open={this.state.open1}
        autoHideDuration={3000}
        onClose={this.handleClose1}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<h5>Modification effectuée</h5>}
        action={[
          <IconButton key="close" aria-label="close" color="inherit"  onClick={this.handleClose1}>
            <CloseIcon />
          </IconButton>,
        ]}
      />
</div>
  );
}
}

