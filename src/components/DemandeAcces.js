import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Button } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Barcode from 'react-barcode';
import './style.css';

export default class DemandeAcces extends Component {
  constructor(props) {
    super(props)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangeadresse=this.onChangeadresse.bind(this)
    this.onChangefiliere=this.onChangefiliere.bind(this)
    this.onChangecin=this.onChangecin.bind(this)
    this.onChangedelivreA=this.onChangedelivreA.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangedomaine=this.onChangedomaine.bind(this)
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.onChangeemail = this.onChangeemail.bind(this)
    this.delete=this.delete.bind(this); 
    this.enregistrer=this.enregistrer.bind(this)


    this.modifier=this.modifier.bind(this);
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeamodifierNom=this.onChangeamodifierNom.bind(this)
    this.handleClose=this.handleClose.bind(this);
    this.onChangeamodifierPrenom=this.onChangeamodifierPrenom.bind(this)
    this.onChangeamodifierAdresse=this.onChangeamodifierAdresse.bind(this)
    this.onChangeamodifierfiliere=this.onChangeamodifierfiliere.bind(this)
    this.onChangeamodifiercin=this.onChangeamodifiercin.bind(this)
    this.onChangeamodifierdelivreA=this.onChangeamodifierdelivreA.bind(this)
    this.onChangeamodifiertelephone=this.onChangeamodifiertelephone.bind(this)
    this.onChangeamodifierdomaine=this.onChangeamodifierdomaine.bind(this)
    this.onChangeamodifieremail=this.onChangeamodifieremail.bind(this)
    this.onChangeamodifierdateNaissance=this.onChangeamodifierdateNaissance.bind(this)
    this.onChangeFile1 = this.onChangeFile1.bind(this)
		this.state={
       nom:'',
       prenom:'',
       adresse:'',
       filiere:'',
       cin:'',
       delivreA:'',
       telephone:'',
       domaine:'',
       email:'',
       datenaissance:'',
       file1:null,
       filename1:null,
       membres:[],
       open:false,
       amodifier:[],
       amodifiernom:'',
       amodifierprenom:'',
       amodifieradresse:'',
       amodifierfiliere:'',
       amodifiercin:'',
       amodifierdelivreA:'',
       amodifiertelephone:'',
       amodifierdomaine:'',
       amodifieremail:'',
       amodifierdateNaissance:'',
       amodifierphoto:'',
       getidMembre:'',
       file:null,
      filename:null,
      demandes:[],
      getid:'',
      motif:'',
      ancienmotif:'',
      photo:'',
      erreur:'',
      };
    
    }
    onChangeamodifierNom(e){
      this.setState({
        amodifiernom:e.target.value
      });
    }
    onChangeamodifierPrenom(e){
      this.setState({
        amodifierprenom:e.target.value
      });
    }
    onChangeamodifierAdresse(e){
      this.setState({
        amodifieradresse:e.target.value
      });
    }
    onChangeamodifierfiliere(e){
      this.setState({
        amodifierfiliere:e.target.value
      });
    }
    onChangeamodifiercin(e){
      this.setState({
        amodifiercin:e.target.value
      });
    }
    onChangeamodifierdelivreA(e){
      this.setState({
        amodifierdelivreA:e.target.value
      });
    }
    onChangeamodifiertelephone(e){
      this.setState({
        amodifiertelephone:e.target.value
      });
    }
    onChangeamodifierdomaine(e){
      this.setState({
        amodifierdomaine:e.target.value
      });
    }
    onChangeamodifieremail(e){
      this.setState({
        amodifieremail:e.target.value
      });
    }
    onChangeamodifierdateNaissance(e){
      this.setState({
        amodifierdateNaissance:e.target.value
      });
    }
    onChangeFile1(e) {
      this.setState({file1:e.target.files[0]})
      this.setState({filename1:e.target.files[0].name})
    }
    modifier(id,mtf,photo){
      this.setState({open:true})
      console.log(id)
      if(mtf=='ko'){
        this.setState({
         ancienmotif:'En attente de validation'
        })   
      }
      if(mtf=='ok'){
        this.setState({
         ancienmotif:'Demande validée'
        })   
      }
      if(mtf=='koko'){
        this.setState({
         ancienmotif:'Demande refusée'
        })   
      }
      this.setState({
        getid:id,
        photo:photo,
        
      })
     
    };
    handleClose(){
      this.setState({open:false})
      };
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/deleteOne.php',{idDemandeAccesMembre:id})
      .then(res => {console.log(res.data)
       this.componentDidMount()
      });
    
       
     } 
     else{}
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    onChangeprenom(e){
      this.setState({
        prenom:e.target.value
      });
    }
    onChangeadresse(e){
      this.setState({
        adresse:e.target.value
      });
    }
    onChangefiliere(e){
      this.setState({
        filiere:e.target.value
      });
    }
    onChangecin(e){
      this.setState({
        cin:e.target.value
      });
    }
    onChangedelivreA(e){
      this.setState({
        delivreA:e.target.value
      });
    }
    onChangetelephone(e){
      this.setState({
        telephone:e.target.value
      });
    }
    onChangedomaine(e){
      this.setState({
        domaine:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangedatenaissance(e){
      this.setState({
        datenaissance:e.target.value
      });
    }
    onChangeFile(e) {
      this.setState({file:e.target.files[0]})
      this.setState({filename:e.target.files[0].name})
  }
  async uploadFile(file){
     

    const formData = new FormData();
    
    formData.append('avatar',file)
    
    return await Axios.post(this.UPLOAD_ENDPOINT, formData,{
        headers: {
            'content-type': 'multipart/form-data'
        }
    });
  }
    enregistrer(e){
      e.preventDefault();
     
    };
    componentDidMount(){
      Axios.get('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/ficheDemandeAcces.php')
      .then(response=>{
        this.setState({demandes:response.data})
        
      })
      .catch(error=>{
        console.log(error)
      })
     }
     onSubmit(e){
      e.preventDefault();
      //alert(this.state.motif)
      if(this.state.motif!=''){
        const amodif={
          idDemandeAccesMembre:this.state.getid,
          estValide:this.state.motif
        }
        console.log(amodif)
        Axios.post('https://phpapiserver.herokuapp.com/api/demandeAccesMembre/update.php',amodif)
        .then(res=>{
          console.log(res.data)
          this.setState({open:false})
          this.componentDidMount()
  
        });
      }
      else{
        this.setState({open:true,erreur:'Veuillez choisir une action'}) 
      }
    
     
    }
  render(){
 
  
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
          <h1>Liste des demandes d'accès à la modification par les membres</h1>
          <ol className="breadcrumb">
              <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
          </ol>
        </section> 

   
    <section className="content">
        <div className="row">
         
    
        <div className="col-md-12">
          <div className="box box-warning">
            <h4 className="box-title">&nbsp;Avec état de validation </h4>
            <div className="box-body">
             <table  className="table table-bordered table-striped">
                <thead>
                  <tr>
                  <th style={{width:'8%'}} scope="col"></th>
                    <th style={{width:'20%'}} scope="col">Noms des membres</th>
                    <th scope="col">Motif de la demande</th>
                    <th scope="col">Validation</th>
                    <th scope="col">Date de demande</th>
                    <th style={{width:'8%'}} scope="col">Actions</th>
                    <th style={{width:'8%'}} scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                { this.state.demandes.map(mbr=>
                  <tr>
                     <td ><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${mbr.photo}`} /> </td>
                    <td data-label="Nom et prénom(s):"> <Link style={{"color":"#383737"}} to={`/detailMembre/${mbr.idMembre}`}>{mbr.nom} &nbsp;{mbr.prenom}</Link></td>
                    <td data-label="Motif:"> <i className="fa fa-comments"></i>&nbsp;{mbr.motif}</td>
                    <td data-label="Validation:">
                    {(()=>{
                       if(mbr.estValide=='ko'){
                          return(<span class="label label-warning">En attente de validation</span>)
                       }
                       if(mbr.estValide=='ok'){
                        return(<span class="label label-success">Demande validée</span>)
                      }
                      if(mbr.estValide=='koko'){
                        return(<span class="label label-danger">Demande réfusée</span>)
                      }
                       
                    })()}
                    
                    </td>
                    <td data-label="Date:">{mbr.created}</td>
                    <td>
                        
                    <IconButton aria-label="delete">
                            <EditIcon fontSize="large" onClick={() => this.modifier(mbr.idDemandeAccesMembre,mbr.estValide,mbr.photo)} />
                        </IconButton>
                        </td> 
                        <td>       
                       
                        <IconButton aria-label="delete">
                            <DeleteIcon fontSize="large" onClick={() => this.delete(mbr.idDemandeAccesMembre)} />
                        </IconButton>
                        
                    </td>
                  </tr>
                )}
                
                  
                </tbody>
                
              </table>
            </div>
          </div>
        </div>


      </div>
    
    </section>
    <Dialog fullWidth={true}
          maxWidth = {'xs'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
           <table>
              <tr>
                <td><Avatar   alt="Remy Sharp" src={`https://phpapiserver.herokuapp.com/uploads/${this.state.photo}`} /></td>
                <td><h4>&nbsp;{this.state.amodifiernom} &nbsp; {this.state.amodifierprenom}</h4></td>  
              </tr>
           </table>
        </DialogTitle>
        <form onSubmit={this.onSubmit}>
           
          <DialogContent dividers>
            <p>Validation de cette demande:</p>
            Etat actuel:&nbsp;<label>{this.state.ancienmotif}</label><br/>
            <label style={{color:'red'}}>{this.state.erreur}</label>
              <select className="form-control" value={this.state.motif}   onChange={(e)=>this.setState({motif:e.target.value})}>
                        <option  value="">Veuillez choisir l'action</option>
                         <option  value="ko">Mise en attente</option>
                         <option  value="ok">Validé la demande</option>
                         <option  value="koko">Réfusé la demande</option>


                      </select>
          
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>
  </div>
   
  );
}
}
