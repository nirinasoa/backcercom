import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import {Link} from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import { PieChart } from 'react-minimal-pie-chart';
import {Pie} from 'react-chartjs-2';
import Moment from 'moment';
import 'moment/locale/fr';
export default class Statistique extends Component {
	constructor(props) {
		super(props)
		this.handleClickOpen=this.handleClickOpen.bind(this);
		this.handleClose=this.handleClose.bind(this);
	
		this.state={
				   pglecture:[],
				   topfavori:[],
				   nombreAdmin:[],
				   nombreOuvrage:[],
				   nombreMembre:[],
				   nombreUM:[],
				   lecteurs:[],
				   open:false,
				   pluslu:[],
				   ouvrageParLangue:[],
					labelsPie:[],
					dataPie:[],
					pieColorArray:[],
					labelsPie1:[],
					dataPie1:[],
					pieColorArray1:[],

				   
		  };
		
		}
		 handleClickOpen(idOuvrage){
			this.setState({open:true})
			Axios.get('https://phpapiserver.herokuapp.com/api/lectureSurPlace/detailLecture.php/?idOuvrage='+idOuvrage)
			.then(response=>{
			this.setState({lecteurs:response.data})
			
			})
			.catch(error=>{
			console.log(error)
			})
			
		  };
		
		   handleClose(){
			this.setState({open:false})
		  };

	componentDidMount(){
	
		Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ouvrageParLangue.php')
		.then(response=>{
		  this.setState({ouvrageParLangue:response.data})
		  let allColors= [];
        let allLabels=[];
        let arrayData=[];
        let colorIsPresent=true;
        let randomColor="";
        
		let i=0;
		
        for (i = 0; i <response.data.length; i++) {
		
           //take a random color
           //make sure all color inside the array are not the same 
            colorIsPresent=true;
            while (colorIsPresent) {
                randomColor="#" +Math.floor(Math.random()*16777215).toString(16);
                colorIsPresent=allColors.some(el => el === randomColor);               
			}
			
            allColors.push(randomColor);
            //insert labels
            allLabels.push(response.data[i].langue);
            //insert data
			arrayData.push(response.data[i].nombre);
		}
		this.setState({
            labelsPie:allLabels,
            dataPie:arrayData,
            pieColorArray:allColors
        });
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/progressionlecture.php')
		.then(response=>{
		  this.setState({pglecture:response.data})
		  let allColors= [];
        let allLabels=[];
        let arrayData=[];
        let colorIsPresent=true;
        let randomColor="";
        
		let i=0;
		
        for (i = 0; i <response.data.length; i++) {
		
           //take a random color
           //make sure all color inside the array are not the same 
            colorIsPresent=true;
            while (colorIsPresent) {
                randomColor="#" +Math.floor(Math.random()*16777215).toString(16);
                colorIsPresent=allColors.some(el => el === randomColor);               
			}
			
            allColors.push(randomColor);
            //insert labels
            allLabels.push(response.data[i].titre);
            //insert data
			arrayData.push(response.data[i].nombreVu);
		}
		this.setState({
            labelsPie1:allLabels,
            dataPie1:arrayData,
            pieColorArray1:allColors
        });
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/favori/topfavori.php')
		.then(response=>{
		  this.setState({topfavori:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/pluslu.php')
		.then(response=>{
		  this.setState({pluslu:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/nombreAdmin.php')
		.then(response=>{
		  this.setState({nombreAdmin:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/membre/nombreMembre.php')
		.then(response=>{
		  this.setState({nombreMembre:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurMembre/nombreUM.php')
		.then(response=>{
		  this.setState({nombreUM:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
		Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/nombreOuvrage.php')
		.then(response=>{
		  this.setState({nombreOuvrage:response.data})
		  
		})
		.catch(error=>{
		  console.log(error)
		})
	   }
	   

	render(){
		const alldataPiePL = {
		
            labels:this.state.labelsPie1,
            datasets: [{
                data: this.state.dataPie1,
                backgroundColor: this.state.pieColorArray1,
                hoverBackgroundColor: this.state.pieColorArray1
            }]
		};
	
		const alldataPie3 = {
            labels:this.state.labelsPie,
            datasets: [{
                data: this.state.dataPie,
                backgroundColor: this.state.pieColorArray,
                hoverBackgroundColor: this.state.pieColorArray
            }]
		};
		/*const data=[
			{ title: 'One', value: 10, color: '#E38627' },
			{ title: 'Two', value: 15, color: '#C13C37' },
			{ title: 'Three', value: 20, color: '#6A2135' }
		]
*/
	
  return (
    <div className="content-wrapper">
     <section className="content-header">
		<h1>Tableau de bord</h1>
		<h4>Consultez les statistiques sur les ouvrages et les membres</h4>
		<ol className="breadcrumb">
			<li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
		</ol>
    </section>    
    <section className="content">
   
                <div className="row">
                    <div className="col-lg-3 col-xs-6">
                    
						<div className="small-box bg-aqua">
							<div className="inner">
							{ this.state.nombreMembre.map(n=>
							<h3>
								{n.nombre}
							</h3>
							)}
							<p>
								Membres
							</p>
							</div>
							<div className="icon">
								<i className="ion ion-person-add"></i>
							</div>
							<Link to="/membre" className="small-box-footer">
							Plus d'informations <i className="fa fa-arrow-circle-right"></i>
							</Link>
						</div>
                    </div>
                    <div className="col-lg-3 col-xs-6">
                    
						<div className="small-box bg-green">
							<div className="inner">
							{ this.state.nombreOuvrage.map(n=>
							<h3>
								{n.nombre}
							</h3>
							)}
								<p>
									Ouvrage
								</p>
							</div>
							<div className="icon">
								<i className="ion ion-ios-book"></i>
							</div>
							<Link to="/dashboard" className="small-box-footer">
							Voir<i className="fa fa-arrow-circle-right"></i>
							</Link>
						</div>
                    </div>
                    <div className="col-lg-3 col-xs-6">
                    
						<div className="small-box bg-yellow">
							<div className="inner">
							
								{ this.state.nombreAdmin.map(n=>
								 		<h3>{n.nombre}</h3>
								)}
							
								<p>
									
									Utilisateur Admin
								</p>
							</div>
							<div className="icon">
								<i className="ion ion-person-add"></i>
							</div>
							<Link to="/admin" className="small-box-footer">
							Plus d'informations <i className="fa fa-arrow-circle-right"></i>
							</Link>
						</div>
                    </div>
                    <div className="col-lg-3 col-xs-6">
                    
						<div className="small-box bg-red">
							<div className="inner">
							{ this.state.nombreUM.map(n=>
							<h3>
								{n.nombre}
							</h3>
							)}
								<p>
									Utilisateur membre
								</p>
							</div>
							<div className="icon">
								<i className="ion ion-person-add"></i>
							</div>
							<Link to="/UM" className="small-box-footer">
							Plus d'informations <i className="fa fa-arrow-circle-right"></i>
							</Link>
						</div>
                    </div>
				 </div>	
               
        <div className="row">
            <div className="col-md-6">
              <div className="box box-danger">
			  <div className="box-header">
                  <h3 className="box-title">  <i className="fa fa-heart" style={{color:'#de3769'}}></i> TOP 3 Ouvrage préférés par les membres</h3>
                </div>
				<div className="box-body">
				{ this.state.topfavori.map(pl=>
                  <div>
                    <span style={{fontSize:13,fontFamily:'Arial'}}><img width="28px" height="35px" src={decodeURIComponent( decodeURIComponent(pl.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}/> 
					<Link style={{"color":"#383737"}} to={`/detailOuvrage/${pl.idOuvrage}`}>&nbsp;{pl.titre}&nbsp;</Link> 
					par <span style={{color:'#9c9899'}}>{pl.nomAuteur}</span>&nbsp;<i className="fa fa-heart" style={{color:'#de3769'}}></i>({pl.nombre})
					</span>
                    <br/>
					</div>
				)}
				</div>
				<hr/>
                <div className="box-header">
				 <h3 className="box-title">Statistique du livre par langue</h3>
                </div>
                <div className="box-body">
				<Pie data={alldataPie3} />
				<span className="text-muted">
					A noter que: <br/>fr: Français&nbsp;
					en: Anglais
				</span>
                <table id="example1" className="table table-bordered table-striped">
					<caption style={{fontSize:'14px'}}>Affichage sous forme de tableau</caption>
                <thead>

                  <tr >
                    <th>Langue</th>
                    <th>Nombre</th>
                  
                  </tr>
                </thead>
                <tbody>
				{ this.state.ouvrageParLangue.map(pl=>
                  <tr>
                    <td>{pl.langue}</td>
                    <td><b>{pl.nombre}</b></td>
                   
                  </tr>
				)}
                 
                  
                </tbody>
                
              </table>
			  <br/>
			
                 
           
                </div>
              </div>
               

            </div>
            <div className="col-md-6">
             
            
                <div className="box">

              
				  { this.state.pluslu.map(pl=>
				  <table>
                   <tr>
					 <td style={{width:'8%'}}> <img width="50px" src={decodeURIComponent( decodeURIComponent(pl.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}/></td>
					 <td>&nbsp;&nbsp;L'ouvrage le plus lu dans la bibliothèque est :<br/><label className="text-danger">&nbsp;<q>{pl.titre}</q>  </label>par {pl.nomAuteur}: <span className="text-danger">{pl.max} membres</span></td>   
					</tr>
				  </table>
				  )}
				  <hr/>
				  <div className="box-header">
				  <h3 className="box-title">Tableau de statistique de lecture</h3>
                </div>
				
                  <span className="text-muted">&nbsp;&nbsp;Voici le tableau résumant la progression de lecture des ouvrages par les membres</span>
                  <table   style={{backgroundColor:'white'}} className="table table-bordered table-striped">
                <thead>
                  <tr>
					  <th style={{width:'8%'}}></th>
                    <th>Titre de l'ouvrage</th>
                    <th>Nbre lecture</th>
                  
                  </tr>
                </thead>
                <tbody>
				{ this.state.pglecture.map(pgl=>
                  <tr>
                    <td>
						<Avatar variant="rounded"  src={decodeURIComponent( decodeURIComponent(pgl.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}/>
     		
					</td>
                    <td>{pgl.titre} par {pgl.nomAuteur}</td>
                    <td >
						<Avatar variant="contained" style={{"backgroundColor":"#f5574c","cursor":"pointer","width":"25px","height":"25px"}} onClick={() => this.handleClickOpen(pgl.idOuvrage)}>
									<span ><b>{pgl.nombreVu}</b></span>
						</Avatar>
							
						
					</td>
                  </tr>
				)}
                  
                </tbody>
                
              </table>
			
            
				</div>
                <div className="box">
				<div className="box-header">
                  <h3 className="box-title">Statistique de progression de lecture</h3>
                </div>
				<Pie data={alldataPiePL} />
                </div>
              
            

            </div>
          </div>
		 
     </section>

	 <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"> <h4>Les membres qui ont lu:</h4></DialogTitle>
      
        <DialogContent>
		{ this.state.lecteurs.map(l=>
          <DialogContentText>
		
            
		
				<ListItem>
					<ListItemAvatar>
						<Avatar alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${l.photo}`} />
					</ListItemAvatar>
					<ListItemText><span style={{fontSize:'14px',fontFamily:'Arial',color:'#5e5c5c'}}>{l.nom} {l.prenom}</span><br/>
					<span style={{fontSize:'13px',fontFamily:'Arial'}}>{Moment(l.dateLecture).locale("de").format('dddd Do MMMM YYYY')}&nbsp;à&nbsp;
					{Moment(l.dateLecture).locale("de").format('HH:mm')}</span></ListItemText>
				</ListItem>
				
			
          </DialogContentText>
		)}
        </DialogContent>
        <DialogActions>
			<Link style={{"color":"#112b52"}} to="lectureSP">Voir la liste complète</Link>
          <Button style={{"backgroundColor":"#333232"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Fermer</h6>
          </Button>
          
        
        </DialogActions>

       
          <br/>
      </Dialog>
</div>
    
   
   
  );
}

}
