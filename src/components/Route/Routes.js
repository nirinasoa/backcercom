
import React, { Component } from 'react'
import {  Switch, Route } from 'react-router-dom'
import Login from '../Login'
import Content from '../Content'
import Carte from '../Carte'
import CartePDF from '../CartePDF'
import Speech from '../Speech'
import BarcodeOuvrage from '../BarcodeOuvrage'
import BarcodeMembre from '../BarcodeMembre'


export default class Routes extends Component {
    render() {
   
        return (
           
            <Switch>
                    <Route path='/' component={Login} exact/>
                    <Route path='/dashboard' component={Content} exact/>
                    <Route path='/UM' component={Content} exact/>
                    <Route path='/widgets' component={Content} exact/>
                    <Route path='/lectureSP' component={Content} exact/>
                    <Route path='/membre' component={Content} exact/>
                    <Route path='/admin' component={Content} exact/>
                    <Route path='/ouvrage' component={Content} exact/>
                    <Route path='/ouvrageauto' component={Content} exact/>
                    <Route path='/profil' component={Content} exact/>
                    <Route path='/detailOuvrage/:id' component={Content} exact/>
                    <Route path='/statistique' component={Content} exact/>
                    <Route path='/carte/:id' component={Carte} exact/>
                    <Route path='/cartePDF' component={CartePDF} exact/>
                    <Route path='/genre' component={Content} exact/>
                    <Route path='/auteur' component={Content} exact/>
                    <Route path='/barecodereader' component={Content} exact/>
                    <Route path='/detailMembre/:id' component={Content} exact/>
                    <Route path='/demandeAcces' component={Content} exact/>
                    <Route path='/apa' component={Content} exact/>
                    <Route path='/detailAuteur/:id' component={Content} exact/>
                    <Route path='/speech/' component={Speech} exact/>
                    <Route path='/barcodeOuvrage/:id' component={BarcodeOuvrage} exact/>
                    <Route path='/barcodeMembre/:id' component={BarcodeMembre} exact/>
                    <Route path='/booksearch' component={Content} exact/>
                    
            </Switch>
            
           
        
        )
    }
}
