import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import jsPDF from 'jspdf';  
import html2canvas from 'html2canvas';  
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {Link} from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';
import Barcode from 'react-barcode';

export default class Carte extends Component{
  constructor(props) {
    super(props)
    this.state={
       idMembre:this.props.match.params.id,
       nom:'',
       prenom:'',
       adresse:'',
       filiere:'',
       cin:'',
       delivreA:'',
       telephone:'',
       domaine:'',
       email:'',
       datenaissance:'',
       photoM:'',
       message:'',
       estResult:''
      
    };
  }
  componentDidMount(){
    Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+this.props.match.params.id)
     .then(response=>{
     this.setState({
         nom:response.data.nom,
         prenom:response.data.prenom,
         adresse:response.data.adresse,
         filiere:response.data.filiere,
         cin:response.data.cin,
         delivreA:response.data.delivreA,
         telephone:response.data.telephone,
         domaine:response.data.domaine,
         email:response.data.email,
         datenaissance:response.data.dateNaissance,
         photoM:response.data.photo
 
     })})
     .catch(error=>{
       console.log(error)
     })
    };
    handlePdf = () => {
        const input = document.getElementById('pdfdiv');  
        html2canvas(input,{ logging: true, allowTaint: false,letterRendering: 1, useCORS: true })  
          .then((canvas) => {  
            var imgWidth = 120;  
            var pageHeight = 210;  
            var imgHeight = canvas.height * imgWidth / canvas.width;  
            var heightLeft = imgHeight;  
            const imgData = canvas.toDataURL('image/jpeg');  
            const pdf = new jsPDF('p', 'mm', 'a4')  
            var position = 10;  
            var heightLeft = imgHeight; 
            pdf.setProperties({
              DisplayDocTitle:true,
                title: 'Carte de lecteur bibliothèque',
                author: 'RYN',
                creator: 'RYN'
            });
            pdf.setFontSize(13);
            pdf.text(5, 5, 'DocFinder_Carte du membre'); 
            pdf.setLineWidth(1)
            pdf.addImage(imgData, 'JPEG', 10, position, imgWidth, imgHeight);  
            var string = pdf.output('datauristring');
                var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
                var x = window.open();
                x.document.open();
                x.document.write(embed);
                x.document.close();
                pdf.save('carte_Bibliotheque.pdf')
          });  
    };
  render(){
    const useStyles = makeStyles((theme) => ({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
      },
    }));
  return (
    <div className="App">  
                        <h2>Affichage simple de la carte de lecteur bibliothèque</h2>
                        <hr/>
                    <div className="row">
                      <div className="col-md-4">

                      </div>
                      <div className="col-md-4"  id="pdfdiv">
                        <table style={{"border":"1px solid #aba9a9","justifyContent":"center", "alignItems":"center",width:'100%'}}>
                          <tr>
                          <td style={{width:'80%',color:'black'}}>
                                <img src="assets/dist/img/logoAnkatso.png" width="70"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="assets/dist/img/logocarte.png" width="270"/><br/>
                                 <span style={{"textAlign":"center","fontWeight":"bolder","fontSize":"13px","fontFamily":"Arial"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 CARTE D'ADHERENT</span><br/><br/>
                                <p style={{"textAlign":"justify","fontFamily":"Arial"}}>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Nom:</label> {this.state.nom}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Prénoms:</label>  {this.state.prenom}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Adresse:</label>  {this.state.adresse} <br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Filière:</label> {this.state.filiere}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>CIN:</label>  {this.state.cin}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Délivré à:</label> {this.state.delivreA}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Télephone: </label> {this.state.telephone}<br/>
                                &nbsp;&nbsp;Signature de l'étudiant &nbsp; &nbsp; &nbsp; &nbsp;Le directeur de la Bibliothèque  <br/><span style={{"marginLeft":"177px"}}>et Archives Universitaire</span><br/><br/>
                                </p>
                             </td>
                             <td style={{width:'25%'}}>
                                   &nbsp;&nbsp;  &nbsp;&nbsp;   <img  src="assets/dist/img/logoBook.PNG" width="60" /><br/><br/>
                                 <Avatar variant="rounded" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"90px",height:'110px'}}/><br/>
                                <div style={{marginLeft:'-10px'}}> <Barcode  flat="false" value={`1mb_${this.props.match.params.id}`} width="1"  height="60"/></div>
                             </td>
                          </tr>
                      </table>
                      </div>
                    </div>  
                   <br/>
                    <Button variant='contained' color='primary' style={{"fontSize":"14px","backgroundColor":"#ad0303"}} onClick={this.handlePdf}><GetAppIcon fontSize="large"/> Exporter et visualiser en PDF</Button>&nbsp;
                    <Button variant='contained' color='primary' style={{"fontSize":"14px","backgroundColor":"#095fb5"}}><ArrowBackIcon fontSize="large"/> <Link to="/membre" style={{"color":"white"}}>Revenir vers le site</Link></Button>
                    </div>
              
               
   
  );
}
}

