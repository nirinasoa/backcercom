import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GetAppIcon from '@material-ui/icons/GetApp';
import jsPDF from 'jspdf';  
import html2canvas from 'html2canvas';  
import { Button } from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {Link} from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';
import Barcode from 'react-barcode';

export default class BarcodeMembre extends Component{
  constructor(props) {
    super(props)
    this.state={};
  }
 
    handlePdf = () => {
        const input = document.getElementById('pdfdiv');  
        html2canvas(input,{ logging: true, allowTaint: false,letterRendering: 1, useCORS: true })  
          .then((canvas) => {  
            var imgWidth = 120;  
            var pageHeight = 210;  
            var imgHeight = canvas.height * imgWidth / canvas.width;  
            var heightLeft = imgHeight;  
            const imgData = canvas.toDataURL('image/jpeg');  
            const pdf = new jsPDF('p', 'mm', 'a4')  
            var position = 10;  
            var heightLeft = imgHeight; 
            pdf.setProperties({
              DisplayDocTitle:true,
                title: 'Code-barres',
                author: 'RYN',
                creator: 'RYN'
            });
            pdf.setFontSize(13);
            pdf.text(5, 5, 'CERCOM_code_barres'); 
            pdf.setLineWidth(1)
            pdf.addImage(imgData, 'JPEG', 10, position, imgWidth, imgHeight);  
            var string = pdf.output('datauristring');
                var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
                var x = window.open();
                x.document.open();
                x.document.write(embed);
                x.document.close();
                pdf.save('1mb_'+this.props.match.params.id+'.pdf')
          });  
    };
  render(){
  
  return (
    <div className="App">  
                        <h2>Affichage simple du code-barres de ce membre</h2>
                        <hr/>
                    <div className="row">
                      <div className="col-md-4">

                      </div>
                      <div className="col-md-4"  id="pdfdiv">
                      <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Code-barres:</label><br/>
                               
                               <Barcode displayValue="true"  fontSize="16" value={`1mb_${this.props.match.params.id}`}   width="1"  height="60"/>
                              
                      </div>
                    </div>  
                   <br/>
                    <Button variant='contained' color='primary' style={{"fontSize":"14px","backgroundColor":"#ad0303"}} onClick={this.handlePdf}><GetAppIcon fontSize="large"/> Exporter et visualiser en PDF</Button>&nbsp;
                    <Button variant='contained' color='primary' style={{"fontSize":"14px","backgroundColor":"#095fb5"}}><ArrowBackIcon fontSize="large"/> <Link  to={`/detailMembre/${this.props.match.params.id}`} style={{"color":"white"}}>Revenir vers le site</Link></Button>
                    </div>
              
               
   
  );
}
}

