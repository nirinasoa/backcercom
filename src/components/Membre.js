import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Button } from '@material-ui/core';
import Avatar from '@material-ui/core/Avatar';
import Barcode from 'react-barcode';
import './style.css';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import ReactPaginate from 'react-paginate';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ReactToExcel from 'react-html-table-to-excel';
import {OutTable, ExcelRenderer} from 'react-excel-renderer';
import MicIcon from '@material-ui/icons/Mic';
import CloseIcon from '@material-ui/icons/Close';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition
const recognition = new SpeechRecognition()

recognition.continous = true
recognition.interimResults = true
recognition.lang = 'fr-FR'
export default class Membre extends Component {
  constructor(props) {
    super(props)
    this.importerExcel = this.importerExcel.bind(this)
    this.toggleListen = this.toggleListen.bind(this)
    this.handleListen = this.handleListen.bind(this)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangeadresse=this.onChangeadresse.bind(this)
    this.onChangefiliere=this.onChangefiliere.bind(this)
    this.onChangecin=this.onChangecin.bind(this)
    this.onChangedelivreA=this.onChangedelivreA.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangedomaine=this.onChangedomaine.bind(this)
    this.onChangeSexe=this.onChangeSexe.bind(this)
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.onChangeemail = this.onChangeemail.bind(this)
    this.delete=this.delete.bind(this); 
    this.enregistrer=this.enregistrer.bind(this)
    this.searchHandler=this.searchHandler.bind(this); 
    this.handlePageClick = this.handlePageClick.bind(this);
    this.reset = this.reset.bind(this);

    this.modifier=this.modifier.bind(this);
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeamodifierNom=this.onChangeamodifierNom.bind(this)
    this.handleClose=this.handleClose.bind(this);
    this.handleClose1=this.handleClose1.bind(this);
    this.onChangeamodifierPrenom=this.onChangeamodifierPrenom.bind(this)
    this.onChangeamodifierAdresse=this.onChangeamodifierAdresse.bind(this)
    this.onChangeamodifierfiliere=this.onChangeamodifierfiliere.bind(this)
    this.onChangeamodifiercin=this.onChangeamodifiercin.bind(this)
    this.onChangeamodifierdelivreA=this.onChangeamodifierdelivreA.bind(this)
    this.onChangeamodifiertelephone=this.onChangeamodifiertelephone.bind(this)
    this.onChangeamodifierdomaine=this.onChangeamodifierdomaine.bind(this)
    this.onChangeamodifieremail=this.onChangeamodifieremail.bind(this)
    this.onChangeamodifierdateNaissance=this.onChangeamodifierdateNaissance.bind(this)
    this.onChangeFile1 = this.onChangeFile1.bind(this)
		this.state={
       nom:'',
       prenom:'',
       adresse:'',
       filiere:'',
       cin:'',
       delivreA:'',
       telephone:'',
       domaine:'',
       email:'',
       datenaissance:'',
       file1:null,
       filename1:null,
       membres:[],
       membresHidden:[],
       open:false,
       open:false,
       amodifier:[],
       amodifiernom:'',
       amodifierprenom:'',
       amodifieradresse:'',
       amodifierfiliere:'',
       amodifiercin:'',
       amodifierdelivreA:'',
       amodifiertelephone:'',
       amodifierdomaine:'',
       amodifieremail:'',
       amodifierdateNaissance:'',
       amodifierphoto:'',
       getidMembre:'',
       file:null,
      filename:null,
      messageEmail:'',
      imageurl:'',
      message:'',
      offset: 0,
      perPage: 10,
      currentPage: 0,
      sexe:'',
      rows:[],
      cols:[],
      listening: false,
      finalTranscript:'',
      bgColor: '#4b63db',
      hidden:false,
      messageEmailUpload:'',
      messageSucess:'',
      fileupload:''
      };
    
    }
    insererMembre(membreAinserer,email){
       if(email!='undefined'){
        Axios.post('https://phpapiserver.herokuapp.com/api/membre/searchByEmail.php',{wordToSearch:email})
        .then(res=>{
          if(res.data.message=='Could not find data.'){
            this.setState({messageEmailUpload:''})
            Axios.post('https://phpapiserver.herokuapp.com/api/membre/create.php',membreAinserer)
            .then(res=>{
            if(res.data=='Membre could not be created.'){
              this.setState({messageEmailUpload:'Vérifiez les champs nuls comme la date de naissance ou les champs qui ne correspondent pas à la base!'})
            }
            else{
              this.setState({messageSucess:'Enregistrement effectué pour '+email})
              this.componentDidMount()
            }
          
            })
          }
          else{
            this.setState({messageEmailUpload:'Erreur d\'enregistrement ou Email existant'})
            console.log('Existant'+res.data)
          }
         
          
        })
        .catch(err=>{
          this.setState({messageEmail:''})}
        );
       }
           
     
             
    }
    importerExcel(){
      this.setState({open1:false})
      if(this.state.rows.length>0){
      let i=1;
      let j=0;
        for (i = 1; i <this.state.rows.length; i++) {
          var string =this.state.rows[i] + '';
          var tab = string.split(",");
          //tab->1 à 10
          let membre={
            nom:tab[1],
            prenom:tab[2],
            adresse:tab[12],
            filiere:tab[4],
            cin:tab[6],
            delivreA:tab[7],
            telephone:tab[8],
            domaine:tab[5],
            email:tab[3],
            dateNaissance:tab[10],
            photo:'',
            codeBarre:tab[9]
    
           }
           this.insererMembre(membre,tab[3])
          }
      }
      else{
       alert('Veuillez choisir un fichier Excel avant d\'importer!')
      }
     
    }
    reset(){
      this.setState({finalTranscript:'',message:'',messageEmailUpload:'',messageSucess:'',fileupload:''})
      this.componentDidMount()
    }
    toggleListen() {
      this.setState({
        bgColor: '#e82a33',
        listening: !this.state.listening
      }, this.handleListen)
    }
    
    handleListen(){
      if (this.state.listening) {
          recognition.start()
    
        let finalTranscript = ''
        recognition.onresult = event => {
          let interimTranscript = ''
    
          for (let i = event.resultIndex; i < event.results.length; i++) {
            const transcript = event.results[i][0].transcript;
            if (event.results[i].isFinal) finalTranscript += transcript + ' ';
            else interimTranscript += transcript;
          }
          this.setState({finalTranscript:finalTranscript});
        if(this.state.finalTranscript!=''){
          this.setState({bgColor:'#4b63db'});
            this.searchHandler1(this.state.finalTranscript)
        }
      }
    }
    else{
      recognition.stop()
      alert('Une erreur est survenue!')
     
    }
  }
    fileHandler = (event) => {
      this.setState({open1:true,fileupload:event.target.value})
      let fileObj = event.target.files[0];
  
      //just pass the fileObj as parameter
      ExcelRenderer(fileObj, (err, resp) => {
        if(err){
          console.log(err);            
        }
        else{
          this.setState({
            cols: resp.cols,
            rows: resp.rows
          });
        }
      });               
  
    }
    onChangeSexe(e){
      this.setState({
        sexe:e.target.value
      });
    }
    handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState({
          currentPage: selectedPage,
          offset: offset
      }, () => {
          this.componentDidMount()
      });

  };
  searchHandler1(e){
    var test=' résultat(s) trouvé(s)';
      Axios.post('https://phpapiserver.herokuapp.com/api/membre/search.php',{wordToSearch:e})
      .then(response=>{
        const data = response.data;
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
        const postdata = slice.map(mbr=>
          <tr>
                    <td data-label="Numéro:"><span className="label label-warning">{mbr.idMembre}</span></td>
                     <td ><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${mbr.photo}`} /> </td>
                    <td data-label="Membre:"> <Link style={{"color":"#383737"}} to={`/detailMembre/${mbr.idMembre}`}>{mbr.nom} &nbsp;{mbr.prenom}</Link></td>
                    <td data-label="Email:">{mbr.email}</td>
                    <td data-label="Filière:">{mbr.filiere}</td>
                    <td data-label="Générer carte:"><button><Link to={`/carte/${mbr.idMembre}`}><RecentActorsIcon fontSize="large"/></Link></button></td>
                    <td>
                        
                    <IconButton aria-label="Modifier">
                            <EditIcon fontSize="large" onClick={() => this.modifier(mbr.idMembre)} />
                        </IconButton>
                        </td> 
                        <td>       
                       
                        <IconButton aria-label="delete">
                            <DeleteIcon fontSize="large" onClick={() => this.delete(mbr.idMembre)} />
                        </IconButton>
                        
                    </td>
                  </tr>)
          this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
      })
      .catch(err=>{
        this.componentDidMount()
        this.setState({message:'Aucun'+test,membres:[]})
      })
    
   
  }
    searchHandler(e){
      this.setState({finalTranscript:e.target.value})
      var test=' résultat(s) trouvé(s)';
      if(e.target.value==''){
          this.componentDidMount()
      }
      else{
        Axios.post('https://phpapiserver.herokuapp.com/api/membre/search.php',{wordToSearch:e.target.value})
        .then(response=>{
          const data = response.data;
          const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
          const postdata = slice.map(mbr=>
            <tr>
                      <td data-label="Numéro:"><span className="label label-warning">{mbr.idMembre}</span></td>
                       <td ><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${mbr.photo}`} /> </td>
                      <td data-label="Membre:"> <Link style={{"color":"#383737"}} to={`/detailMembre/${mbr.idMembre}`}>{mbr.nom} &nbsp;{mbr.prenom}</Link></td>
                      <td data-label="Email:">{mbr.email}</td>
                      <td data-label="Filière:">{mbr.filiere}</td>
                      <td data-label="Générer carte:"><button><Link to={`/carte/${mbr.idMembre}`}><RecentActorsIcon fontSize="large"/></Link></button></td>
                      <td>
                          
                      <IconButton aria-label="delete">
                              <EditIcon fontSize="large" onClick={() => this.modifier(mbr.idMembre)} />
                          </IconButton>
                          </td> 
                          <td>       
                         
                          <IconButton aria-label="delete">
                              <DeleteIcon fontSize="large" onClick={() => this.delete(mbr.idMembre)} />
                          </IconButton>
                          
                      </td>
                    </tr>)
            this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
        })
        .catch(err=>{
          this.componentDidMount()
          this.setState({message:'Aucun'+test,membres:[]})
        })
      }
     
    }
    onChangeamodifierNom(e){
      this.setState({
        amodifiernom:e.target.value
      });
    }
    onChangeamodifierPrenom(e){
      this.setState({
        amodifierprenom:e.target.value
      });
    }
    onChangeamodifierAdresse(e){
      this.setState({
        amodifieradresse:e.target.value
      });
    }
    onChangeamodifierfiliere(e){
      this.setState({
        amodifierfiliere:e.target.value
      });
    }
    onChangeamodifiercin(e){
      this.setState({
        amodifiercin:e.target.value
      });
    }
    onChangeamodifierdelivreA(e){
      this.setState({
        amodifierdelivreA:e.target.value
      });
    }
    onChangeamodifiertelephone(e){
      this.setState({
        amodifiertelephone:e.target.value
      });
    }
    onChangeamodifierdomaine(e){
      this.setState({
        amodifierdomaine:e.target.value
      });
    }
    onChangeamodifieremail(e){
      this.setState({
        amodifieremail:e.target.value
      });
    }
    onChangeamodifierdateNaissance(e){
      this.setState({
        amodifierdateNaissance:e.target.value
      });
    }
    onChangeFile1(e) {
      this.setState({file1:e.target.files[0]})
      this.setState({filename1:e.target.files[0].name})
    }
    modifier(id){
      this.setState({open:true})
      console.log(id)
      Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+id)
      .then(response=>{
      
        this.setState({
          getidMembre:response.data.idMembre,
          amodifiernom:response.data.nom,
          amodifierprenom:response.data.prenom,
          amodifieradresse:response.data.adresse,
          amodifierfiliere:response.data.filiere,
          amodifiercin:response.data.cin,
          amodifierdelivreA:response.data.delivreA,
          amodifiertelephone:response.data.telephone,
          amodifierdomaine:response.data.domaine,
          amodifieremail:response.data.email,
          amodifierdateNaissance:response.data.dateNaissance,
          amodifierphoto:response.data.photo,
          
        })
      })
      .catch(error=>{
        console.log(error)
      }) 
      console.log(this.state.amodifiernom)
    };
    handleClose(){
      this.setState({open:false})
      };
     
    handleClose1(){
        this.setState({open1:false,rows:[],cols:[],fileupload:''})
    };
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/membre/deleteOne.php',{idMembre:id})
      .then(res => {
        this.componentDidMount()  
        console.log(res.data)
      
      });
     
       
     } 
     else{}
    }
    UPLOAD_ENDPOINT = 'http://phpapiserver.herokuapp.com/upload.php';
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    onChangeprenom(e){
      this.setState({
        prenom:e.target.value
      });
    }
    onChangeadresse(e){
      this.setState({
        adresse:e.target.value
      });
    }
    onChangefiliere(e){
      this.setState({
        filiere:e.target.value
      });
    }
    onChangecin(e){
      this.setState({
        cin:e.target.value
      });
    }
    onChangedelivreA(e){
      this.setState({
        delivreA:e.target.value
      });
    }
    onChangetelephone(e){
      this.setState({
        telephone:e.target.value
      });
    }
    onChangedomaine(e){
      this.setState({
        domaine:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangedatenaissance(e){
      this.setState({
        datenaissance:e.target.value
      });
    }
    onChangeFile(e) {
      this.setState({file:e.target.files[0]})
      this.setState({filename:e.target.files[0].name})
  }
  async uploadFile(file){
     
    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      data.append("public_id", originalfilename[0]);
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        this.setState({imageurl:data.url})
        console.log(data)
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
    
 
  }
  /*async uploadFile(file){
     

    const formData = new FormData();
    
    formData.append('avatar',file)
    
    return await Axios.post(this.UPLOAD_ENDPOINT, formData,{
        headers: {
            'content-type': 'multipart/form-data'
        }
    });
  }*/
    enregistrer(e){
      e.preventDefault();
      //console.log(this.state.file.name)

      let res =  this.uploadFile(this.state.file);
      const membre={
        nom:this.state.nom,
        prenom:this.state.prenom,
        adresse:this.state.adresse,
        filiere:this.state.filiere,
        cin:this.state.cin,
        delivreA:this.state.delivreA,
        telephone:this.state.telephone,
        domaine:this.state.domaine,
        email:this.state.email,
        dateNaissance:this.state.datenaissance,
        photo:this.state.filename,
        codeBarre:this.state.sexe

       }
      console.log(membre)
      //if email inexistant => create
      Axios.post('https://phpapiserver.herokuapp.com/api/membre/searchByEmail.php',{wordToSearch:this.state.email})
      .then(res=>{
        if(res.data.message=='Could not find data.'){
          this.setState({messageEmail:''})
          Axios.post('https://phpapiserver.herokuapp.com/api/membre/create.php',membre)
          .then(res=>{console.log(res.data)
          alert('membre a été crée avec succès!')
          this.componentDidMount()
          })
        }
        else{
          this.setState({messageEmail:'=>Email existant!'})
          console.log(res.data)
        }
       
        
      })
      .catch(err=>{
        this.setState({messageEmail:''})}
      );
      //else =>erreur
 
  
    
      /*
      if (window.confirm( 'Le nouveau membre a été enregistré avec succès.Voulez-vous voir son carte membre et l\'exporter en PDF?' ) ) {
          window.location.href="/cartePDF"
      } 
      else {
          window.location.href="/membre"
      }*/
    };
    componentDidMount(){
      Axios.get('https://phpapiserver.herokuapp.com/api/membre/read.php')
      .then(response=>{
         this.setState({membresHidden:response.data})
      })
      Axios.get('https://phpapiserver.herokuapp.com/api/membre/read.php')
      .then(response=>{
        const data = response.data;
        const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
        const postdata = slice.map(mbr=>
          <tr>
                    <td data-label="Numéro:"><span className="label label-warning">{mbr.idMembre}</span></td>
                     <td ><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${mbr.photo}`} /> </td>
                    <td data-label="Membre:"> <Link style={{"color":"#383737"}} to={`/detailMembre/${mbr.idMembre}`}>{mbr.nom} &nbsp;{mbr.prenom}</Link></td>
                    <td data-label="Email:">{mbr.email}</td>
                    <td data-label="Filière:">{mbr.filiere}</td>
                    <td data-label="Générer carte:"><button><Link to={`/carte/${mbr.idMembre}`}><RecentActorsIcon fontSize="large"/></Link></button></td>
                    <td >
                        
                    <IconButton aria-label="delete">
                            <EditIcon fontSize="large" onClick={() => this.modifier(mbr.idMembre)} />
                        </IconButton>
                        </td> 
                        <td>       
                       
                        <IconButton aria-label="delete">
                            <DeleteIcon fontSize="large" onClick={() => this.delete(mbr.idMembre)} />
                        </IconButton>
                        
                    </td>
                  </tr>)
           this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
      
        
      })
      .catch(error=>{
        console.log(error)
      })
     }
     onSubmit(e){
      e.preventDefault();
     
     
      if(this.state.filename1==null){
      const amodifierMembre={
        idMembre:this.state.getidMembre,
        nom:this.state.amodifiernom,
        prenom:this.state.amodifierprenom,
        adresse:this.state.amodifieradresse,
        filiere:this.state.amodifierfiliere,
        cin:this.state.amodifiercin,
        delivreA:this.state.amodifierdelivreA,
        telephone:this.state.amodifiertelephone,
        domaine:this.state.amodifierdomaine,
        email:this.state.amodifieremail,
        dateNaissance:this.state.amodifierdateNaissance,
        photo:this.state.amodifierphoto
      }
     console.log(amodifierMembre)
      Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
      .then(response=>{
         if(response){
          this.setState({open:false})
         this.componentDidMount()
          }
          else{
              this.setState({open:true}) 
          }
         })
        }
        else{
          let res =  this.uploadFile(this.state.file1);
          const amodifierMembre={
            idMembre:this.state.getidMembre,
            nom:this.state.amodifiernom,
            prenom:this.state.amodifierprenom,
            adresse:this.state.amodifieradresse,
            filiere:this.state.amodifierfiliere,
            cin:this.state.amodifiercin,
            delivreA:this.state.amodifierdelivreA,
            telephone:this.state.amodifiertelephone,
            domaine:this.state.amodifierdomaine,
            email:this.state.amodifieremail,
            dateNaissance:this.state.amodifierdateNaissance,
            photo:this.state.filename1
          }
         console.log(amodifierMembre)
          Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
          .then(response=>{
             if(response){
              this.setState({open:false,filename1:null})
              this.componentDidMount()
              }
              else{
                  this.setState({open:true}) 
              }
             })
        }

          
  
    }
  render(){
    let messageUpload;
    let messageUpload1;
    if(this.state.messageEmailUpload!=''){
      messageUpload= <span style={{color:'red',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-exclamation-triangle"></i>&nbsp;{this.state.messageEmailUpload}</span>
    }
    if(this.state.messageSucess!=''){
      messageUpload1= <span style={{color:'green',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-check"></i>&nbsp;{this.state.messageSucess}</span>
    }
  let tablehidden;
  if(this.state.hidden==false){
    tablehidden=<Accordion style={{padding:'5px 5px'}}>
                  <AccordionSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                      >
                      <Typography ><span style={{fontSize:'13px',fontFamily:'Arial'}}>©RYN 2020&nbsp;&nbsp;&nbsp;Voir  liste complète des membres</span></Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                      <Typography>
                      <table style={{width:'100%',fontFamily:'Arial',fontSize:'13px'}} id="tableMembreHidden"  className="table table-bordered table-striped" >
                          <thead>
                              <tr>
                                  <th style={{width:'3%'}} scope="col">N°</th>
                                  <th  scope="col">Nom</th>
                                  <th  scope="col">Prénoms</th>
                                  <th  scope="col">Email</th>
                                  <th scope="col">Filière</th>
                                  <th scope="col">Domaine</th>
                                  <th scope="col">CIN</th>
                                  <th scope="col">Délivré à</th>
                                  <th scope="col">Téléphone</th>
                                  <th scope="col">Sexe</th>
                                  <th scope="col">Date de naissance</th>
                                  <th scope="col">Date d'entrée</th>
                                  <th scope="col">Adresse</th>
                              </tr>
                          </thead>
                          <tbody>
                          {this.state.membresHidden.map(mbr=>
                              <tr>
                                  <td data-label="Numéro:"><span className="label label-warning">{mbr.idMembre}</span></td>
                                  <td data-label="Nom:"> {mbr.nom}</td>
                                  <td data-label="Prénoms:">{mbr.prenom}</td>
                                  <td data-label="Email:">{mbr.email}</td>
                                  <td data-label="Filière:">{mbr.filiere}</td>
                                  <td data-label="Domaine:">{mbr.domaine}</td>
                                  <td data-label="CIN:">{mbr.cin}</td>
                                  <td data-label="Délivré à:">{mbr.delivreA}</td>
                                  <td data-label="telephone:">{mbr.telephone}</td>
                                  <td data-label="Sexe:">{mbr.codeBarre}</td>
                                  <td data-label="Date de naissance:">{mbr.dateNaissance}</td>
                                  <td data-label="Date d'entrée:">{mbr.created}</td>
                                  <td data-label="Adresse:">{mbr.adresse}</td>
                              </tr>)}
                          </tbody>
                      </table>
                      </Typography>
                  </AccordionDetails>
              </Accordion>
 }
  
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
        Ajout et affichage de la liste des membres: 
       
      </h1>
     
      <ol className="breadcrumb">
      
         
          <li>

          <ReactToExcel 
             className="excel"
             sheet="Sheet"
              table="tableMembreHidden" 
              filename="Liste_Membres_CERCOM"
              submit="submit 1"
              buttonText="Exporter la liste vers Excel"
              
              />&nbsp;&nbsp;
          </li>
         
          <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
  
       
      </ol>
    </section>

   
    <section className="content">
        <div className="row">
            <div className="col-md-4">
            <div className="box box-warning">
            <div className="box-header">
            <h3 className="box-title">Ajouter un membre </h3>
              <div style={{borderRadius:'5px',border:'1px solid #dedcdc',padding:'7px 7px'}}>
            
              <span style={{fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}>Importer un fichier Excel (*.xlsx)</span>
              {messageUpload}{messageUpload1}
              <input 
              value={this.state.fileupload}
                type="file" 
                onChange={this.fileHandler.bind(this)} 
                style={{"padding":"10px"}} 
                accept=".xlsx" 
              />
              <div style={{display:'inline-block'}}>
              <button className="btn btn-warning" onClick={this.reset}>Initiliser</button>&nbsp;
              <button className="btn btn-success" onClick={this.importerExcel}>Importer</button>
             
              </div>
            
              </div>
        
            </div>
            <form role="form" onSubmit={this.enregistrer}>
                  <div className="box-body">
                    <p className="text-muted">Pour ajouter un nouveau membre, veuillez remplir les champs suivants:</p>
                    <div className="form-group">
                    <RadioGroup required row aria-label="position" name="position" defaultValue="top"  onChange={this.onChangeSexe}>
                      
                      <FormControlLabel
                        value="Masculin"
                        control={<Radio color="primary" 
                        inputProps={{ style: { width: 50 }}}
                        
                        />}
                        label={<span style={{ fontSize: '14px' }}>Masculin</span>}
                        labelPlacement="start"
                       />
                      <FormControlLabel 
                        value="Féminin"
                        control={<Radio color="primary" />}
                        label={<span style={{ fontSize: '14px' }}>Féminin</span>}
                        labelPlacement="start"
                      />
                    </RadioGroup>
                      </div>
                    <div className="form-group">
                      <label>Nom du membre</label>
                      <input required type="text" className="form-control" id="nom" placeholder="Nom..."  value={this.state.nom} onChange={this.onChangenom}/>
                    </div>
                    <div className="form-group">
                      <label>Prénom(s) du membre</label>
                      <input required type="text" className="form-control" id="prenom" placeholder="Prénom(s)..."  value={this.state.prenom} onChange={this.onChangeprenom}/>
                    </div>
              
                      <div className="form-group">
                      <label>Adresse</label>
                      <input type="text" className="form-control" id="adresse" placeholder="Adresse..."  value={this.state.adresse} onChange={this.onChangeadresse}/>
                    </div>
                    <div className="form-group">
                     
                      <label >Email&nbsp;<b style={{color:'red'}}>{this.state.messageEmail}</b></label>
                      <input  required type="email" className="form-control" id="exampleInputEmail1" placeholder="Email..."  value={this.state.email} onChange={this.onChangeemail}/>
                    </div>
                    <div className="form-group">
                      <label >CIN</label>
                      <input type="text" className="form-control" id="cin" placeholder="xxxxxxxxxx..."  value={this.state.cin} onChange={this.onChangecin}/>
                    </div>
                    <div className="form-group">
                      <label>Délivré à</label>
                      <input type="text" className="form-control" id="delivrea" placeholder="délivré à..."  value={this.state.delivreA} onChange={this.onChangedelivreA}/>
                    </div>
                    <div className="form-group">
                      <label required >Domaine</label>
                      <input type="text" className="form-control" id="domaine" placeholder="domaine..."  value={this.state.domaine} onChange={this.onChangedomaine}/>
                    </div>
                    <div className="form-group">
                      <label >Date de naissance</label>
                      <input  required type="date" className="form-control" id="datenaissance"  onChange={(event) => this.setState({datenaissance: event.target.value})}/>
                    </div>
                    <div className="form-group">
                      <label >Filière</label>
                      <input type="text" className="form-control" id="filiere" placeholder="Filière..."  value={this.state.filiere} onChange={this.onChangefiliere}/>
                    </div>
                    <div className="form-group">
                      <label >Numéro de téléphone</label>
                      <input type="text" className="form-control" id="numero" placeholder="+261 xx yyyy zz..."  value={this.state.telephone} onChange={this.onChangetelephone}/>
                    </div>
                   
                    <div className="form-group">
                      <label >Photo du membre</label>
                      <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>
                    </div>
                    
                  </div>
                 
                  <div className="box-footer">
                    
                    <button type="submit" className="btn btn-primary">Enregistrer</button>
                  </div>
            </form>
            </div>
            </div>
       
     
        <div className="col-md-8">
          <div className="box box-primary">
              <div style={{display:'inline-block'}}>
                  <h3 className="box-title">Liste des membres </h3>
                 
              </div>
            <div className="box-body">
            <TextField  value={this.state.finalTranscript} onChange={this.searchHandler} id="search" type="text" className="form-control" placeholder="Effectuer une recherche..." variant="outlined"  
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end" >
                           <IconButton onClick={this.reset}>
                                <CloseIcon  fontSize="large"/>
                          </IconButton>
                           <IconButton onClick={this.toggleListen}>
                                <MicIcon style={{"color":this.state.bgColor}} fontSize="large"/>
                          </IconButton>
                          <IconButton>
                             <SearchIcon fontSize="large"/>
                          </IconButton>

                        </InputAdornment>
                      ),
                    }}
                    inputProps={{style:{fontSize:13}}}
                  />
                <br/> <br/>
                <span style={{color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
             <table id="tableMembre"  className="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th style={{width:'8%'}} scope="col">N°</th>
                    <th style={{width:'8%'}} scope="col"></th>
                    <th style={{width:'20%'}} scope="col">Nom et prénom(s) des membres</th>
                    <th scope="col">Email</th>
                    <th scope="col">Filière</th>
                    <th style={{width:'8%'}} scope="col">Générer carte</th>
                    <th style={{width:'8%'}} scope="col">Actions</th>
                    <th style={{width:'8%'}} scope="col"></th>
                  </tr>
                </thead>
                <tbody>
                {this.state.postdata}  
                
                  
                </tbody>
                
              </table>
          
              <div style={{marginLeft:'40%'}}>
             <ReactPaginate 
                   previousLabel={<KeyboardArrowLeftIcon fontSize="default"/>}
                   nextLabel={<KeyboardArrowRightIcon fontSize="default" />}
                   breakLabel={"..."}
                   breakClassName={"break-me"}
                   pageCount={this.state.pageCount}
                   marginPagesDisplayed={2}
                   pageRangeDisplayed={5}
                   onPageChange={this.handlePageClick}
                   containerClassName={"pagination"}
                   subContainerClassName={"pages pagination"}
                   activeClassName={"active"}/>
            </div>
           
            </div>
          </div>
        </div>


      </div>
    
    </section>
    <Dialog fullWidth={true}
          maxWidth = {'xl'} open={this.state.open1} onClose={this.handleClose1} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title" style={{backgroundColor:'#016917',color:'white'}}>
          <h4> <CloseIcon style={{cursor:'pointer'}} onClick={this.handleClose1}/>&nbsp;&nbsp;Visualisation du fichier Excel </h4>
        
        </DialogTitle>
          <DialogContent dividers>
              <h5 style={{color:'#d16d0f',fontWeight:'bold',fontFamily:'Arial',fontSize:'13px'}}><i className="fa fa-exclamation-triangle"></i>&nbsp;Votre fichier Excel doit suivre les ordres des colonnes suivantes:</h5>
              <img src="assets/dist/img/modelMembreExcel.jpg"/>
              <h5 style={{color:'#525150',fontWeight:'bold',fontFamily:'Arial',fontSize:'13px'}}><i className="fa fa-arrow-right"></i>&nbsp;Le fichier Excel à importer dans la base:</h5>
             
              <OutTable data={this.state.rows} 
                  columns={this.state.cols} 
              
                 
              />  
          </DialogContent>
          <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose1} color="primary">
		  	    <h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          <Button onClick={this.importerExcel} style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
            <h6 style={{"color":"white","fontFamily":"Arial"}}>Importer</h6>
          </Button>
        </DialogActions>
      
       
          <br/>
      </Dialog>

    <Dialog fullWidth={true}
          maxWidth = {'xs'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
           <table>
              <tr>
                <td><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.amodifierphoto}`} /></td>
                <td><h4>&nbsp;{this.state.amodifiernom} &nbsp; {this.state.amodifierprenom}</h4></td>  
              </tr>
           </table>
        </DialogTitle>
        <form onSubmit={this.onSubmit}>
           
          <DialogContent dividers>
            <p>Effectuer une modification sur ce membre:</p>
            <label>Nom du membre:</label>
            <TextField
            variant="outlined"
            id="nomG"
            type="text"
            value={this.state.amodifiernom}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierNom}
          />
           <label>Prénom:</label>
           <TextField
            variant="outlined"
            id="nationaliteM"
            type="text"
            value={this.state.amodifierprenom}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierPrenom}
          />
            <label>Adresse:</label>
           <TextField
            variant="outlined"
            id="adresse"
            type="text"
            value={this.state.amodifieradresse}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierAdresse}
          />
           <label>Filiere:</label>
           <TextField
            variant="outlined"
            id="filiere"
            type="text"
            value={this.state.amodifierfiliere}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierfiliere}
          />
           <label>CIN:</label>
           <TextField
            variant="outlined"
            id="Cin"
            type="text"
            value={this.state.amodifiercin}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifiercin}
          />
           <label>Délivré à:</label>
           <TextField
            variant="outlined"
            id="delivreA"
            type="text"
            value={this.state.amodifierdelivreA}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdelivreA}
          />
           <label>Contact:</label>
           <TextField
            variant="outlined"
            id="contact"
            type="text"
            value={this.state.amodifiertelephone}
            InputProps={{ style: {fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifiertelephone}
          />
           <label>Domaine:</label>
           <TextField
            variant="outlined"
            id="domaine"
            type="text"
            value={this.state.amodifierdomaine}
            InputProps={{ style: {fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdomaine}
          />
           <label>Email:</label>
           <TextField
            variant="outlined"
            id="email"
            type="email"
            value={this.state.amodifieremail}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifieremail}
          />
           <label>Date de naissance:</label>
           <TextField
            variant="outlined"
            id="dateNN"
            type="date"
            value={this.state.amodifierdateNaissance}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdateNaissance}
          />
        
        <div className="form-group">
           <label >Modifier l'image de l'ouvrage</label>
          <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile1 }/>

         </div>
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>

      {tablehidden}
    
  </div>
   
  );
}
}
