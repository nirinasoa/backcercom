import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import { Link } from 'react-router-dom';

export default class DetailAuteur extends Component {
  constructor(props) {
    super(props)
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeauteur=this.onChangeauteur.bind(this)
    this.onChangenationalite=this.onChangenationalite.bind(this)
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
  
    this.state={
       idAuteur:this.props.match.params.id,
       nomauteur:'',
       datenaissance:'',
       nationalite:'',
       created:'',
       ouvrages:[],
       message:''
    };
  }
  onChangeauteur(e){
    this.setState({
      nomauteur:e.target.value
    });
  }
  onChangenationalite(e){
    this.setState({
      nationalite:e.target.value
    });
  }
  onChangedatenaissance(e){
    this.setState({
      datenaissance:e.target.value
    });
  }
 
  componentDidMount(){
    Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageAuteur.php/?idAuteur='+this.props.match.params.id)
      .then(response=>{
        if(response.data.length>0){
          this.setState({ouvrages:response.data,message:''})
        }
        else{
          this.setState({ouvrages:[],message:'Aucun ouvrage enregistré!'})
        }
      
        
      })
      .catch(err=>{
        this.setState({ouvrages:[],message:'Aucun ouvrrage enregistré!'})
      })
    Axios.get('https://phpapiserver.herokuapp.com/api/auteur/single_read.php/?idAuteur='+this.props.match.params.id)
    .then(response=>{
    this.setState({
      nomauteur:response.data.nomAuteur,
      datenaissance:response.data.dateNaissance,
      nationalite:response.data.nationalite,
      created:response.data.created,
     

    })})
    .catch(error=>{
      console.log(error)
    })
   };
  onSubmit(e){
    e.preventDefault();
    //alert(this.state.nomauteur)
        const auteur={
          idAuteur:this.state.idAuteur,
          nomAuteur:this.state.nomauteur,
          nationalite:this.state.nationalite,
          dateNaissance:this.state.datenaissance,
       
    
         }
        console.log(auteur)
       Axios.post('https://phpapiserver.herokuapp.com/api/auteur/update.php',auteur)
        .then(res=>console.log(res.data));
        alert('L\'auteur a été modifié avec succès!')
        // window.location.href='/dadhboard'*/
      }
    
  
 
render(){
  let message;
  if(this.state.message!=''){
    message= <h4 style={{color:'#d63c41'}}>
                <span style={{fontWeight:'bold',fontFamily:'Arial',fontSize:'15px'}}><i className="fa fa-exclamation-circle"></i>&nbsp;{this.state.message}</span>
            </h4>
  }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
       {this.state.nomauteur}
       
      </h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-person"></i> Quelques informations</a></li>
       
      </ol>
    </section>

   
    <section className="content">
      <div className="row">
        
        <div className="col-md-7">
            <div className="box box-warning">
          
                <h4 style={{marginLeft:'16px'}}>Quelques informations</h4><hr/>
                
                
                            <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nom de l'auteur:</label> {this.state.nomauteur}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nationalité:</label> {this.state.nationalite}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date de naissance:</label> {this.state.datenaissance}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date d'entrée de cette donnée:</label> {this.state.created}<br/>
                         
                            </div>
                       
                          
                <br/>
            </div>
            <h3 className="box-title">Liste des ses ouvrages</h3>
           {message}
            {this.state.ouvrages.map(ouvrage=>
              <div>
                <Paper style={{maxWidth: 750,padding: '10px 10px'}}>
                  <Grid container spacing={2}>
                    <Grid item>
                      <ButtonBase style={{margin: 'auto', display: 'block',maxWidth: '100%',maxHeight: '100%'}}>
                          <a href={ decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}><Avatar variant="rounded" src={ decodeURIComponent( decodeURIComponent(ouvrage.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))} style={{"height":"140px","width":"100px"}}/></a>
                      </ButtonBase>
                    </Grid>
                  <Grid item md={12} md container>
                    <Grid item md container direction="column" spacing={2}>
                      <Grid item md>
                        <Typography gutterBottom variant="subtitle1">
                          <span style={{fontSize:'17px',fontFamily:'Arial',color:'#4d4b4b'}}><b>{ouvrage.titre}</b></span>
                        </Typography>
                        <Typography variant="body2" gutterBottom>
                          <span style={{fontSize:'14px',fontFamily:'Arial',color:'#454343'}}>Genre: {ouvrage.nomGenre}</span> 
                        </Typography>
                        <Typography variant="body2" color="textSecondary">
                          <span style={{fontSize:'14px',fontFamily:'Arial',color:'#787575'}}>{ouvrage.description}</span>
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" style={{ cursor: 'pointer' }}>
                        <span style={{fontSize:'13px',fontFamily:'Arial',color:'#787575'}}><Link to={`/detailOuvrage/${ouvrage.idOuvrage}`}>Voir plus de détail</Link></span>
                        </Typography>
                      </Grid>
                    </Grid>
                    <Grid item>
                      <Typography variant="subtitle1">
                        <span style={{fontSize:'13px',fontFamily:'Arial',color:'#525151'}}>Nbre de page:{ouvrage.nombrePage}</span><br/>
                        <span style={{fontSize:'13px',fontFamily:'Arial',color:'#525151'}}>Quantité:{ouvrage.quantite}</span><br/>
                        <span style={{fontSize:'13px',fontFamily:'Arial',color:'#525151'}}>Année d'édition:{ouvrage.anneeEdition}</span><br/>
                        <span style={{fontSize:'13px',fontFamily:'Arial',color:'#525151'}}>Langue:{ouvrage.langue}</span>
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
              </Paper>
              <br/>
              </div>
              )}
           
        </div>
        <div className="col-md-5">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Modifier quelques informations:</h3>
            </div>
            
            <div className="box-body">
                <form onSubmit={this.onSubmit}>
                <div className="form-group">
                      <label>Nom de l'auteur</label>
                      <input type="text" className="form-control" id="auteur" value={this.state.nomauteur} onChange={this.onChangeauteur}/>
                </div>
                <div className="form-group">
                      <label>Nationalité</label>
                      <input type="text" className="form-control" id="nationalite" value={this.state.nationalite} onChange={this.onChangenationalite}/>
                </div>
                <div className="form-group">
                      <label>Date de naissance:</label>
                      <input type="date" className="form-control" id="datenaissance" value={this.state.datenaissance} onChange={this.onChangedatenaissance}/>
                </div>
                <div class="box-footer">
                    
                    <button type="submit" class="btn btn-success">Enregistrer la modification</button>
                  </div>
            </form>
             
            </div>

          </div>
        </div>
     

      </div>
      <div className="row">
        
        <div className="col-md-12">
          <div className="">
             
              <div className="box-body">
             

              </div>
          </div>
        </div>
      </div>
    </section>
  </div>
   
  );
}

}
