import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Barcode from 'react-barcode';
import html2canvas from 'html2canvas';  
import jsPDF from 'jspdf';  
import GetAppIcon from '@material-ui/icons/GetApp';
import SaveIcon from '@material-ui/icons/Save';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import {Link} from 'react-router-dom';

export default class DetailMembre extends Component {
  constructor(props) {
    super(props)
    this.convertToImage=this.convertToImage.bind(this)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangeadresse=this.onChangeadresse.bind(this)
    this.onChangefiliere=this.onChangefiliere.bind(this)
    this.onChangecin=this.onChangecin.bind(this)
    this.onChangedelivreA=this.onChangedelivreA.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangedomaine=this.onChangedomaine.bind(this)
    this.onChangedatenaissance=this.onChangedatenaissance.bind(this)
    this.onChangeemail = this.onChangeemail.bind(this)
    this.exporterPDF=this.exporterPDF.bind(this)
    this.modifier=this.modifier.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.state={
       idMembre:this.props.match.params.id,
       nom:'',
       prenom:'',
       adresse:'',
       filiere:'',
       cin:'',
       delivreA:'',
       telephone:'',
       domaine:'',
       email:'',
       datenaissance:'',
       photoM:'',
       message:'',
       estResult:'',
       isineditmode:false,
       sexe:''
      
    };
  }
  handlePdf1 = () => {
    const input = document.getElementById('pdfdiv');  
    html2canvas(input,{ logging: true, allowTaint: false,letterRendering: 1, useCORS: true })  
      .then((canvas) => {  
        var imgWidth = 120;  
        var pageHeight = 210;  
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
        const imgData = canvas.toDataURL('image/jpeg');  
        const pdf = new jsPDF('p', 'mm', 'a4')  
        var position = 15;  
        var heightLeft = imgHeight; 
        pdf.setProperties({
          DisplayDocTitle:true,
            title: 'Carte de lecteur bibliothèque ',
            author: 'RYN',
            creator: 'RYN'
        });
        pdf.setFontSize(13);
        pdf.text(5, 5, 'CERCOM_Carte de lecteur bibliothèque'); 
        pdf.setLineWidth(1)
        pdf.addImage(imgData, 'JPEG', 15, position, imgWidth, imgHeight);  
        var string = pdf.output('datauristring');
            var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
            var x = window.open();
            x.document.open();
            x.document.write(embed);
            x.document.close();
            pdf.save('carte_Bibliotheque.pdf')
      });  
};
  handlePdf = () => {
    const input = document.getElementById('pdfdiv1');  
    html2canvas(input,{ logging: true, allowTaint: false,letterRendering: 1, useCORS: true })  
      .then((canvas) => {  
        var imgWidth = 120;  
        var pageHeight = 210;  
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
        const imgData = canvas.toDataURL('image/jpeg');  
        const pdf = new jsPDF('p', 'mm', 'a4')  
        var position = 15;  
        var heightLeft = imgHeight; 
        pdf.setProperties({
          DisplayDocTitle:true,
            title: 'Carte du nouveau membre',
            author: 'RYN',
            creator: 'RYN'
        });
        pdf.setFontSize(13);
        pdf.text(5, 5, 'CERCOM_Carte du membre'); 
        pdf.setLineWidth(1)
        pdf.addImage(imgData, 'JPEG', 15, position, imgWidth, imgHeight);  
        var string = pdf.output('datauristring');
            var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
            var x = window.open();
            x.document.open();
            x.document.write(embed);
            x.document.close();
            pdf.save('carte.pdf')
      });  
};
  exporterPDF(filename){
    const input = document.getElementById('barcode');  
    html2canvas(input,{ logging: true, allowTaint: false,letterRendering: 1, useCORS: true })  
      .then((canvas) => {  
        var imgWidth = 200;  
        var pageHeight = 210;  
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
        const imgData = canvas.toDataURL('image/jpeg');  
        const pdf = new jsPDF('p', 'mm', 'a4')  
        var position = 10;  
        var heightLeft = imgHeight; 
        pdf.setProperties({
          DisplayDocTitle:true,
            title: 'Code barre',
            author: 'RYN',
            creator: 'RYN'
        });
        pdf.setFontSize(13);
        pdf.text(5, 5, 'DocFinder_Code barre_'+filename); 
        pdf.setLineWidth(1)
        pdf.addImage(imgData, 'JPEG', 10, position, imgWidth, imgHeight);  
        var string = pdf.output('datauristring');
            var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
            var x = window.open();
            x.document.open();
            x.document.write(embed);
            x.document.close();
        
      });  
  }
  convertToImage(filename){
    document.getElementById("download").addEventListener("click", function() {

      html2canvas(document.getElementById('barcode')).then(function(canvas) {
  
          console.log(canvas);
        
          var link = document.createElement('a');

          if (typeof link.download === 'string') {
      
              link.href = canvas.toDataURL();
              link.download = filename;
      
              //Firefox requires the link to be in the body
              document.body.appendChild(link);
      
              //simulate click
              link.click();
      
              //remove the link when done
              document.body.removeChild(link);
      
          } else {
      
              window.open(canvas.toDataURL());
      
          }
      });
  });
  }
  onChangenom(e){
    this.setState({
      nom:e.target.value
    });
  }
  onChangeprenom(e){
    this.setState({
      prenom:e.target.value
    });
  }
  onChangeadresse(e){
    this.setState({
      adresse:e.target.value
    });
  }
  onChangefiliere(e){
    this.setState({
      filiere:e.target.value
    });
  }
  onChangecin(e){
    this.setState({
      cin:e.target.value
    });
  }
  onChangedelivreA(e){
    this.setState({
      delivreA:e.target.value
    });
  }
  onChangetelephone(e){
    this.setState({
      telephone:e.target.value
    });
  }
  onChangedomaine(e){
    this.setState({
      domaine:e.target.value
    });
  }
  onChangeemail(e){
    this.setState({
      email:e.target.value
    });
  }
  onChangedatenaissance(e){
    this.setState({
      datenaissance:e.target.value
    });
  }
  componentDidMount(){
   Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+this.props.match.params.id)
    .then(response=>{
    this.setState({
        nom:response.data.nom,
        prenom:response.data.prenom,
        adresse:response.data.adresse,
        filiere:response.data.filiere,
        cin:response.data.cin,
        delivreA:response.data.delivreA,
        telephone:response.data.telephone,
        domaine:response.data.domaine,
        email:response.data.email,
        datenaissance:response.data.dateNaissance,
        photoM:response.data.photo,
        sexe:response.data.codeBarre

    })})
    .catch(error=>{
      console.log(error)
    })
   };
   changeEditModeNom=()=>{
    this.setState({isineditmode:!this.state.isineditmode})
     console.log("Vous avez cliqué sur le nom");
   }
   modifier(id){
    let res =  this.uploadFile(this.state.file);
   if(this.state.filename!=null){
   
    const amodifierMembre={
      idMembre:id,
      nom:this.state.nom,
      prenom:this.state.prenom,
      adresse:this.state.adresse,
      filiere:this.state.filiere,
      cin:this.state.cin,
      delivreA:this.state.delivreA,
      telephone:this.state.telephone,
      domaine:this.state.domaine,
      email:this.state.email,
      dateNaissance:this.state.datenaissance,
      photo:this.state.filename
    }
    console.log(amodifierMembre)
      Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
      .then(response=>{
        window.alert('Membre modifié!')
      })
    }
    else{
      const amodifierMembre={
        idMembre:id,
        nom:this.state.nom,
        prenom:this.state.prenom,
        adresse:this.state.adresse,
        filiere:this.state.filiere,
        cin:this.state.cin,
        delivreA:this.state.delivreA,
        telephone:this.state.telephone,
        domaine:this.state.domaine,
        email:this.state.email,
        dateNaissance:this.state.datenaissance,
        photo:this.state.photoM
      }
      console.log(amodifierMembre)
        Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
        .then(response=>{
          window.alert('Membre modifié!')
        })
    }
   }
   UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
   onChangenom(e){
     this.setState({
       nom:e.target.value
     });
   }
   onChangeFile(e) {
    this.setState({file:e.target.files[0]})
    this.setState({filename:e.target.files[0].name})
}
async uploadFile(file){
   

  const formData = new FormData();
  
  formData.append('avatar',file)
  
  return await Axios.post(this.UPLOAD_ENDPOINT, formData,{
      headers: {
          'content-type': 'multipart/form-data'
      }
  });
}
render(){
  let editMode;
  if(this.state.isineditmode==true){
    editMode=<input type="text" value={this.state.nom} />
  }
  else{

  }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
      Fiche détaillée du membre
       
      </h1>
     
      <ol className="breadcrumb">
      <Button variant='contained' color='primary' style={{textTransform: 'capitalize',"fontSize":"13px","backgroundColor":"#ad0303"}} onClick={this.handlePdf}><GetAppIcon fontSize="large"/>PDF carte membre</Button>&nbsp;&nbsp;
      <Button variant='contained' color='primary' style={{textTransform: 'capitalize',"fontSize":"13px","backgroundColor":"#ad0303"}} onClick={this.handlePdf1}><GetAppIcon fontSize="large"/>PDF carte bibliothèque</Button>
       &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </ol>
    </section>

   
    <section className="content">
      <div className="row">
        
        <div className="col-md-5">
            <div className="box box-warning">
          
            <h4 style={{"textAlign":"center"}}>{this.state.nom} &nbsp;{this.state.prenom}</h4><hr/>
                       <Avatar src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"100px",height:'100px',marginLeft:'50px'}}/>
                       {(()=>{
                    if(this.state.isineditmode==true){
                        return   (
                          <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nom:</label><input  type="text" value={this.state.nom} onChange={this.onChangenom} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prénom(s):</label> <input   type="text" value={this.state.prenom} onChange={this.onChangeprenom} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Adresse:</label><input   type="text" value={this.state.adresse} onChange={this.onChangeadresse}  /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Filière:</label> <input  type="text" value={this.state.filiere} onChange={this.onChangefiliere} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;CIN:</label><input   type="text" value={this.state.cin} onChange={this.onChangecin} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Téléphone:</label> <input   type="text" value={this.state.telephone} onChange={this.onChangetelephone}  /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Domaine:</label> <input   type="text" value={this.state.domaine} onChange={this.onChangedomaine} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Email:</label><input   type="email" value={this.state.email} onChange={this.onChangeemail} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date de naissance:</label> <input   type="date" value={this.state.dateNaissance} onChange={this.onChangedatenaissance} /><br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Code-barres généré:</label><br/>
                          <label for="exampleInputFile">Modifier le photo du membre</label>
                          <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>
                          <div id="barcode" style={{marginLeft:'20px'}} ><Barcode   value={`1mb_${this.props.match.params.id}`}   width="1"  height="60"/></div>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Acton sur le code-barres:</label><br/>
                          <Link size="small" style={{marginLeft:'20px'}} id="download" onClick={()=>this.convertToImage(`1mb_${this.props.match.params.id}`)}><GetAppIcon fontSize="large" />&nbsp;Enregistrer (Code-barres)</Link>&nbsp;&nbsp;
                          <Link  style={{marginLeft:'20px'}} id="pdf" onClick={()=>this.exporterPDF(`1mb_${this.props.match.params.id}`)}><GetAppIcon fontSize="large" style={{color:'white'}}/>Expoter vers PDF</Link><br/><hr/>
                          <Button size="small" style={{textTransform: 'uppercase',backgroundColor:'#1569b3',color:'white',marginLeft:'50%'}} onClick={()=>this.modifier(this.props.match.params.id)}><SaveIcon fontSize="large" /><h5>Enregistrer</h5></Button>&nbsp;&nbsp;
                          <Button size="small" style={{textTransform: 'uppercase',backgroundColor:'#e04338',color:'white'}} onClick={this.changeEditModeNom}><HighlightOffIcon fontSize="large" style={{color:'white'}}/><h5>Annuler</h5></Button>
                      </div>

                        ) 
                       
                    }
                    if(this.state.isineditmode==false){
                        return   (
                          <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nom:</label>{this.state.nom}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prénom(s):</label> {this.state.prenom}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Adresse:</label> {this.state.adresse}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Filière:</label> {this.state.filiere}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;CIN:</label> {this.state.cin}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Téléphone:</label> {this.state.telephone}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Domaine:</label> {this.state.domaine}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Email:</label> {this.state.email}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date de naissance:</label> {this.state.datenaissance}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Code-barres généré:</label><br/>
                          <div id="barcode" style={{marginLeft:'20px'}} ><Barcode   value={`1mb_${this.props.match.params.id}`}   width="1"  height="60"/></div>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Acton sur le code-barres:</label><br/>
                         
                          <Link size="small" style={{marginLeft:'20px'}} to={`/barcodeMembre/${this.props.match.params.id}`} ><GetAppIcon fontSize="large"/>Visualiser ce code-barres et Expoter(PDF)</Link><br/><hr/>
                          <Button  size="small" style={{marginLeft:'60%',textTransform: 'uppercase',backgroundColor:'#2a6313',color:'white'}}  onClick={this.changeEditModeNom}><EditIcon fontSize="large" style={{color:'white'}}/><h5>Modifier&nbsp;</h5></Button>
                      </div>
                        )
                 }

                
                })()}
 
                      
                        <br/>
            </div>
         
        </div>
        <div className="col-md-6">
       <h4>Carte du membre</h4>
        <div  id="pdfdiv1" style={{backgroundColor:'white',"border":"1px solid #aba9a9",width:'500px'}}>
        
        <table>
          <tr>
            <td style={{paddingLeft:'40px',fontFamily:'Arial'}}>
              <b style={{fontSize:'13px'}}>UNIVERSITE D'ANTANANARIVO<br/>
              Faculté de/d'{this.state.filiere}<br/> de {this.state.domaine}</b>
            </td>
            <td style={{paddingLeft:'80px'}}> <br/> <Avatar variant="rounded" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"100px",height:'120px'}}/></td>
          </tr>
          </table>
           <p style={{paddingLeft:'40px',color:'#242323',fontFamily:'Arial'}}>
              N° Matricule:  {this.state.idMembre}<br/>
              Nom:  {this.state.nom}<br/>
              Prénoms:  {this.state.prenom}<br/>
              Né(e) le:  {this.state.datenaissance}<br/>
              Sexe:  {this.state.sexe}<br/>
              Adresse:  {this.state.adresse}<br/>
           </p>
           <p style={{paddingLeft:'40px',fontFamily:'Arial',fontSize:13,color:'#242323'}}>
             Le chef de service de la scolarité <span style={{paddingLeft:'50px'}}>Signature de l'étudiant</span>
             <br/>  <br/><br/>
             RAVELONJATO Haja Herilala<span style={{paddingLeft:'80px'}}>Id:{this.state.idMembre}</span><br/><br/>
           </p>
          
    
      </div>
      <h4>Carte de lecteur bibliothèque</h4>
        <div  id="pdfdiv" style={{backgroundColor:'white',"border":"1px solid #aba9a9",width:'550px'}}>
        
                        <table>
                          <tr>
                          <td style={{width:'78%',color:'black'}}>
                                <img src="assets/dist/img/logoAnkatso.png" width="70"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="assets/dist/img/logocarte.png" width="270"/><br/>
                                 <span style={{"textAlign":"center","fontWeight":"bolder","fontSize":"13px","fontFamily":"Arial"}}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 CARTE D'ADHERENT</span><br/><br/>
                                <p style={{"textAlign":"justify","fontFamily":"Arial"}}>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Nom:</label> {this.state.nom}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Prénoms:</label>  {this.state.prenom}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Adresse:</label>  {this.state.adresse} <br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Filière:</label> {this.state.filiere}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>CIN:</label>  {this.state.cin}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Délivré à:</label> {this.state.delivreA}<br/>
                                &nbsp;&nbsp;<label style={{"fontSize":"13px"}}>Télephone: </label> {this.state.telephone}<br/>
                                &nbsp;&nbsp;Signature de l'étudiant &nbsp; &nbsp; &nbsp; &nbsp;Le directeur de la Bibliothèque  <br/><span style={{"marginLeft":"177px"}}>et Archives Universitaire</span><br/><br/>
                                </p>
                             </td>
                             <td style={{width:'25%'}}>
                                    <img  src="assets/dist/img/logoBook.PNG" width="70" /><br/><br/>
                                 <Avatar variant="rounded" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"100px",height:'120px'}}/><br/>
                                 <Barcode  flat="false" value={`1mb_${this.props.match.params.id}`} width="1"  height="65"/>
                             </td>
                          </tr>
                      </table>
                      </div>
                      </div>
            
        <div className="col-md-1">
          <br/>
         
       
        </div>

      </div>
    </section>
  </div>
   
  );
}

}
