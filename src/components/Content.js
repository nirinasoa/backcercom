import React, { Component } from 'react';
import {Switch,Route,Redirect} from 'react-router-dom'
import Dashboard from './Dashboard';
import Widgets from './Widgets';
import Header from './Header';
import Sidebar from './Sidebar';
import LectureSP from './LectureSP';
import Membre from './Membre';
import Admin from './Admin';
import Ouvrage from './Ouvrage';
import Profil from './Profil';
import DetailOuvrage from './DetailOuvrage';
import Statistique from './Statistique';
import Carte from './Carte';
import CartePDF from './CartePDF';
import Genre from './Genre';
import Auteur from './Auteur';
import BareCodeReader from './BareCodeReader';
import DetailMembre from './DetailMembre';
import OuvrageAuto from './OuvrageAuto';
import DemandeAcces from './DemandeAcces';
import DetailAuteur from './DetailAuteur';
import Apa from './Apa';
import UM from './UM';
import Speech from './Speech';
import BarcodeOuvrage from './BarcodeOuvrage';
import BarcodeMembre from './BarcodeMembre';
import BookSearchOnline from './BookSearchOnline';


export default class Content extends Component {
  constructor(props){
      super(props);
      
      const isLoggedIn=  window.sessionStorage.getItem("loggedIn");

      let loggedIn = true;
      if(isLoggedIn==null){
          loggedIn=false ;
      }
      this.state={
          loggedIn
      }
  }

  render() {
    if(this.state.loggedIn === false){
            
        return  <Redirect to="/"/>
    }
  return (
    <div className="wrapper">
    
        <Header/>
        <Sidebar/>
       
        <Switch>
             <Route  path="/dashboard" component={Dashboard} exact/>
             <Route  path="/speech" component={Speech} exact/>
             <Route  path="/UM" component={UM} exact/>
            <Route  path="/widgets" component={Widgets} exact/>
            <Route  path="/lectureSP" component={LectureSP} sexact/>
            <Route  path="/membre" component={Membre} exact/>
            <Route  path="/admin" component={Admin} exact/>
            <Route  path="/ouvrage" component={Ouvrage} exact/>
            <Route  path="/profil" component={Profil} exact/>
            <Route  path="/detailOuvrage/:id" component={DetailOuvrage} exact/>
            <Route  path="/detailMembre/:id" component={DetailMembre} exact/>
            <Route  path="/detailAuteur/:id" component={DetailAuteur} exact/>
            <Route  path="/statistique" component={Statistique} exact/>
            <Route  path="/carte/:id" component={Carte} exact/>
            <Route  path="/barcodeOuvrage/:id" component={BarcodeOuvrage} exact/>
            <Route path='/barcodeMembre/:id' component={BarcodeMembre} exact/>
            <Route  path="/genre" component={Genre} exact/>
            <Route  path="/auteur" component={Auteur} exact/>
            <Route  path="/apa" component={Apa} exact/>
            <Route  path="/barecodereader" component={BareCodeReader} exact/>
            <Route  path="/ouvrageauto" component={OuvrageAuto} exact/>
            <Route  path="/demandeAcces" component={DemandeAcces} exact/>
            <Route  path="/booksearch" component={BookSearchOnline} exact/>
            
        </Switch>
    </div>
  );
}
}


