import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';
import Moment from 'moment';
import 'moment/locale/fr';

export default class BookSearchOnline extends Component {
  constructor(props) {
    super(props)
    
    this.onChangeconfirmpassword=this.onChangeconfirmpassword.bind(this)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangepassword=this.onChangepassword.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.onChangesearch=this.onChangesearch.bind(this)
    this.enregistrer=this.enregistrer.bind(this)
    this.fetchData = this.fetchData.bind(this);
    this.insererOuvrage = this.insererOuvrage.bind(this);
		this.state={
       nom:'',
       prenom:'',
       telephone:'',
       email:'',
       password:'',
       admnins:[],
       file:null,
       filename:null,
       confirmpassword:'',
       errorpassword:'',
       errorweakpassword:'',
       errorchamp:'',
       search:'',
       ouvrages:[],
       messageSuccess:'',
       messageError:'',
       taille:''
     
      
      };
    
    }
    creerOuvrage(ouvrage,titre,nomAuteur){
      if((titre=='' )||(nomAuteur=='')){
        this.setState({messageError:'Le titre ou l\'auteur de l\'ouvrage ne peut pas être nul'})
      }
      else{
          Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/create.php',ouvrage)
          .then(res=>{ 
            if(res.data=='Ouvrage created successfully.'){
                this.setState({messageSuccess:'Enregistrement effectué pour: '+''+titre})
                  this.componentDidMount()
                
              }
            else{
              this.setState({messageError:'Erreur d\'enregistrement pour: '+titre})
              //window.alert('Erreur d\'enregistrement. Veuillez réessayer!')
            }
          })
      }
  }
    insererOuvrage(photo,titre,editeur,nombrePage,prix,langue,annee,dateEntree,quantite,nomAuteur,origine,lieuEdition,description,nomGenre){
      if(nomAuteur!='undefined' && nomGenre!='undefined'){
            Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+nomAuteur)
            .then(response=>{this.setState({idAuteurFromColumn:response.data.idAuteur})
          
            Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
            .then(res=>{
              if(res.data.message==null){
                  //if genre miexiste=> getIdgenre
                  this.setState({idGenreFromColumn:res.data.idGenre })
                  const ouvrage={
                    idAuteur:this.state.idAuteurFromColumn,
                    titre:titre,
                    editeur:editeur,
                    nombrePage:nombrePage,
                    prix:prix,
                    idGenre:this.state.idGenreFromColumn,
                    langue:langue,
                    anneeEdition:Moment(annee).format("YYYY"),
                    dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                    quantite:quantite,
                    etatActuel:'NL',
                    origine:origine,
                    lieuEdition:lieuEdition,
                    description:description,
                    codeBarre:'',
                    photo:photo
          
                  }
                  console.log(ouvrage)
                   this.creerOuvrage(ouvrage,titre,nomAuteur)
              }
              else{
                const genre={
                  nomGenre:nomGenre,
                  description:''
                }
                console.log(genre)
                Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
                .then(resp=>{
                      console.log(resp.data)
                      Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
                      .then(res1=>{
                          this.setState({idGenreFromColumn:res1.data.idGenre })
                          const ouvrage={
                              idAuteur:this.state.idAuteurFromColumn,
                              titre:titre,
                              editeur:editeur,
                              nombrePage:nombrePage,
                              prix:prix,
                              idGenre:this.state.idGenreFromColumn,
                              langue:langue,
                              anneeEdition:Moment(annee).format("YYYY"),
                              dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                              quantite:quantite,
                              etatActuel:'NL',
                              origine:origine,
                              lieuEdition:lieuEdition,
                              description:description,
                              codeBarre:'',
                              photo:photo
                  
                          }
                          console.log(ouvrage)
                          this.creerOuvrage(ouvrage,titre,nomAuteur)
                      })
                });
              }
            

            })
          
            })
            .catch(error=>{
              console.log('auteur not found')
              const auteur={
                nomAuteur:nomAuteur,
                dateNaissance:'1900-01-01',
                nationalite:''
        
              }
              console.log(auteur)
              Axios.post('https://phpapiserver.herokuapp.com/api/auteur/create.php',auteur)
              .then(res=>{
                console.log(res.data)
              //find idauteur where auteur
              Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+nomAuteur)
            .then(response=>{
              
              this.setState({idAuteurFromColumn:response.data.idAuteur})
            Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
            .then(res=>{
              if(res.data.message==null){
                //if genre miexiste=> getIdgenre
                this.setState({idGenreFromColumn:res.data.idGenre })
                          const ouvrage={
                              idAuteur:this.state.idAuteurFromColumn,
                              titre:titre,
                              editeur:editeur,
                              nombrePage:nombrePage,
                              prix:prix,
                              idGenre:this.state.idGenreFromColumn,
                              langue:langue,
                              anneeEdition:Moment(annee).format("YYYY"),
                              dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                              quantite:quantite,
                              etatActuel:'NL',
                              origine:origine,
                              lieuEdition:lieuEdition,
                              description:description,
                              codeBarre:'',
                              photo:photo
        
                            }
                console.log(ouvrage)
                this.creerOuvrage(ouvrage,titre,nomAuteur)
            }
            else{
              const genre={
                nomGenre:nomGenre,
                description:''
              }
              console.log(genre)
              Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
              .then(resp=>{
                    console.log(resp.data)
                    Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+nomGenre)
                    .then(res1=>{
                        this.setState({idGenreFromColumn:res1.data.idGenre })
                            const ouvrage={
                                  idAuteur:this.state.idAuteurFromColumn,
                                  titre:titre,
                                  editeur:editeur,
                                  nombrePage:nombrePage,
                                  prix:prix,
                                  idGenre:this.state.idGenreFromColumn,
                                  langue:langue,
                                  anneeEdition:Moment(annee).format("YYYY"),
                                  dateEntree:Moment().format("YYYY-MM-DDThh:mm"),
                                  quantite:quantite,
                                  etatActuel:'NL',
                                  origine:origine,
                                  lieuEdition:lieuEdition,
                                  description:description,
                                  codeBarre:'',
                                  photo:photo
                    
                            }
                        console.log(ouvrage)
                        this.creerOuvrage(ouvrage,titre,nomAuteur)
                          
                    })
              });
            }
          
          
            })
          
            })
              
              });
              //console.log(error)
            }) 
      }
     
      else{
        console.log('Misy tsisy na tsisy daoly ny ateur na genre')
      }
      //getIdAuteur
      //getIdGenre
      //inserer

    }
    fetchData(word) {
     
      fetch(`https://www.googleapis.com/books/v1/volumes?q=${encodeURIComponent(word)}`)
          .then(response => response.json() )
          .then(responseJson => {
         // console.log('donnees='+responseJson.items)

         // console.log('titre='+responseJson.items.volumeInfo.title)
          var data = [];
          //data.push(responseJson.items[0]);
         
          for(var i in responseJson.items) {
            data.push(responseJson.items[i]);
        }
        
        console.log(data);
        this.setState({taille:data.length})
          console.log('taille='+data.length);
          this.setState({ ouvrages:data});
           
       })
      .catch(error => {
        console.error(error);
      });
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    
    onChangesearch(e){
      
      this.fetchData(e.target.value)
      this.setState({
        search:e.target.value
      });
    }
    onChangeprenom(e){
      this.setState({
        prenom:e.target.value
      });
    }
    onChangeconfirmpassword(e){
      this.setState({
        confirmpassword:e.target.value
      });
      if(this.state.password!== e.target.value){
        this.setState({
          errorpassword:'->Mot de passe différent'
        });
      }
      else{
        this.setState({
          errorpassword:''
        });
      }
    }
    onChangetelephone(e){
      this.setState({
        telephone:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangepassword(e){
      console.log("Nombre password"+e.target.value.split('').length)
      if(e.target.value.split('').length<4){
        this.setState({errorweakpassword:'->Mot de passe trop court'})
      }
      else{
        this.setState({errorweakpassword:''})
      }
      this.setState({
        password:e.target.value
      });
    }
    onChangeFile(e) {
      this.setState({file:e.target.files[0]})
      this.setState({filename:e.target.files[0].name})
  }
  async uploadFile(file){
    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      data.append("public_id", originalfilename[0]);
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
  }
    enregistrer(e){
      e.preventDefault();
      

   };
  componentDidMount(){
    Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/read.php')
    .then(res=> 
        this.setState({admnins:res.data})
    )};
  render(){
    let messageError;
    let messageSuccess;
    if(this.state.messageError!=''){
      messageError= <span style={{color:'red',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-exclamation-triangle"></i>&nbsp;{this.state.messageError}</span>
    }
    if(this.state.messageSuccess!=''){
      messageSuccess= <span style={{color:'green',fontWeight:'bold',fontSize:'13px',fontFamily:'Arial'}}><br/><i className="fa fa-check"></i>&nbsp;{this.state.messageSuccess}</span>
    }
  return (
    <div className="content-wrapper">
     
    <section className="content-header">
      <h1>
        Recherche des livres en ligne<br/>
        <h4 className="text-muted">Vous pouvez ajouter des livres par rapport aux résulats obtenus</h4>
      </h1>
      <ol className="breadcrumb">
      <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
      </ol>
    </section>

 
    <section className="content">
      <div className="row">
          <div className="col-md-6">
              <div class="input-group margin">
                    <input type="text"
                        className="form-control"
                        value={this.state.search}
                        onChange={this.onChangesearch}
                        placeholder="Rechercher un mot clé,nom de l'auteur,ISBN,titre ou autre..."
                     />
                    <span className="input-group-btn">
                      <button className="btn btn-info btn-flat" type="button"><i className="fa fa-search"></i></button>
                    </span>
              </div>

          </div>
      </div>
      <div className="row">
        <div className="col-md-12">
        
          <ul className="timeline">
            <li className="time-label">
              <span className="bg-green">
               Recherche
              </span>
            </li>
            
          
            <li>
              <i className="fa fa-book bg-purple"></i>
              <div className="timeline-item">
                <span className="time" ><i className="fa fa-search-o"></i><b>{this.state.taille}&nbsp;Résultat(s) trouvé(s)</b> </span>
                <h3 className="timeline-header"><a >Le résulat de la recherche par mot clé s'affiche ici.</a> </h3>
                &nbsp;&nbsp;{messageError}{messageSuccess}
                <div className="timeline-body">
                  <div className="row">
                  <div className="box-body no-padding">
                  <ul className="users-list clearfix">
                 {this.state.ouvrages.map(ouvr=>
                      
                      
                        <li style={{display:'inline',textAlign:'justify'}}>
                           {(() => {
                             if((ouvr.volumeInfo.imageLinks!=null)&&(ouvr.volumeInfo.title!=null) &&(ouvr.volumeInfo.publisher!=null)
                             &&(ouvr.volumeInfo.pageCount!=null)&&(ouvr.volumeInfo.language!=null) && (ouvr.volumeInfo.authors!=null)
                             &&(ouvr.volumeInfo.categories!=null)
                             
                             ){
                               return(
                                <button onClick={()=>
                                  this.insererOuvrage(ouvr.volumeInfo.imageLinks.thumbnail,ouvr.volumeInfo.title,
                                    ouvr.volumeInfo.publisher,ouvr.volumeInfo.pageCount,'100',ouvr.volumeInfo.language,
                                    ouvr.volumeInfo.publishedDate,'','5',
                                    ouvr.volumeInfo.authors[0],'','',
                                    ouvr.volumeInfo.description,ouvr.volumeInfo.categories[0])
                              } 
                              className="btn btn-success">
                              <i className="fa fa-plus"></i>
                              </button>
                               )
                               
                             }
                             else if((ouvr.volumeInfo.imageLinks!=null)&&(ouvr.volumeInfo.title!=null) &&(ouvr.volumeInfo.publisher!=null)
                             &&(ouvr.volumeInfo.pageCount!=null)&&(ouvr.volumeInfo.language!=null) && (ouvr.volumeInfo.authors!=null)
                             &&(ouvr.volumeInfo.categories==null)
                             
                             ){
                               return(
                                <button onClick={()=>
                                  this.insererOuvrage(ouvr.volumeInfo.imageLinks.thumbnail,ouvr.volumeInfo.title,
                                    ouvr.volumeInfo.publisher,ouvr.volumeInfo.pageCount,'100',ouvr.volumeInfo.language,
                                    ouvr.volumeInfo.publishedDate,'','5',
                                    ouvr.volumeInfo.authors[0],'','',
                                    ouvr.volumeInfo.description,'Standard')
                              } 
                              className="btn btn-default">
                              <i className="fa fa-plus"></i>
                              </button>
                               )
                             
                             }
                             else if((ouvr.volumeInfo.imageLinks!=null)&&(ouvr.volumeInfo.title!=null) &&(ouvr.volumeInfo.publisher==null)
                             &&(ouvr.volumeInfo.pageCount!=null)&&(ouvr.volumeInfo.language!=null) && (ouvr.volumeInfo.authors!=null)
                             &&(ouvr.volumeInfo.categories!=null)
                             
                             ){
                               return(
                                <button onClick={()=>
                                  this.insererOuvrage(ouvr.volumeInfo.imageLinks.thumbnail,ouvr.volumeInfo.title,
                                    '',ouvr.volumeInfo.pageCount,'100',ouvr.volumeInfo.language,
                                    ouvr.volumeInfo.publishedDate,'','5',
                                    ouvr.volumeInfo.authors[0],'','',
                                    ouvr.volumeInfo.description,'Standard')
                              } 
                              className="btn btn-default">
                              <i className="fa fa-plus"></i>
                              </button>
                               )
                             
                             }
                             else if((ouvr.volumeInfo.imageLinks==null)&&(ouvr.volumeInfo.title!=null) &&(ouvr.volumeInfo.publisher==null)
                             &&(ouvr.volumeInfo.pageCount!=null)&&(ouvr.volumeInfo.language!=null) && (ouvr.volumeInfo.authors!=null)
                             &&(ouvr.volumeInfo.categories!=null)
                             
                             ){
                               return(
                                <button onClick={()=>
                                  this.insererOuvrage('',ouvr.volumeInfo.title,
                                    '',ouvr.volumeInfo.pageCount,'100',ouvr.volumeInfo.language,
                                    ouvr.volumeInfo.publishedDate,'','5',
                                    ouvr.volumeInfo.authors[0],'','',
                                    ouvr.volumeInfo.description,'Standard')
                              } 
                              className="btn btn-default">
                              <i className="fa fa-plus"></i>
                              </button>
                               )
                             
                             }
                             else if((ouvr.volumeInfo.authors==null)&&(ouvr.volumeInfo.categories!=null)){
                              return(
                                <button onClick={()=>
                                  this.insererOuvrage(ouvr.volumeInfo.imageLinks.thumbnail,ouvr.volumeInfo.title,
                                    ouvr.volumeInfo.publisher,ouvr.volumeInfo.pageCount,'100',ouvr.volumeInfo.language,
                                    ouvr.volumeInfo.publishedDate,'','5',
                                    'Standard','','',
                                    ouvr.volumeInfo.description,'Standard')
                              } 
                              className="btn btn-warning">
                              <i className="fa fa-plus"></i>
                              </button>
                               )
                             }
                            
                            
                            }
                             
                             
                             )()}
                              
                            {(() => {
                                  if(ouvr.volumeInfo.imageLinks!=null){
                                      return( <Avatar style= {{width:180,height:180}}variant="rounded" src={ouvr.volumeInfo.imageLinks.thumbnail} alt="..." className='img-rounded' />
                                      )
                                  }
                                  if(ouvr.volumeInfo.imageLinks==null){
                                    return( <Avatar style= {{width:180,height:180}}variant="rounded"><i className="fa fa-book"></i></Avatar>)
                                  }
                            })()}
                             <a className="users-list-name" href="#">{ouvr.volumeInfo.title}</a>
                            
                           {(() => {
                                  if(ouvr.volumeInfo.authors!=null){
                                      return(<span className="users-list-date">Auteur:&nbsp;{ouvr.volumeInfo.authors}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.authors==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.authors}</span>)
                                  }
                            })()}
                             {(() => {
                                  if(ouvr.volumeInfo.categories!=null){
                                      return(   <span className="users-list-date">Genre: {ouvr.volumeInfo.categories}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.categories==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.categories}</span>)
                                  }
                            })()}
                             {(() => {
                                  if(ouvr.volumeInfo.publisher!=null){
                                      return(   <span className="users-list-date">Editeur: {ouvr.volumeInfo.publisher}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.publisher==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.publisher}</span>)
                                  }
                            })()}
                             {(() => {
                                  if(ouvr.volumeInfo.language!=null){
                                      return(   <span className="users-list-date">Langue: {ouvr.volumeInfo.language}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.language==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.language}</span>)
                                  }
                            })()}
                             {(() => {
                                  if(ouvr.volumeInfo.publishedDate!=null){
                                      return(   <span className="users-list-date">Année d'édition: {ouvr.volumeInfo.publishedDate}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.publishedDate==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.publishedDate}</span>)
                                  }
                            })()}
                             {(() => {
                                  if(ouvr.volumeInfo.pageCount!=null){
                                      return(<span className="users-list-date">Nombre de page: {ouvr.volumeInfo.pageCount}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.pageCount==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.pageCount}</span>)
                                  }
                            })()}
                           {(() => {
                                  if(ouvr.volumeInfo.industryIdentifiers!=null){
                                      return(<span className="users-list-date">ISBN:&nbsp;{ouvr.volumeInfo.industryIdentifiers[0].identifier}</span>
                                      )
                                  }
                                  if(ouvr.volumeInfo.industryIdentifiers==null){
                                    return(<span className="users-list-date">{ouvr.volumeInfo.industryIdentifiers}</span>)
                                  }
                            })()}
                          
                            
                     
                        </li>
                )}
                   </ul>
                  </div>
             
                  </div>
                  

                 </div>
                
              </div>
            </li>
          
         
            
            <li>
              <i style={{color:'white',backgroundColor:'#ebb746'}} className="fa fa-search"></i>
            </li>
          </ul>
        </div>
      </div>

     

    </section>
  </div>
  );
}
}
