import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import { Link } from 'react-router-dom';

export default class Admin extends Component {
  constructor(props) {
    super(props)
    
    this.onChangeconfirmpassword=this.onChangeconfirmpassword.bind(this)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangepassword=this.onChangepassword.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)

    this.enregistrer=this.enregistrer.bind(this)

		this.state={
       nom:'',
       prenom:'',
       telephone:'',
       email:'',
       password:'',
       admnins:[],
       file:null,
       filename:null,
       confirmpassword:'',
       errorpassword:'',
       errorweakpassword:'',
       errorchamp:''
     
      
      };
    
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    onChangeprenom(e){
      this.setState({
        prenom:e.target.value
      });
    }
    onChangeconfirmpassword(e){
      this.setState({
        confirmpassword:e.target.value
      });
      if(this.state.password!== e.target.value){
        this.setState({
          errorpassword:'->Mot de passe différent'
        });
      }
      else{
        this.setState({
          errorpassword:''
        });
      }
    }
    onChangetelephone(e){
      this.setState({
        telephone:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangepassword(e){
      console.log("Nombre password"+e.target.value.split('').length)
      if(e.target.value.split('').length<4){
        this.setState({errorweakpassword:'->Mot de passe trop court'})
      }
      else{
        this.setState({errorweakpassword:''})
      }
      this.setState({
        password:e.target.value
      });
    }
    onChangeFile(e) {
      this.setState({file:e.target.files[0]})
      this.setState({filename:e.target.files[0].name})
  }
  async uploadFile(file){
    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      data.append("public_id", originalfilename[0]);
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
  }
    enregistrer(e){
      e.preventDefault();
      //Verif is mail valid
      if(new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)==true){
          if(this.state.errorweakpassword!='->Mot de passe trop court'){
            if(this.state.password!=this.state.confirmpassword){
              this.setState({
                errorchamp:'',
                errorpassword:'->Mot de passe différent'
              });
            }
            else{
              //alert('on peut inscrire')
              //find dans table utilisateur admin where email=this.state.email
              Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/findByEmail.php/?email='+this.state.email)
              .then(response=>{
                //Si oui->Email existant dans la base de données
                if(response.data.email!=null){
                  this.setState({
                  
                    errorchamp:'->Email existant!'
                  });
                }
               
                
              })
               .catch(error=>{
                //Sinon ->Vous pouvez inscrire sans blocage
                console.log('non existant')
                let res =  this.uploadFile(this.state.file);
                  const admin={
                    nom:this.state.nom,
                    prenom:this.state.prenom,
                    email:this.state.email,
                    contact:this.state.telephone,
                    motdepasse:this.state.password,
                    photo:this.state.filename

                  }
                  console.log(admin)
                Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/register.php',admin)
                  .then(res=>{
                    alert('Le compte admin a été enregistré avec succès!')
                    Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/read.php')
                    .then(response=>{
                      this.setState({admnins:response.data})
                      
                    })
                  });
                 
               }) 

                
            }
          }
          else{
            this.setState({errorweakpassword:'Mot de passe trop court'})
          }
      }
      else{
        this.setState({ errorchamp:'->Email invalide'})
      }
        

   };
  componentDidMount(){
    Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/read.php')
    .then(res=> 
        this.setState({admnins:res.data})
    )};
  render(){
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
        Ajout et affichage de la liste des moniteurs: 
      
       
      </h1>
      <ol className="breadcrumb">
      <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
       
      </ol>
    </section>

   
    <section className="content">
        <div className="row">
            <div className="col-md-4">
            <div className="box box-warning">
            <div className="box-header">
              <h3 className="box-title">Ajout des moniteurs </h3>
            </div>
            <form role="form" onSubmit={this.enregistrer}>
                  <div className="box-body">
                    <p className="text-muted">Pour ajouter un nouveau administrateur, veuillez remplir les champs suivants:</p>
                    <div className="form-group">
                      <label>Nom de l'admin</label>
                      <input type="text" className="form-control" id="nom2" placeholder="Nom..." value={this.state.nom} onChange={this.onChangenom} required/>
                    </div>
                    <div className="form-group">
                      <label>Prénom(s)</label>
                      <input type="text" className="form-control" id="prenom2" placeholder="Prénom(s)..." value={this.state.prenom} onChange={this.onChangeprenom} required/>
                    </div>
                    <div className="form-group">
                      <label for="exampleInputEmail1">Email &nbsp;<span style={{color:'#cf4040'}}>{this.state.errorchamp}</span></label>
                      <input type="email" className="form-control" id="exampleInputEmail2" placeholder="Email..." value={this.state.email} onChange={this.onChangeemail} required/>
                    </div>
                  <div className="form-group">
                      <label for="exampleInputEmail1">Numéro de téléphone</label>
                      <input type="text" className="form-control" id="numero1" placeholder="+261 xx yyyy zz..." value={this.state.telephone} onChange={this.onChangetelephone}/>
                    </div>
                    <div class="form-group">
                   
                      <label for="exampleInputPassword1">Mot de passe &nbsp;  <span style={{color:'#cf4040',"fontFamily":"Arial"}}>{this.state.errorweakpassword}</span></label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Mot de passe..." value={this.state.password} onChange={this.onChangepassword}/>
                    </div>
                    <div class="form-group">
                      
                      
                      <label for="exampleInputPassword1">Confirmer mot de passe &nbsp;<span style={{color:'#cf4040'}}>{this.state.errorpassword}</span></label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirmer le mot de passe..." value={this.state.confirmpassword} onChange={this.onChangeconfirmpassword}/>
                    </div>
                    <div className="form-group">
                      <label for="exampleInputFile">Photo d'admin membre</label>
                      <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>
                    </div>
                    
                  </div>

                  <div class="box-footer">
                    
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
            </form>
            </div>
            </div>
       
     
        <div className="col-md-8">
          <div className="box box-primary">
            <div className="box-header">
              <h3 className="box-title">Liste des administrateurs membres </h3>
             
            </div>
            
            <div className="box-body">
            <p className="text-muted">Voici la liste des moniteurs ou administrateurs enregistrée dans ce système:</p>
             <table className="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="8%"></th>
                    <th>Nom et prénom(s) des membres</th>
                    <th>Date d'entrée</th>
                    <th>Email</th>
                  
                  </tr>
                </thead>
                <tbody>
                { this.state.admnins.map(adm=>
                  <tr>
                    <td>
                       <Avatar style={{"width":"40px","height":"40px"}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${adm.photo}`} />
                      </td>
                    <td data-label="Nom et prénom(s):">{adm.nom} &nbsp;{adm.prenom} </td>
                    <td data-label="Date d'entrée:">{adm.created}</td>
                    <td data-label="Email:">{adm.email}</td>
                    
                   
                  </tr>
                
                )}
                </tbody>
                
              </table>
            </div>
          </div>
        </div>


      </div>
    </section>
  </div>
   
  );
}
}
