import React,{Component} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Pagination from '@material-ui/lab/Pagination';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import Divider from '@material-ui/core/Divider';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Barcode from 'react-barcode';
import Avatar from '@material-ui/core/Avatar';
import ReactPaginate from 'react-paginate';
import  './style.css';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import ChatIcon from '@material-ui/icons/Chat';
import ViewDayIcon from '@material-ui/icons/ViewDay';


export default class Apa extends Component{
  constructor(props) {
    super(props)
    this.delete=this.delete.bind(this); 
    this.handlePageClick = this.handlePageClick.bind(this);
    this.searchHandler=this.searchHandler.bind(this); 
		this.state={
      ouvrages:[],
      message:'',
      offset: 0,
		  perPage: 10,
		  currentPage: 0,
		 
    };

    }
    handlePageClick = (e) => {
      const selectedPage = e.selected;
      const offset = selectedPage * this.state.perPage;

      this.setState({
          currentPage: selectedPage,
          offset: offset
      }, () => {
          this.componentDidMount()
      });

  };
    searchHandler(e){
     
      console.log("search="+e.target.value)
     

        var test=' résultat(s) trouvé(s)'
      Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/search.php/',{wordToSearch:e.target.value})
      .then(response=>{
        if(response.data.message!='Could not find data.'){
          const data = response.data;
          const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
          const postdata = slice.map(ouvrage=>
          <div style={{paddingLeft:'50px'}} >
              <span style={{fontFamily:'Times New Roman',fontSize:'14px'}}>{ouvrage.nomAuteur}&nbsp;({ouvrage.anneeEdition}). <i>{ouvrage.titre}</i>. {ouvrage.lieuEdition}: {ouvrage.editeur}, {ouvrage.nombrePage} p</span>
              <br/><br/>                 
          </div>)
      
          this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage),ouvrages:response.data,message:response.data.length+test})
        }
        else{
          this.setState({message:'Aucun'+test,ouvrages:[]})
        }
       
        
      
        //this.setState({posts:response.data})
      })
    
      }
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
      Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/deleteOne.php',{idOuvrage:id})
      .then(res => console.log(res.data));
     window.location="/dashboard"
       
     } 
     else{}
    } 
    componentDidMount(){
      Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/ficheOuvrageApa.php')
      .then(response=>{
      const data = response.data;
      const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage)	
      const postdata = slice.map(ouvrage=>
      <div style={{paddingLeft:'50px'}} >
          <span style={{fontFamily:'Times New Roman',fontSize:'14px'}}>{ouvrage.nomAuteur}&nbsp;({ouvrage.anneeEdition}). <i>{ouvrage.titre}</i>. {ouvrage.lieuEdition}: {ouvrage.editeur}, {ouvrage.nombrePage} p</span>
          <br/><br/>                       
      </div>)
     this.setState({postdata,pageCount: Math.ceil(data.length / this.state.perPage)})
        
      })
      .catch(error=>{
        console.log(error)
      })
     }
  render(){
    const useStyles = makeStyles((theme) => ({
      root: {
        display: 'flex',
        flexWrap: 'wrap',
      },
      textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: '25ch',
      },
    }));
  return (
    <div className="content-wrapper">
    
  

    <section className="content">
            <div className="row" >
            <div className="col-md-1">
               
               </div>
                <div className="col-md-5">
               
                  <div className="input-group">
                    <input onChange={this.searchHandler} id="search" type="text" className="form-control" placeholder="Effectuer une recherche..." style={{"borderRadius":"10%","backgroundColor":"#ffffff","border":"1px solid #827f7f"}} />
                    <span className="input-group-btn">
                      <button id='search-btn' className="btn btn-flat" style={{"border":"1px solid #827f7f"}}><i className="fa fa-search"></i></button>
                    </span>
                  </div>
                
                </div>
               
                <div className="col-md-2">
                
                
                </div>
                <hr/>
            </div>
            <div className="row">
               <div className="col-md-5">
                 
               </div>
               <div className="col-md-5">
               
               </div>
            </div>
                
           
            
              <div className="row" >
                <div className="col-md-1"></div>
                <div className="col-md-10" >
               <div className="box">
                 <br/>
                <p style={{paddingLeft:'50px',fontSize:'18px',fontWeight:'bold',fontFamily:'Arial'}}>Bibliographies</p>
              
                <span style={{paddingLeft:'50px',color:'#ba2f2f',fontFamily:'Arial',fontSize:13,fontWeight:'bold',textAlign:'center'}}>{this.state.message} </span>
                 
                  {this.state.postdata}  
                    <br/>
            
              <div style={{marginLeft:'40%'}}>
              <ReactPaginate 
                    previousLabel={<KeyboardArrowLeftIcon fontSize="medium"/>}
                    nextLabel={<KeyboardArrowRightIcon fontSize="medium" />}
                    breakLabel={"..."}
                    breakClassName={"break-me"}
                    pageCount={this.state.pageCount}
                    marginPagesDisplayed={2}
                    pageRangeDisplayed={5}
                    onPageChange={this.handlePageClick}
                    containerClassName={"pagination"}
                    subContainerClassName={"pages pagination"}
                    activeClassName={"active"}/>
             </div>
             <br/>
             </div>
             </div>
             </div>
             
      
        
   <br/>
    </section>
    <br/>
  </div>
   
  );
}
}

