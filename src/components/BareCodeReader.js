import React,{Component} from 'react';
import BarcodeScannerComponent from "react-webcam-barcode-scanner";
import Button from '@material-ui/core/Button';
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import StopIcon from '@material-ui/icons/Stop';
import Avatar from '@material-ui/core/Avatar';
import Axios from 'axios';
import IconButton from '@material-ui/core/IconButton';
import Barcode from 'react-barcode';
import EditIcon from '@material-ui/icons/Edit';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import DeleteIcon from '@material-ui/icons/Delete';
import { Link } from 'react-router-dom';
import Moment from 'moment';
import 'moment/locale/fr';
import AddIcon from '@material-ui/icons/Add';

export default class BareCodeReader extends Component{
 
    constructor(props) {
        super(props)
        this.scannCodeBarre=this.scannCodeBarre.bind(this)  
        this.demarrer=this.demarrer.bind(this)  
        this.onChangecodeisbn=this.onChangecodeisbn.bind(this)  
        this.resetISBN=this.resetISBN.bind(this)  
        this.stop=this.stop.bind(this) 
        this.reset=this.reset.bind(this) 
        this.deleteMembre=this.deleteMembre.bind(this); 
        this.deleteOuvrage=this.deleteOuvrage.bind(this); 
        this.modifierMembre=this.modifierMembre.bind(this);
        this.modifierOuvrage=this.modifierOuvrage.bind(this);
        this.onSubmit=this.onSubmit.bind(this)
        this.onChangeamodifierNom=this.onChangeamodifierNom.bind(this)
        this.handleClose=this.handleClose.bind(this);
        this.onChangeamodifierPrenom=this.onChangeamodifierPrenom.bind(this)
        this.onChangeamodifierAdresse=this.onChangeamodifierAdresse.bind(this)
        this.onChangeamodifierfiliere=this.onChangeamodifierfiliere.bind(this)
        this.onChangeamodifiercin=this.onChangeamodifiercin.bind(this)
        this.onChangeamodifierdelivreA=this.onChangeamodifierdelivreA.bind(this)
        this.onChangeamodifiertelephone=this.onChangeamodifiertelephone.bind(this)
        this.onChangeamodifierdomaine=this.onChangeamodifierdomaine.bind(this)
        this.onChangeamodifieremail=this.onChangeamodifieremail.bind(this)
        this.onChangeamodifierdateNaissance=this.onChangeamodifierdateNaissance.bind(this)
        this.onChangeFile1 = this.onChangeFile1.bind(this)
        this.fetchData = this.fetchData.bind(this);   
         this.state={
            openCamera:false,
            data:'  Veuillez cliquer sur l\'icône Caméra pour commencer le scan',
            ouvrages:[],
            titre:'',
            editeur:'',
            open:false,
            nombrePage:1,
            prix:1,
            nomGenre:'',
            langue:'',
            anneeEdition:'',
            quantite:1,
            origine:'',
            lieuEdition:'',
            descriptionGenre:'',
            description:'',
            nomAuteur:'',
            dateEntree:'',
            codeisbn:'',
           photo:'',
           etatActuel:'',
           created:'',
           estOuvrage:'',
            //membre
            nom:'',
            prenom:'',
            adresse:'',
            filiere:'',
            cin:'',
            delivreA:'',
            telephone:'',
            domaine:'',
            email:'',
            datenaissance:'',
            photoM:'',
            message:'',
            estResult:'',
            idMembre:'',
            idOuvrage:'',
            amodifier:[],
            amodifiernom:'',
            amodifierprenom:'',
            amodifieradresse:'',
            amodifierfiliere:'',
            amodifiercin:'',
            amodifierdelivreA:'',
            amodifiertelephone:'',
            amodifierdomaine:'',
            amodifieremail:'',
            amodifierdateNaissance:'',
            amodifierphoto:'',
            getidMembre:'',
            file:null,
           filename:null,

        };
    }
    scannCodeBarre(result){
      if (result!=''){
                                                       
        var pieces = result.split("_");
        console.log('tab0='+pieces[0]+' tab1='+pieces[1])
        this.setState({data:result,estResult:pieces[0],message:''})
        if(pieces[0]=='1OUVR000'){
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+pieces[1])
            .then(response=>{
                if(response.data.message!='Could not find data.'){
                console.log(response.data)
                
                this.setState({ouvrages:response.data,
                    idOuvrage:response.data.idOuvrage,
                    nomGenre:response.data.nomGenre,
                    descriptionGenre:response.data.descriptionGenre,
                    nomAuteur:response.data.nomAuteur,
                    titre:response.data.titre,
                    langue:response.data.langue,
                    editeur:response.data.editeur,
                    nombrePage:response.data.nombrePage,
                    prix:response.data.prix,
                    anneeEdition:response.data.anneeEdition,
                    dateEntree:response.data.dateEntree,
                    quantite:response.data.quantite,
                    quantite:response.data.quantite,
                    origine:response.data.origine,
                    lieuEdition:response.data.lieuEdition,
                    description:response.data.description,
                    photo:response.data.photo,
                    created:response.data.created,
                    etatActuel:response.data.etatActuel,})
                }
                else{
                this.setState({message:'Aucun',ouvrages:[]})
                }
            
            })
            .catch(error=>{
                this.setState({message:'Aucun',ouvrages:[]})
                console.log(error)
              }) 
        }
        else if(pieces[0]=='1mb'){
            Axios.post('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+pieces[1])
            .then(response=>{
                if(response.data.message!='Could not find data.'){
                console.log(response.data)
                
                this.setState({
                    idMembre:response.data.idMembre,
                    nom:response.data.nom,
                    prenom:response.data.prenom,
                    adresse:response.data.adresse,
                    filiere:response.data.filiere,
                    cin:response.data.cin,
                    delivreA:response.data.delivreA,
                    telephone:response.data.telephone,
                    domaine:response.data.domaine,
                    email:response.data.email,
                    datenaissance:response.data.dateNaissance,
                    photoM:response.data.photo,
                    message:''
                })
                }
                else{
               //this.setState({message:'Aucun',ouvrages:[]})
                }
            })
            .catch(error=>{
                this.setState({message:'Aucun',ouvrages:[]})
                console.log(error)
              }) 
        }
        else{
          this.fetchData(result)
            this.setState({estResult:'ISBN'})
            console.log('Message='+this.state.message)
        }
        
        
    }
    else {
     
        //this.setState({message:'Aucun',ouvrages:[]})
    }
    }
    fetchData(isbn) {
     
      fetch(`https://www.googleapis.com/books/v1/volumes?q=isbn:${encodeURIComponent(isbn)}`)
          .then(response => response.json() )
          .then(responseJson => {
             console.log('titre='+responseJson.items[0].volumeInfo.title)
             if(responseJson.items[0].volumeInfo.description!=null){
                this.setState({ description:responseJson.items[0].volumeInfo.description});
              }
            
              if(responseJson.items[0].volumeInfo.pageCount!=null){
                this.setState({ nombrePage:responseJson.items[0].volumeInfo.pageCount});
              }
              if(responseJson.items[0].volumeInfo.title!=null){
                this.setState({ titre:responseJson.items[0].volumeInfo.title});
              }
              if(responseJson.items[0].volumeInfo.publisher!=null){
                this.setState({ editeur:responseJson.items[0].volumeInfo.publisher});
              }
              if(responseJson.items[0].volumeInfo.publishedDate!=null){
                this.setState({ anneeEdition:Moment(responseJson.items[0].volumeInfo.publishedDate).format("YYYY")});
              }
              if(responseJson.items[0].volumeInfo.language!=null){
                this.setState({ langue:responseJson.items[0].volumeInfo.language});
              }
              if(responseJson.items[0].volumeInfo.authors[0]!=null){
                this.setState({ nomAuteur:responseJson.items[0].volumeInfo.authors[0]});
              }
              if(responseJson.items[0].volumeInfo.categories[0]!=null){
                this.setState({ nomGenre:responseJson.items[0].volumeInfo.categories[0]});
              }
              if(responseJson.items[0].volumeInfo.imageLinks.thumbnail!=null){
                this.setState({ photo:responseJson.items[0].volumeInfo.imageLinks.thumbnail});
              }
       })
      .catch(error => {
        console.error(error);
      });
    }
    resetISBN(){
      this.setState({
        codeisbn:'',
      })
    }
    reset(){
      this.setState({
        codeisbn:'',
        openCamera:false,
        data:'  Veuillez cliquer sur l\'icône Caméra pour commencer le scan',
        ouvrages:[],
        titre:'',
        editeur:'',
        open:false,
        nombrePage:1,
        prix:1,
        nomGenre:'',
        langue:'',
        anneeEdition:'',
        quantite:1,
        origine:'',
        lieuEdition:'',
        descriptionGenre:'',
        description:'',
        nomAuteur:'',
        dateEntree:'',
       photo:'',
       etatActuel:'',
       created:'',
       estOuvrage:'',
        //membre
        nom:'',
        prenom:'',
        adresse:'',
        filiere:'',
        cin:'',
        delivreA:'',
        telephone:'',
        domaine:'',
        email:'',
        datenaissance:'',
        photoM:'',
        message:'',
        estResult:'',
        idMembre:'',
        idOuvrage:'',
        amodifier:[],
        amodifiernom:'',
        amodifierprenom:'',
        amodifieradresse:'',
        amodifierfiliere:'',
        amodifiercin:'',
        amodifierdelivreA:'',
        amodifiertelephone:'',
        amodifierdomaine:'',
        amodifieremail:'',
        amodifierdateNaissance:'',
        amodifierphoto:'',
        getidMembre:'',
        file:null,
       filename:null,
      })
    }
    deleteOuvrage(id){
        if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/deleteOne.php',{idOuvrage:id})
            .then(res => console.log(res.data));
           window.location="/dashboard"
             
           } 
           else{}
    }
    deleteMembre(id){
        if(window.confirm("Voulez-vous vraiment le supprimer définitivement?")){
        Axios.post('https://phpapiserver.herokuapp.com/api/membre/deleteOne.php',{idMembre:id})
        .then(res => console.log(res.data));
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/read.php')
        .then(response=>{
          this.setState({membres:response.data})
         });
        } 
       else{}
    }
    modifierOuvrage(id){
        console.log("id="+id)
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
    async uploadFile(file){
     

        const formData = new FormData();
        
        formData.append('avatar',file)
        
        return await Axios.post(this.UPLOAD_ENDPOINT, formData,{
            headers: {
                'content-type': 'multipart/form-data'
            }
        });
      }
      onSubmit(e){
        e.preventDefault();
       
        let res =  this.uploadFile(this.state.file1);
        if(this.state.filename1==null){
        const amodifierMembre={
          idMembre:this.state.getidMembre,
          nom:this.state.amodifiernom,
          prenom:this.state.amodifierprenom,
          adresse:this.state.amodifieradresse,
          filiere:this.state.amodifierfiliere,
          cin:this.state.amodifiercin,
          delivreA:this.state.amodifierdelivreA,
          telephone:this.state.amodifiertelephone,
          domaine:this.state.amodifierdomaine,
          email:this.state.amodifieremail,
          dateNaissance:this.state.amodifierdateNaissance,
          photo:this.state.amodifierphoto
        }
       console.log(amodifierMembre)
        Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
        .then(response=>{
           if(response){
            this.setState({open:false})
            Axios.get('https://phpapiserver.herokuapp.com/api/membre/read.php')
            .then(response=>{this.setState({membres:response.data})})
            }
            else{
                this.setState({open:true}) 
            }
           })
          }
          else{
            const amodifierMembre={
              idMembre:this.state.getidMembre,
              nom:this.state.amodifiernom,
              prenom:this.state.amodifierprenom,
              adresse:this.state.amodifieradresse,
              filiere:this.state.amodifierfiliere,
              cin:this.state.amodifiercin,
              delivreA:this.state.amodifierdelivreA,
              telephone:this.state.amodifiertelephone,
              domaine:this.state.amodifierdomaine,
              email:this.state.amodifieremail,
              dateNaissance:this.state.amodifierDateN,
              photo:this.state.filename1
            }
           console.log(amodifierMembre)
            Axios.post('https://phpapiserver.herokuapp.com/api/membre/update.php',amodifierMembre)
            .then(response=>{
               if(response){
                this.setState({open:false})
                Axios.get('https://phpapiserver.herokuapp.com/api/membre/read.php')
                .then(response=>{this.setState({membres:response.data})})
                }
                else{
                    this.setState({open:true}) 
                }
               })
          }
        } 
        onChangecodeisbn(e){
          if(e.target.value!=''){
            this.scannCodeBarre(e.target.value)
          }
         
          this.setState({
            codeisbn:e.target.value
          });
        }
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    handleClose(){
        this.setState({open:false})
        };
    onChangeFile1(e) {
        this.setState({file1:e.target.files[0]})
        this.setState({filename1:e.target.files[0].name})
      }
    onChangeamodifierNom(e){
        this.setState({
          amodifiernom:e.target.value
        });
      }
      onChangeamodifierPrenom(e){
        this.setState({
          amodifierprenom:e.target.value
        });
      }
      onChangeamodifierAdresse(e){
        this.setState({
          amodifieradresse:e.target.value
        });
      }
      onChangeamodifierfiliere(e){
        this.setState({
          amodifierfiliere:e.target.value
        });
      }
      onChangeamodifiercin(e){
        this.setState({
          amodifiercin:e.target.value
        });
      }
      onChangeamodifierdelivreA(e){
        this.setState({
          amodifierdelivreA:e.target.value
        });
      }
      onChangeamodifiertelephone(e){
        this.setState({
          amodifiertelephone:e.target.value
        });
      }
      onChangeamodifierdomaine(e){
        this.setState({
          amodifierdomaine:e.target.value
        });
      }
      onChangeamodifieremail(e){
        this.setState({
          amodifieremail:e.target.value
        });
      }
      onChangeamodifierdateNaissance(e){
        this.setState({
          amodifierdateNaissance:e.target.value
        });
      }
    demarrer(){
        this.setState({openCamera:true})
    }
    stop(){
         this.setState({openCamera:false})
    }
   
    modifierMembre(id){
        this.setState({open:true})
        console.log(id)
        Axios.get('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+id)
        .then(response=>{
        
          this.setState({
            getidMembre:response.data.idMembre,
            amodifiernom:response.data.nom,
            amodifierprenom:response.data.prenom,
            amodifieradresse:response.data.adresse,
            amodifierfiliere:response.data.filiere,
            amodifiercin:response.data.cin,
            amodifierdelivreA:response.data.delivreA,
            amodifiertelephone:response.data.telephone,
            amodifierdomaine:response.data.domaine,
            amodifieremail:response.data.email,
            amodifierdateNaissance:response.data.dateNaissance,
            amodifierphoto:response.data.photo,
            
          })
        })
        .catch(error=>{
          console.log(error)
        }) 
        console.log(this.state.amodifiernom)
      };
render(){
 let display_result;
 if((this.state.estResult=='1OUVR000')&& (this.state.openCamera==true)){
     display_result=<div style={{marginLeft:'30px'}}>
                        <h4 style={{"textAlign":"center"}}>{this.state.titre}</h4><hr/>
                                 <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                                    <table>
                                       <tr>
                                          <td>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Auteur:</label> {this.state.nomAuteur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Titre:</label> {this.state.titre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Editeur:</label> {this.state.editeur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prix:</label> {this.state.prix}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Genre:</label> {this.state.nomGenre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Langue:</label> {this.state.langue}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Année d'édition:</label> {this.state.anneeEdition}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date d'entrée:</label> {this.state.dateEntree}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Quantité en stock:</label> {this.state.quantite}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Origine:</label> {this.state.origine}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Lieu d'édition:</label> {this.state.lieuEdition}<br/>
                                   
                                          </td>
                                          <td>
                                              <Avatar variant="rounded" src={decodeURIComponent(this.state.photo.replace(/&amp;/g, "&"))} style={{width:"200px",height:'300px',marginLeft:'20px'}}/>
                                          </td>
                                       </tr>
                                    </table>
                                    <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Description:</label> <div style={{width:'80%',marginLeft:'20px'}}>&nbsp;&nbsp;{this.state.description}</div><br/>
                                    
                                </div>
                                 <br/>
                                <Link style={{"color":"#383737"}} to={`/detailOuvrage/${this.state.idOuvrage}`}>
                                    <IconButton aria-label="delete" style={{backgroundColor:'#779136',marginLeft:'20px'}}>
                                        <EditIcon fontSize="large" style={{color:'white'}}  />
                                    </IconButton>
                                </Link>&nbsp;&nbsp;
                                <IconButton title="Supprimer" aria-label="Supprimer" style={{backgroundColor:'#db4646'}}>
                                    <DeleteIcon style={{color:'white'}} fontSize="large" onClick={() => this.deleteOuvrage(this.state.idOuvrage)} />
                                </IconButton>
                        <br/>
                </div>
 }
 else  if((this.state.estResult=='1OUVR000')&& (this.state.openCamera==false)){
  display_result=<div style={{marginLeft:'30px'}}>
                     <h4 style={{"textAlign":"center"}}>{this.state.titre}</h4><hr/>
                              <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                                 <table>
                                    <tr>
                                       <td>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Auteur:</label> {this.state.nomAuteur}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Titre:</label> {this.state.titre}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Editeur:</label> {this.state.editeur}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prix:</label> {this.state.prix}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Genre:</label> {this.state.nomGenre}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Langue:</label> {this.state.langue}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Année d'édition:</label> {this.state.anneeEdition}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date d'entrée:</label> {this.state.dateEntree}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Quantité en stock:</label> {this.state.quantite}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Origine:</label> {this.state.origine}<br/>
                                           <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Lieu d'édition:</label> {this.state.lieuEdition}<br/>
                                          
                                
                                       </td>
                                       <td>
                                           <Avatar variant="rounded" src={this.state.photo} style={{width:"200px",height:'300px',marginLeft:'20px'}}/>
                                       </td>
                                    </tr>
                                 </table>
                                 <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Description:</label> <div style={{width:'80%',marginLeft:'20px'}}>&nbsp;&nbsp;{this.state.description}</div><br/>
                                 
                             </div>
                              <br/>
                             <Link style={{"color":"#383737"}} to={`/detailOuvrage/${this.state.idOuvrage}`}>
                                 <IconButton aria-label="delete" style={{backgroundColor:'#779136',marginLeft:'20px'}}>
                                     <EditIcon fontSize="large" style={{color:'white'}}  />
                                 </IconButton>
                             </Link>&nbsp;&nbsp;
                             <IconButton title="Supprimer" aria-label="Supprimer" style={{backgroundColor:'#db4646'}}>
                                 <DeleteIcon style={{color:'white'}} fontSize="large" onClick={() => this.deleteOuvrage(this.state.idOuvrage)} />
                             </IconButton>
                     <br/>
             </div>
}
 else if((this.state.estResult=='1mb') && (this.state.openCamera==true)){
    display_result=<div style={{marginLeft:'30px'}}>
                       <h4 style={{"textAlign":"center"}}>{this.state.nom} &nbsp;{this.state.prenom}</h4><hr/>
                       <Avatar src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"100px",height:'100px',marginLeft:'50px'}}/>
                        <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nom:</label> {this.state.nom}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prénom(s):</label> {this.state.prenom}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Adresse:</label> {this.state.adresse}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Filière:</label> {this.state.filiere}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;CIN:</label> {this.state.cin}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Téléphone:</label> {this.state.telephone}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Domaine:</label> {this.state.domaine}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Email:</label> {this.state.email}<br/>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date de naissance:</label> {this.state.datenaissance}<br/>
                            <IconButton style={{backgroundColor:'#779136',marginLeft:'20px'}} title="Modifier" aria-label="Modiffier">
                                <EditIcon  style={{color:'white'}} fontSize="large" onClick={() => this.modifierMembre(this.state.idMembre)} />
                            </IconButton>&nbsp;&nbsp;
                            <IconButton title="Supprimer" aria-label="Supprimer" style={{backgroundColor:'#db4646'}}>
                                <DeleteIcon style={{color:'white'}} fontSize="large" onClick={() => this.deleteMembre(this.state.idMembre)} />
                            </IconButton>
                        </div>
                        <br/>
               </div>
}
else if((this.state.estResult=='1mb') && (this.state.openCamera==false)){
  display_result=<div style={{marginLeft:'30px'}}>
                     <h4 style={{"textAlign":"center"}}>{this.state.nom} &nbsp;{this.state.prenom}</h4><hr/>
                     <Avatar src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.photoM}`} style={{width:"100px",height:'100px',marginLeft:'50px'}}/>
                      <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nom:</label> {this.state.nom}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prénom(s):</label> {this.state.prenom}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Adresse:</label> {this.state.adresse}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Filière:</label> {this.state.filiere}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;CIN:</label> {this.state.cin}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Téléphone:</label> {this.state.telephone}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Domaine:</label> {this.state.domaine}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Email:</label> {this.state.email}<br/>
                          <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date de naissance:</label> {this.state.datenaissance}<br/>
                          <IconButton style={{backgroundColor:'#779136',marginLeft:'20px'}} title="Modifier" aria-label="Modiffier">
                              <EditIcon  style={{color:'white'}} fontSize="large" onClick={() => this.modifierMembre(this.state.idMembre)} />
                          </IconButton>&nbsp;&nbsp;
                          <IconButton title="Supprimer" aria-label="Supprimer" style={{backgroundColor:'#db4646'}}>
                              <DeleteIcon style={{color:'white'}} fontSize="large" onClick={() => this.deleteMembre(this.state.idMembre)} />
                          </IconButton>
                      </div>
                      <br/>
             </div>
}
else if((this.state.data=='  Veuillez cliquer sur l\'icône Caméra pour commencer le scan')&& (this.state.openCamera==true)){
    display_result= <Avatar  variant="rounded" style={{width:400,height:400,backgroundColor:'#dce0de',color:'#302f2f',marginLeft:'50px'}}><h4>Vous verrez ici le résultat.</h4></Avatar>
}
else if((this.state.estResult=='ISBN') &&  (this.state.openCamera==false)){
  display_result=<div style={{marginLeft:'30px'}}>
                             
                              <h4 style={{"textAlign":"center",fontWeight:'bold',fontFamily:'Arial'}}>{this.state.titre}
                                <div class="pull-right">
                                  <Link title="Ajouter" to="/ouvrage" href="#" class="btn btn-success btn-sm">
                                    <i style={{color:'white'}} className="fa fa-plus"></i>
                                  </Link>&nbsp;&nbsp;
                                  <Link  title="Annuler" onClick={this.reset} href="#" class="btn btn-danger btn-sm">
                                    <i style={{color:'white'}} className="fa fa-close"></i>
                                  </Link>
                                </div>
                              </h4>
                              <hr/>
                              <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                              <table>
                                       <tr>
                                          <td>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Auteur:</label> {this.state.nomAuteur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Titre:</label> {this.state.titre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Editeur:</label> {this.state.editeur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Genre:</label> {this.state.nomGenre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Langue:</label> {this.state.langue}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Année d'édition:</label> {this.state.anneeEdition}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nombre de page:</label> {this.state.nombrePage}<br/>
                                           </td>
                                          <td>
                                          <Avatar variant="rounded" src={this.state.photo} style={{width:"200px",height:'300px',marginLeft:'20px'}}/>
                             
                                          </td>
                                       </tr>
                                    </table>
                                    <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Description:</label> <div style={{width:'80%',marginLeft:'20px'}}>&nbsp;&nbsp;{this.state.description}</div><br/>
                                        
                             </div>
                            <br/>
                            
                     <br/>
             </div>
}
else if((this.state.estResult=='ISBN')&& (this.state.openCamera==true)){
  display_result=<div style={{marginLeft:'30px'}}>
                             
                              <h4 style={{"textAlign":"center",fontWeight:'bold',fontFamily:'Arial'}}>{this.state.titre}
                                <div class="pull-right">
                                  <Link title="Ajouter" to="/ouvrage" href="#" class="btn btn-success btn-sm">
                                    <i style={{color:'white'}} className="fa fa-plus"></i>
                                  </Link>&nbsp;&nbsp;
                                  <Link  title="Annuler" onClick={this.reset} href="#" class="btn btn-danger btn-sm">
                                    <i style={{color:'white'}} className="fa fa-close"></i>
                                  </Link>
                                </div>
                              </h4>
                              <hr/>
                              <div style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                              <table>
                                       <tr>
                                          <td>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Auteur:</label> {this.state.nomAuteur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Titre:</label> {this.state.titre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Editeur:</label> {this.state.editeur}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Genre:</label> {this.state.nomGenre}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Langue:</label> {this.state.langue}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Année d'édition:</label> {this.state.anneeEdition}<br/>
                                              <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nombre de page:</label> {this.state.nombrePage}<br/>
                                          
                                          </td>
                                          <td>
                                          <Avatar variant="rounded" src={decodeURIComponent(this.state.photo.replace(/&amp;/g, "&"))} style={{width:"200px",height:'300px',marginLeft:'20px'}}/>
                             
                                          </td>
                                       </tr>
                                    </table>
                                    <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Description:</label> <div style={{width:'80%',marginLeft:'20px'}}>&nbsp;&nbsp;{this.state.description}</div><br/>
                                        
                             </div>
                            <br/>
                            
                     <br/>
             </div>
}
else{
    display_result= <Avatar variant="rounded" style={{width:400,height:400,backgroundColor:'#dce0de',color:'#db3725',marginLeft:'50px'}}><h4>Aucune information obtenue</h4></Avatar>
}
 return (
    <div className="content-wrapper">
        <section className="content-header">
            <h1> Scanner du code-Barres</h1>
            <small style={{fontFamily:'Arial',fontSize:'14px'}} className="text-muted">
              Consulter le résumé d'un ouvrage à partir d'un code-barres généré par le système
              ou ISBN
            </small>
            <ol className="breadcrumb">
          <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
         
          </ol>
        </section>
        <section className="content">
            <div className="row">
                 <div className="col-md-5">
                    <div className="box box-warning">
                        <div className="box-header">
                            <h4>Utiliser un lecteur code-barres</h4>
                            <div class="input-group margin">
                              <input
                              type="text"
                              placeholder="Code ISBN" autoFocus={true}  className="form-control" onChange={this.onChangecodeisbn} value={this.state.codeisbn}/>
                                <span className="input-group-btn">
                                  <button  onClick={this.reset} className="btn btn-warning btn-flat" type="button"><i className="fa fa-close"></i></button>
                                </span>
                          </div>
                            
                              <br/><h4 className="box-title">Via Webcam</h4>&nbsp;
                              <button className='btn btn-primary' onClick={this.reset}><i className="fa fa-refresh"></i>&nbsp;Réinitialiser</button>
                            
                        </div>
                       
                        {(()=>{
                                if(this.state.openCamera==true){
                                    return(
                                            <div style={{marginLeft:'50px'}}>
                                                <Tooltip title="Arrêter le scanner" aria-label="Arrêter le scanner">
                                                    <Fab style={{backgroundColor:'#ed5d4a',color:'white'}} onClick={this.stop}>
                                                        <StopIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:400,height:400}}>
                                                 <BarcodeScannerComponent
                                                    width={500}
                                                    format="EAN-13"
                                                    height={500}
                                                    
                                                    onUpdate={(err, result) => {
                                                    if (result){
                                                       
                                                        var pieces = result.text.split("_");
                                                        console.log('tab0='+pieces[0]+' tab1='+pieces[1])
                                                        this.setState({data:result.text,estResult:pieces[0],message:''})
                                                        if(pieces[0]=='1OUVR000'){
                                                            Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+pieces[1])
                                                            .then(response=>{
                                                                if(response.data.message!='Could not find data.'){
                                                                console.log(response.data)
                                                                
                                                                this.setState({ouvrages:response.data,
                                                                    idOuvrage:response.data.idOuvrage,
                                                                    nomGenre:response.data.nomGenre,
                                                                    descriptionGenre:response.data.descriptionGenre,
                                                                    nomAuteur:response.data.nomAuteur,
                                                                    titre:response.data.titre,
                                                                    langue:response.data.langue,
                                                                    editeur:response.data.editeur,
                                                                    nombrePage:response.data.nombrePage,
                                                                    prix:response.data.prix,
                                                                    anneeEdition:response.data.anneeEdition,
                                                                    dateEntree:response.data.dateEntree,
                                                                    quantite:response.data.quantite,
                                                                    quantite:response.data.quantite,
                                                                    origine:response.data.origine,
                                                                    lieuEdition:response.data.lieuEdition,
                                                                    description:response.data.description,
                                                                    photo:response.data.photo,
                                                                    created:response.data.created,
                                                                    etatActuel:response.data.etatActuel,})
                                                                }
                                                                else{
                                                                this.setState({message:'Aucun'+test,ouvrages:[]})
                                                                }
                                                            
                                                            })
                                                            .catch(error=>{
                                                                this.setState({message:'Aucun'+test,ouvrages:[]})
                                                                console.log(error)
                                                              }) 
                                                        }
                                                        else if(pieces[0]=='1mb'){
                                                            Axios.post('https://phpapiserver.herokuapp.com/api/membre/single_read.php/?idMembre='+pieces[1])
                                                            .then(response=>{
                                                                if(response.data.message!='Could not find data.'){
                                                                console.log(response.data)
                                                                
                                                                this.setState({
                                                                    idMembre:response.data.idMembre,
                                                                    nom:response.data.nom,
                                                                    prenom:response.data.prenom,
                                                                    adresse:response.data.adresse,
                                                                    filiere:response.data.filiere,
                                                                    cin:response.data.cin,
                                                                    delivreA:response.data.delivreA,
                                                                    telephone:response.data.telephone,
                                                                    domaine:response.data.domaine,
                                                                    email:response.data.email,
                                                                    datenaissance:response.data.dateNaissance,
                                                                    photoM:response.data.photo,
                                                                    message:''
                                                                })
                                                                }
                                                                else{
                                                               //this.setState({message:'Aucun',ouvrages:[]})
                                                                }
                                                            })
                                                            .catch(error=>{
                                                                this.setState({message:'Aucun',ouvrages:[]})
                                                                console.log(error)
                                                              }) 
                                                        }
                                                        else{
                                                          this.fetchData(result.text)
                                                            this.setState({estResult:'ISBN'})
                                                            console.log('Message='+this.state.message)
                                                        }
                                                        
                                                        
                                                    }
                                                    else {
                                                     
                                                        //this.setState({message:'Aucun',ouvrages:[]})
                                                    }
                                                    }}
                                                />
                                               </Avatar>
                                            </div>
                                        )
                                }  
                                if(this.state.openCamera==false){
                                    return(
                                            <div style={{marginLeft:'50px'}}>
                                                <Tooltip title="Démarrer le scanner" aria-label="Démarrer le scanner">
                                                    <Fab style={{backgroundColor:'#488a3b',color:'white'}} onClick={this.demarrer}>
                                                        <CameraAltIcon fontSize="large"/>
                                                    </Fab>
                                                </Tooltip><br/>
                                                <Avatar variant="rounded" style={{width:400,height:400}}>
                                                    <h4 style={{color:'#343634'}}>Démarrer le scanner et approcher le code-barres</h4>
                                                </Avatar>
                                            </div>
                                        )
                                }
                            }
                        )()}
             
              
                  <br/>
                    </div>
                </div>
                <div className="col-md-7">
                    <div className="box box-success">
                        <div className="box-header">
                            <h3 className="box-title">Résultat du scan </h3>
                        </div>
                        <div className="box-body">
                        <p>&nbsp;&nbsp;Résultat:&nbsp;{this.state.data}</p>
                       <br/>
                        {display_result}
                       <br/>
                       </div>
                    </div>
                </div>
               
            </div>
        </section>
        <Dialog fullWidth={true}
          maxWidth = {'xs'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">
           <table>
              <tr>
                <td><Avatar   alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${this.state.amodifierphoto}`} /></td>
                <td><h4>&nbsp;{this.state.amodifiernom} &nbsp; {this.state.amodifierprenom}</h4></td>  
              </tr>
           </table>
        </DialogTitle>
        <form onSubmit={this.onSubmit}>
           
          <DialogContent dividers>
            <p>Effectuer une modification sur ce membre:</p>
            <label>Nom du membre:</label>
            <TextField
            variant="outlined"
            id="nomG"
            type="text"
            value={this.state.amodifiernom}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierNom}
          />
           <label>Prénom:</label>
           <TextField
            variant="outlined"
            id="nationaliteM"
            type="text"
            value={this.state.amodifierprenom}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierPrenom}
          />
            <label>Adresse:</label>
           <TextField
            variant="outlined"
            id="adresse"
            type="text"
            value={this.state.amodifieradresse}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierAdresse}
          />
           <label>Filiere:</label>
           <TextField
            variant="outlined"
            id="filiere"
            type="text"
            value={this.state.amodifierfiliere}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierfiliere}
          />
           <label>CIN:</label>
           <TextField
            variant="outlined"
            id="Cin"
            type="text"
            value={this.state.amodifiercin}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifiercin}
          />
           <label>Délivré à:</label>
           <TextField
            variant="outlined"
            id="delivreA"
            type="text"
            value={this.state.amodifierdelivreA}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdelivreA}
          />
           <label>Contact:</label>
           <TextField
            variant="outlined"
            id="contact"
            type="text"
            value={this.state.amodifiertelephone}
            InputProps={{ style: {fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifiertelephone}
          />
           <label>Domaine:</label>
           <TextField
            variant="outlined"
            id="domaine"
            type="text"
            value={this.state.amodifierdomaine}
            InputProps={{ style: {fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdomaine}
          />
           <label>Email:</label>
           <TextField
            variant="outlined"
            id="email"
            type="email"
            value={this.state.amodifieremail}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifieremail}
          />
           <label>Date de naissance:</label>
           <TextField
            variant="outlined"
            id="dateNN"
            type="date"
            value={this.state.amodifierdateNaissance}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierdateNaissance}
          />
        
        <div className="form-group">
           <label for="exampleInputFile">Modifier l'image de l'ouvrage</label>
          <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile1 }/>

         </div>
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>
    </div>
  )
}
}
 
