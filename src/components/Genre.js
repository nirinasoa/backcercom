import React,{Component} from 'react';
import Axios from 'axios';
import EditIcon from '@material-ui/icons/Edit';
import RecentActorsIcon from '@material-ui/icons/RecentActors';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import {Link} from 'react-router-dom';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Fab from '@material-ui/core/Fab';

export default class Genre extends Component {
  
  constructor(props) {
    super(props)
    this.modifier=this.modifier.bind(this);
    this.handleClose=this.handleClose.bind(this);
    this.onChangeamodifierNom=this.onChangeamodifierNom.bind(this)
    this.onChangeamodifierDesc=this.onChangeamodifierDesc.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.onSubmitGenre=this.onSubmitGenre.bind(this)
    this.onChangenomGenre=this.onChangenomGenre.bind(this)
    this.onChangedescriptionGenre=this.onChangedescriptionGenre.bind(this)
    this.delete=this.delete.bind(this); 
		this.state={
      genres:[],
      open:false,
      amodifier:[],
      amodifierNom:'',
      amodifierDescription:'',
      getidGenre:'',
      nomGenre:'',
      descriptionGenre:'',
		 
    };
    
    }
    delete(id){
      if(window.confirm("Voulez-vous vraiment le supprimer définitivement")){
      Axios.post('https://phpapiserver.herokuapp.com/api/genre/deleteOne.php',{idGenre:id})
      .then(res =>{ 
      
        this.componentDidMount()
      });
    
       
     } 
     else{
      this.componentDidMount()
     }
      
  }
  onChangenomGenre(e){
    this.setState({
      nomGenre:e.target.value
    });
  }
  onChangedescriptionGenre(e){
    this.setState({
      descriptionGenre:e.target.value
    });
  }
  onSubmitGenre(e){
    e.preventDefault();
    const genre={
      nomGenre:this.state.nomGenre,
      description:this.state.descriptionGenre
     }
    console.log(genre)
    Axios.post('https://phpapiserver.herokuapp.com/api/genre/create.php',genre)
    .then(res=>console.log(res.data));
    alert('Le genre a été crée avec succès!')
    window.location='/genre';
  }

  modifier(idGenre){
    this.setState({open:true})
    Axios.get('https://phpapiserver.herokuapp.com/api/genre/single_read.php/?idGenre='+idGenre)
    .then(response=>{
    
      this.setState({
        getidGenre:response.data.idGenre,
        amodifierNom:response.data.nomGenre,
        amodifierDescription:response.data.description,
      })
    
     
    })
    .catch(error=>{
      console.log(error)
    }) 
    };
  handleClose(){
    this.setState({open:false})
    };
    componentDidMount(){
		Axios.get('https://phpapiserver.herokuapp.com/api/genre/read.php')
    .then(response=>{
      this.setState({genres:response.data})
      
		})
		.catch(error=>{
		  console.log(error)
		})
   }
   onChangeamodifierNom(e){
    this.setState({
      amodifierNom:e.target.value
    });
  }
  onChangeamodifierDesc(e){
    this.setState({
      amodifierDescription:e.target.value
    });
  }
  onSubmit(e){
    e.preventDefault();
    console.log("id="+this.state.getidGenre+"nom"+this.state.amodifierNom+" desc="+this.state.amodifierDescription)
    const amodiferGenre={
      idGenre:this.state.getidGenre,
      nomGenre:this.state.amodifierNom,
      description:this.state.amodifierDescription
      
    }
    console.log(amodiferGenre)
    Axios.post('https://phpapiserver.herokuapp.com/api/genre/update.php',amodiferGenre)
    .then(response=>{
       if(response){
           window.location="/genre"
        }
        else{
            this.setState({open:true}) 
        }
        
        
      })
      .catch()

  }
    render(){
  
  return (
    <div className="content-wrapper">
        <section className="content-header">
          <h1>Genre</h1>
          <h4>Consultez les genres des ouvrages existants ou ajoutez un nouveau</h4>
          <ol className="breadcrumb">
            <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
          </ol>
        </section> 
        <section className="content">
           <div className="row">
              <div className="col-md-8">
                <div className="box box-danger">
                  <div className="box-header">
                    <div className="box-body">
                      
             <table className="table table-bordered table-striped">
             <thead>
               <tr>
                  <th width="20%">Catégorie</th>
                  <th width="40%">Description</th>
                  <th width="9%">Ations</th>
                  <th width="9%"></th>
               </tr>
               </thead>
               <tbody>
                { this.state.genres.map(g=>
                 
                <tr>
                    <td width="50%"  data-label="Catégorie:"> {g.nomGenre}</td>
                    <td data-label="Description:" width="90%"> {g.description}  </td>
                    <td data-label="Actions:" width="9%">
                      <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                        <EditIcon fontSize="large"  style={{"color":"#4a4d49"}} onClick={() => this.modifier(g.idGenre)} /> 
                      </Fab>
                    </td>
                    <td width="9%">
                      <Fab size="small" color="default" aria-label="add" title="Modifier cette ligne">
                        <DeleteIcon fontSize="large"  style={{"color":"#4a4d49"}} onClick={() => this.delete(g.idGenre)} /> 
                      </Fab>
                    </td>
                    
                    
                    
                    </tr>
                )
                }
                 </tbody>
             </table>
            </div>
            </div>
				</div>  
        </div>
        <div className="col-md-4">
            
        <div className="box box-primary">
            <div className="box-header">
              <h3 className="box-title">Ajout d'un genre </h3>
            </div>
            <form role="form" onSubmit={this.onSubmitGenre}>
                  <div className="box-body">
                    <p className="text-muted">Pour ajouter un genre, veuillez remplir les champs suivants:</p>
                    <div className="form-group">
                      <label>Genre de l'ouvrage</label>
                      <input type="text" className="form-control" id="auteur" placeholder="Genre..." value={this.state.nomGenre} onChange={this.onChangenomGenre}/>
                    </div>
                   
                    <div className="form-group">  
                    <label >Description</label>  
                        
                        <textarea className="textarea" placeholder="Mettre la description ici" style={{"width": "100%", "height": "100px", "font-size": "14px", "lineHeight": "18px", "border": "1px solid #dddddd", "padding": "10px"}} value={this.state.descriptionGenre} onChange={this.onChangedescriptionGenre}></textarea>
                  
              
                    </div>
                   
                  </div>
                  <div class="box-footer">
                    
                    <button type="submit" class="btn btn-primary">Enregistrer</button>
                  </div>
            </form>
            <div className="box box-primary">
                 
            </div>
            </div>
        </div>
        <div className="col-md-2"></div>

				</div>

        </section>
        <Dialog fullWidth={true}
      maxWidth = {'sm'} open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title"> <h4>Effectuer une modification:</h4></DialogTitle>
        <form onSubmit={this.onSubmit}>
       
          <DialogContent dividers>
            <label>Catégorie:</label>
            <TextField
            variant="outlined"
            id="nomG"
            type="text"
            value={this.state.amodifierNom}
            InputProps={{ style: { fontSize: 13 }}}
            inputProps={{ style: { fontSize: 13 }}}           
             fullWidth
             onChange={this.onChangeamodifierNom}
          />
           <label>Description:</label>
          <textarea className="textarea"
            style={{"width": "100%", "height": "100px", "font-size": "14px", "lineHeight": "18px", "padding": "10px"}}
          
            value={this.state.amodifierDescription}
            onChange={this.onChangeamodifierDesc}
            ></textarea>
        
      
        </DialogContent>
        <DialogActions>
	
          <Button style={{"backgroundColor":"#ba1818"}} onClick={this.handleClose} color="primary">
		  	<h6 style={{"color":"white","fontFamily":"Arial"}}>Annuler</h6>
          </Button>
          
          <Button style={{"backgroundColor":"#1a6929"}}  type="submit" color="primary">
          <h6 style={{"color":"white","fontFamily":"Arial"}}>Modifier</h6>
          </Button>
        </DialogActions>
        </form>
       
          <br/>
      </Dialog>
</div>
  );
}
}

