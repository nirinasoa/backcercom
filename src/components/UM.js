import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import Avatar from '@material-ui/core/Avatar';
import {Link} from 'react-router-dom';
export default class UM extends Component {
  constructor(props) {
    super(props)
    
    this.onChangeconfirmpassword=this.onChangeconfirmpassword.bind(this)
    this.onChangenom=this.onChangenom.bind(this)
    this.onChangeprenom=this.onChangeprenom.bind(this)
    this.onChangetelephone=this.onChangetelephone.bind(this)
    this.onChangeemail=this.onChangeemail.bind(this)
    this.onChangepassword=this.onChangepassword.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)

    this.enregistrer=this.enregistrer.bind(this)

		this.state={
       nom:'',
       prenom:'',
       telephone:'',
       email:'',
       password:'',
       admnins:[],
       file:null,
       filename:null,
       confirmpassword:'',
       errorpassword:'',
       errorweakpassword:'',
       errorchamp:''
     
      
      };
    
    }
    UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
    onChangenom(e){
      this.setState({
        nom:e.target.value
      });
    }
    onChangeprenom(e){
      this.setState({
        prenom:e.target.value
      });
    }
    onChangeconfirmpassword(e){
      this.setState({
        confirmpassword:e.target.value
      });
      if(this.state.password!== e.target.value){
        this.setState({
          errorpassword:'->Mot de passe différent'
        });
      }
      else{
        this.setState({
          errorpassword:''
        });
      }
    }
    onChangetelephone(e){
      this.setState({
        telephone:e.target.value
      });
    }
    onChangeemail(e){
      this.setState({
        email:e.target.value
      });
    }
    onChangepassword(e){
      console.log("Nombre password"+e.target.value.split('').length)
      if(e.target.value.split('').length<4){
        this.setState({errorweakpassword:'->Mot de passe trop court'})
      }
      else{
        this.setState({errorweakpassword:''})
      }
      this.setState({
        password:e.target.value
      });
    }
    onChangeFile(e) {
      this.setState({file:e.target.files[0]})
      this.setState({filename:e.target.files[0].name})
  }
  async uploadFile(file){
    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      data.append("public_id", originalfilename[0]);
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
  }
    enregistrer(e){
      e.preventDefault();
      //Verif is mail valid
      if(new RegExp(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/g).test(this.state.email)==true){
          if(this.state.errorweakpassword!='->Mot de passe trop court'){
            if(this.state.password!=this.state.confirmpassword){
              this.setState({
                errorchamp:'',
                errorpassword:'->Mot de passe différent'
              });
            }
            else{
              //alert('on peut inscrire')
              //find dans table utilisateur admin where email=this.state.email
              Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/findByEmail.php/?email='+this.state.email)
              .then(response=>{
                //Si oui->Email existant dans la base de données
                if(response.data.email!=null){
                  this.setState({
                  
                    errorchamp:'->Email existant!'
                  });
                }
               
                
              })
               .catch(error=>{
                //Sinon ->Vous pouvez inscrire sans blocage
                console.log('non existant')
                let res =  this.uploadFile(this.state.file);
                  const admin={
                    nom:this.state.nom,
                    prenom:this.state.prenom,
                    email:this.state.email,
                    contact:this.state.telephone,
                    motdepasse:this.state.password,
                    photo:this.state.filename

                  }
                  console.log(admin)
                Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurAdmin/register.php',admin)
                  .then(res=>{
                    alert('Le compte admin a été enregistré avec succès!')
                    Axios.get('https://phpapiserver.herokuapp.com/api/utilisateurMembre/UM.php')
                    .then(response=>{
                      this.setState({admnins:response.data})
                      
                    })
                  });
                 
               }) 

                
            }
          }
          else{
            this.setState({errorweakpassword:'Mot de passe trop court'})
          }
      }
      else{
        this.setState({ errorchamp:'->Email invalide'})
      }
        

   };
  componentDidMount(){
    Axios.post('https://phpapiserver.herokuapp.com/api/utilisateurMembre/UM.php')
    .then(res=> 
        this.setState({admnins:res.data})
    )};
  render(){
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
        Liste des membres ayant son compte
      
       
      </h1>
      <ol className="breadcrumb">
          <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
      </ol>
    </section>

   
    <section className="content">
        <div className="row">
           
       
     
        <div className="col-md-11">
          <div className="box box-primary">
            <div className="box-header">
              <h3 className="box-title">Liste des utilisateurs membres </h3>
             
            </div>
            
            <div className="box-body">
            <p className="text-muted">Voici la liste des utilisateurs membres enregistrée dans ce système:</p>
             <table className="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th width="8%"></th>
                    <th>Nom et prénom(s) des membres</th>
                    <th>Date d'entrée</th>
                    <th>Email</th>
                  
                  </tr>
                </thead>
                <tbody>
                { this.state.admnins.map(adm=>
                  <tr>
                    <td>
                       <Avatar style={{"width":"40px","height":"40px"}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${adm.photo}`} />
                      </td>
                    <td data-label="Nom et prénom(s):"><Link to={`/detailMembre/${adm.idMembre}`}>{adm.nom} &nbsp;{adm.prenom}</Link> </td>
                    <td data-label="Date d'entrée:">{adm.created}</td>
                    <td data-label="Email:">{adm.email}</td>
                    
                   
                  </tr>
                
                )}
                </tbody>
                
              </table>
            </div>
          </div>
        </div>


      </div>
    </section>
  </div>
   
  );
}
}
