import React,{Component} from 'react';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import IconButton from '@material-ui/core/IconButton';
import Axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import Avatar from '@material-ui/core/Avatar';
import Barcode from 'react-barcode';
import html2canvas from 'html2canvas';  
import jsPDF from 'jspdf';  
import GetAppIcon from '@material-ui/icons/GetApp';
import SaveIcon from '@material-ui/icons/Save';
import {Link} from 'react-router-dom';
import Moment from 'moment';
import 'moment/locale/fr';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';


export default class DetailOuvrage extends Component {
  constructor(props) {
    super(props)
    this.exporterPDF=this.exporterPDF.bind(this)
    this.posterCommentaire=this.posterCommentaire.bind(this)
    this.convertToImage=this.convertToImage.bind(this)
    this.convertToImage1=this.convertToImage1.bind(this)
    this.onChangeauteur=this.onChangeauteur.bind(this)
    this.onChangetitre=this.onChangetitre.bind(this)
    this.onChangeediteur=this.onChangeediteur.bind(this)
    this.onChangeediteur=this.onChangeediteur.bind(this)
    this.onChangeprix=this.onChangeprix.bind(this)
    this.onChangenombrepage=this.onChangenombrepage.bind(this)
    this.onChangegenre=this.onChangegenre.bind(this)
    this.onChangelangue=this.onChangelangue.bind(this)
    this.onChangeannee=this.onChangeannee.bind(this)
    this.onChangedateentree=this.onChangedateentree.bind(this)
    this.onChangequantite=this.onChangequantite.bind(this)
    this.onChangeorigine=this.onChangeorigine.bind(this)
    this.onChangelieu=this.onChangelieu.bind(this)
    this.onChangedescription=this.onChangedescription.bind(this)
    this.onSubmit=this.onSubmit.bind(this)
    this.onChangeFile = this.onChangeFile.bind(this)
    this.onChangemessage = this.onChangemessage.bind(this)
    
    this.state={
       idOuvrage:this.props.match.params.id,
       ouvrages:[],
       titre:'',
       editeur:'',
       nombrePage:1,
       prix:1,
       nomGenre:'',
       langue:'',
       anneeEdition:'',
       quantite:1,
       origine:'',
       lieuEdition:'',
       descriptionGenre:'',
       description:'',
       nomAuteur:'',
       dateEntree:'',
      photo:'',
      etatActuel:'',
      created:'',
      genres:[],
      file:null,
      filename:null,
      nombre:'',
      commentaires:[],
      message:''
    };
  }
  posterCommentaire(){
    const commentaire={
        idMembre:sessionStorage.getItem("idMembre"),
        idOuvrage:this.props.match.params.id,
        message:this.state.message
      }
      console.log(commentaire)
    Axios.post('https://phpapiserver.herokuapp.com/api/commentaire/create.php',commentaire)
      .then(res=>{
        console.log(res.data)
        this.setState({
            message:''
        });
       this.componentDidMount();
      });
  }
  exporterPDF(filename){
    const input = document.getElementById('pdf');  
    html2canvas(input)  
      .then((canvas) => {  
        var imgWidth = 90;  
        var pageHeight = 210;  
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
        const imgData = canvas.toDataURL('image/jpeg');  
        const pdf = new jsPDF('p', 'mm', 'a4')  
        var position = 15;  
        var heightLeft = imgHeight; 
        pdf.setProperties({
          DisplayDocTitle:true,
            title: 'Carte de lecteur bibliothèque',
            author: 'RYN',
            creator: 'RYN'
        });
        pdf.setFontSize(13);
        pdf.text(5, 5, 'DocFinder_Carte du membre'); 
        pdf.setLineWidth(1)
        pdf.addImage(imgData, 'JPEG', 15, position, imgWidth, imgHeight);  
        var string = pdf.output('datauristring');
            var embed = "<embed width='100%' height='100%' src='" + string + "'/>"
            var x = window.open();
            x.document.open();
            x.document.write(embed);
            x.document.close();
            pdf.save(filename+'.pdf')
      }); 
  }
  convertToImage1(filename){
  
   /* html2canvas(document.querySelector("#barcode")).then(canvas => {
       var link = document.createElement("a");
      document.body.appendChild(link);
      link.download = "html_image.png";
      link.href = canvas.toDataURL("image/png");
      link.target = '_blank';
      link.click();
  
      
  });*/
  html2canvas(document.querySelector("#test")).then(canvas => {
    
    var link = document.createElement("a");
    link.href = canvas.toDataURL("image/png");
   
   link.download =filename+".png";
  

   document.body.appendChild(link);
   link.click();
   document.body.removeChild(link);
   
});
 
    
  }
  convertToImage(filename){
    document.getElementById("download").addEventListener("click", function() {

      html2canvas(document.getElementById('barcode')).then(function(canvas) {
  
          console.log(canvas);
        
          var link = document.createElement('a');

          if (typeof link.download === 'string') {
      
              link.href = canvas.toDataURL();
              link.download = filename;
      
              //Firefox requires the link to be in the body
              document.body.appendChild(link);
      
              //simulate click
              link.click();
      
              //remove the link when done
              document.body.removeChild(link);
      
          } else {
      
              window.open(canvas.toDataURL());
      
          }
      });
  });
  }
  onChangemessage(e){
    this.setState({
      message:e.target.value
    });
  }
  onChangeauteur(e){
    this.setState({
      nomAuteur:e.target.value
    });
  }
  onChangelangue(e){
    this.setState({
      langue:e.target.value
    });
  }
  onChangetitre(e){
    this.setState({
      titre:e.target.value
    });
  }
  onChangeediteur(e){
    this.setState({
      editeur:e.target.value
    });
  }
  onChangenombrepage(e){
    this.setState({
      nombrePage:parseInt(e.target.value)
    });
   
  }
  onChangeprix(e){
    this.setState({
      prix:e.target.value
    });
  }
  onChangegenre(e){
     this.setState({
      nomGenre:e.target.value
    });
  }
 
  onChangeannee(e){
    this.setState({
      anneeEdition:e.target.value
    });
  }
  onChangedateentree(e){
    this.setState({
      dateEntree:e.target.value
    });
  }
  onChangequantite(e){
    this.setState({
      quantite:e.target.value
    });
  }
  onChangeorigine(e){
    this.setState({
      origine:e.target.value
    });
  }
  onChangelieu(e){
    this.setState({
      lieuEdition:e.target.value
    });
  }
  onChangedescription(e){
    this.setState({
      description:e.target.value
    });
  }

  componentDidMount(){
    Axios.get('https://phpapiserver.herokuapp.com/api/commentaire/nombreCommentaireParOuvrage.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
          if(response.data.nombre!=null){
            this.setState({nombre:response.data.nombre+' commentaire(s)'})
        }
        else{
            this.setState({nombre:'Aucun commentaire',commentaires:[]})
        }
        
      
        })
         
        
        Axios.get('https://phpapiserver.herokuapp.com/api/commentaire/ficheCommentaireOk.php/?idOuvrage='+this.props.match.params.id)
        .then(response=>{
            if(response.data.length>0){
                console.log("ato="+response.data)
                this.setState({commentaires:response.data})
            }
            else{
                this.setState({nombre:'Aucun commentaire',commentaires:[
                   
                  ]})
            }
          
        })
        .catch(error=>{
            console.log(error)
          
          })
    Axios.get('https://phpapiserver.herokuapp.com/api/genre/read.php')
      .then(response=>{
        this.setState({genres:response.data})
        
      })
    Axios.get('https://phpapiserver.herokuapp.com/api/ouvrage/findIdByClmView.php/?idOuvrage='+this.props.match.params.id)
    .then(response=>{
    this.setState({
      nomGenre:response.data.nomGenre,
      descriptionGenre:response.data.descriptionGenre,
      nomAuteur:response.data.nomAuteur,
      titre:response.data.titre,
      langue:response.data.langue,
      editeur:response.data.editeur,
      nombrePage:response.data.nombrePage,
      prix:response.data.prix,
      anneeEdition:response.data.anneeEdition,
      dateEntree:response.data.dateEntree,
      quantite:response.data.quantite,
      quantite:response.data.quantite,
      origine:response.data.origine,
      lieuEdition:response.data.lieuEdition,
      description:response.data.description,
      photo:response.data.photo,
      created:response.data.created,
      etatActuel:response.data.etatActuel,
      idAuteurFromColumn1:'',
      idGenreFromColumn1:'',

    })})
    .catch(error=>{
      console.log(error)
    })
   };
  onSubmit(e){
    e.preventDefault();
    let res =  this.uploadFile(this.state.file);
  
    Axios.get('https://phpapiserver.herokuapp.com/api/auteur/findIdByClm.php/?nomAuteur='+this.state.nomAuteur)
    .then(response=>{this.setState({idAuteurFromColumn1:response.data.idAuteur})
    
    console.log("Nom genre="+this.state.nomGenre)
  
   // alert("idAuteurFromColumn="+window.localStorage.getItem("idAuteurFromColumn"))
    Axios.get('https://phpapiserver.herokuapp.com/api/genre/findIdByClm.php/?nomGenre='+this.state.nomGenre)
    .then(response=>{
      //console.log('id='+response.data.idGenre);
      this.setState({
        idGenreFromColumn1:response.data.idGenre
      })
      if(this.state.filename==null){
        const ouvrage={
          idOuvrage:this.state.idOuvrage,
          idAuteur:this.state.idAuteurFromColumn1,
          titre:this.state.titre,
          editeur:this.state.editeur,
          nombrePage:this.state.nombrePage,
          prix:this.state.prix,
          idGenre:response.data.idGenre,
          langue:this.state.langue,
          anneeEdition:this.state.anneeEdition,
          dateEntree:this.state.dateEntree,
          quantite:this.state.quantite,
          etatActuel:this.state.etatActuel,
          origine:this.state.origine,
          lieuEdition:this.state.lieuEdition,
          description:this.state.description,
           photo:this.state.photo
    
         }
        console.log(ouvrage)
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/update.php',ouvrage)
        .then(res=>{
          console.log('filenamen tsisy='+res.data)
          alert('L\'ouvrage a été modifié avec succès!')
          this.componentDidMount()
        })
        .catch(error=>{
          alert('Erreur de modification!')
        })
      }
      else{
        const ouvrage={
          idOuvrage:this.state.idOuvrage,
          idAuteur:this.state.idAuteurFromColumn1,
          titre:this.state.titre,
          editeur:this.state.editeur,
          nombrePage:this.state.nombrePage,
          prix:this.state.prix,
          idGenre:response.data.idGenre,
          langue:this.state.langue,
          anneeEdition:this.state.anneeEdition,
          dateEntree:this.state.dateEntree,
          quantite:this.state.quantite,
          etatActuel:this.state.etatActuel,
          origine:this.state.origine,
          lieuEdition:this.state.lieuEdition,
          description:this.state.description,
           photo:this.state.filename
    
         }
        console.log(ouvrage)
        Axios.post('https://phpapiserver.herokuapp.com/api/ouvrage/update.php',ouvrage)
        .then(res=>{
          console.log('filenamen mis='+res.data)
          alert('L\'ouvrage a été modifié avec succès!')
          this.componentDidMount()
        })
        .catch(error=>{
          alert('Erreur de modification!')
        })
        //alert('Le nouvel ouvrage a été modifié avec succès!')
        // window.location.href='/dadhboard'
      }
     
     
    })
    .catch(error=>{
      console.log(error)
    })  
  })
  }
  UPLOAD_ENDPOINT = 'https://phpapiserver.herokuapp.com/upload.php';
  async uploadFile(file){
     

    if(file!=null){
      const data = new FormData();
      var originalfilename = file.name.split(".");
      
     
      
      data.append('file',file)
      data.append('upload_preset','insta-clone')
      data.append('cloud_name','ITUnivesity')
      
  
      fetch('https://api.cloudinary.com/v1_1/itunivesity/image/upload',{
          method:'post',
          body:data
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)
        this.setState({filename:data.url})
      })
      .catch(err=>{
        console.log(err)
      })
    }
   else{}
    
  }
  onChangeFile(e) {
    this.setState({file:e.target.files[0]})
   
  }
render(){
  let buttonposterComentaire;
  if(this.state.message!=''){
    buttonposterComentaire=<button onClick={this.posterCommentaire} style={{padding: '5px 8px',border: 'none',backgroundColor:'gray',color:'white',width:'200px'}}>Poster le commentaire</button>
  }
  return (
    <div className="content-wrapper">
    
    <section className="content-header">
      <h1>
      Fiche détaillée de l'ouvrage
       
      </h1>
      <ol className="breadcrumb">
        <li><a href="#"><i className="fa fa-person"></i> A propos de ce membre</a></li>
       
      </ol>
    </section>

   
    <section className="content">
      <div className="row">
        
        <div className="col-md-7">
            <div className="box box-warning">
                 <h4 style={{"textAlign":"center"}}>{this.state.titre}</h4><hr/>
                      
                            <table>
                              <tr>
                                <td>
                                <div  style={{"fontFamily":"Arial","fontSize":"14px","textAlign":"justify"}}>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Auteur:</label> {this.state.nomAuteur}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Titre:</label> {this.state.titre}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Editeur:</label> {this.state.editeur}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Prix:</label> {this.state.prix}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Genre:</label> {this.state.nomGenre}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Langue:</label> {this.state.langue}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Année d'édition:</label> {this.state.anneeEdition}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Date d'entrée:</label> {this.state.dateEntree}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Quantité en stock:</label> {this.state.quantite}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Origine:</label> {this.state.origine}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Lieu d'édition:</label> {this.state.lieuEdition}<br/>
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Nombre de page:</label> {this.state.nombrePage}<br/>
                                
                               </div>
                                </td>
                                <td>
                                <Avatar variant="rounded" src={ decodeURIComponent( decodeURIComponent(this.state.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}  style={{width:"250px",height:'300px',marginLeft:'20px'}}/>
                               
                                </td>
                               
                              </tr>
                            </table>
                            <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Description:</label> <div style={{width:'80%',marginLeft:'20px'}}>&nbsp;&nbsp;{this.state.description}</div><br/>
                               
                            <div id="pdf" >
                                <label style={{"color":"#484a48",marginLeft:'20px'}}>&nbsp;&nbsp;Code barre:</label><br/>
                               
                                  <Barcode displayValue="true"   value={`1OUVR000_${this.props.match.params.id}`}   width="1"  height="60"/>
                                  </div>
                             
                                <Link size="small" style={{marginLeft:'20px'}} to={`/barcodeOuvrage/${this.props.match.params.id}`}><GetAppIcon fontSize="large"/>Visualiser ce code-barres et Expoter(PDF)</Link><br/><hr/>
                            
                            <br/>
                            <div className="row" >
                            <div className='col-md-1'></div>
                            <div className='col-md-8'>
                            <div style={{display:'inline-block'}}>
                      
                     
                  </div>
                 </div>
                    
                </div>
                           
                           
                <br/>
            </div>
            <div className="box box-success">
                        <div className="box-header">
                            <i className="fa fa-comments-o"></i>
                            <h3 className="box-title"> <h4>{this.state.nombre}</h4> </h3>
                            <div className="box-tools pull-right" data-toggle="tooltip" title="Status">
                                <div className="btn-group" data-toggle="btn-toggle" >
                                    <button type="button" className="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i></button>
                                    <button type="button" className="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
                                </div>
                            </div>
                        </div>
                        <div className="box-body chat" id="chat-box">
                        { this.state.commentaires.map(comms=> 
                            <div className="item">
                                <Avatar className="offline"  style={{width:35,height:35}} alt="Remy Sharp" src={`https://res.cloudinary.com/itunivesity/image/upload/v1598908049/upload/${comms.photo}`}  />
                             
                                <p className="message">
                                    <Link  to={`/detailMembre/${comms.idMembre}`} className="name">
                                      <small className="text-muted pull-right"><i className="fa fa-clock-o"></i> {Moment(comms.created).locale("de").format('HH:mm')}</small>
                                      {comms.nom}&nbsp;{comms.prenom}
                                    </Link>
                                    {comms.message}<br/>
                                    <span  className="text-muted" style={{fontSize:'13px'}}>{Moment(comms.created).locale("de").format('Do MMMM YYYY')}</span>
                                </p>
                            </div>
                             )}
                      </div>
                    </div>
        </div>
        <div className="col-md-5">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Modifier cet ouvrage:</h3>
            </div>
            
            <div className="box-body">
                <form onSubmit={this.onSubmit}>
                <div className="form-group">
                      <label>Auteur</label>
                      <input type="text" className="form-control" id="auteur" value={this.state.nomAuteur} onChange={this.onChangeauteur}/>
                </div>
                <div className="form-group">
                      <label>Titre</label>
                      <input type="text" className="form-control" id="titre" value={this.state.titre} onChange={this.onChangetitre}/>
                </div>
                <div className="form-group">
                      <label>Editeur:</label>
                      <input type="text" className="form-control" id="editeur" value={this.state.editeur} onChange={this.onChangeediteur}/>
                </div>
                <div className="form-group">
                      <label>Prix</label>
                      <input type="text" className="form-control" id="prix" value={this.state.prix} onChange={this.onChangeprix}/>
                </div>
                <div class="form-group">
                      <label>Genre</label>
                      <select className="form-control" value={this.state.nomGenre}   onChange={(e)=>this.setState({nomGenre:e.target.value})}>
                         <option  value={this.state.nomGenre}>{this.state.nomGenre}</option>
                         { this.state.genres.map(gr=>
                             <option  value={gr.nomGenre}>{gr.nomGenre}</option>
                          )}

                      </select>
                     
                    </div>
                    <div class="form-group">
                      <label>Langue</label>
                      <input type="text" className="form-control" id="langue" value={this.state.langue} onChange={this.onChangelangue}/>
                 
                    </div>
                <div className="form-group">
                      <label>Année d'édition:</label>
                      <input type="number"  step="1" className="form-control" id="annee" value={this.state.anneeEdition} onChange={this.onChangeannee}/>
                </div>
                <div className="form-group">
                      <label>Date d'entrée:</label>
                      <input type="text" className="form-control" id="dateentree" value={this.state.dateEntree} onChange={this.onChangedateentree}/>
                </div>
                <div className="form-group">
                      <label>Quantité en stock:</label>
                      <input type="text" className="form-control" id="quantite" value={this.state.quantite} onChange={this.onChangequantite}/>
                </div>
                <div className="form-group">
                      <label>Origine:</label>
                      <input type="text" className="form-control" id="origine" value={this.state.origine} onChange={this.onChangeorigine}/>
                </div>
                <div className="form-group">
                      <label>Lieu d'édition:</label>
                      <input type="text" className="form-control" id="lieuedition" value={this.state.lieuEdition} onChange={this.onChangelieu}/>
                </div>
                <div className="form-group">
                      <label>Nombre de page:</label>
                      <input type="number" className="form-control" id="nombrePage" value={this.state.nombrePage} onChange={this.onChangenombrepage}/>
                </div>
                <div className="form-group">  
                      <label >Description</label>  
                      <textarea className="textarea" value={this.state.description} onChange={this.onChangedescription} style={{"width": "100%", "height": "100px", "font-size": "14px", "lineHeight": "18px", "border": "1px solid #dddddd", "padding": "10px"}}></textarea>
                    </div>
                    <div className="form-group">
                      <label for="exampleInputFile">Modifier l'image de l'ouvrage</label>
                      <p>Ancien image:<Avatar variant="rounded"  alt="Remy Sharp" src={ decodeURIComponent( decodeURIComponent(this.state.photo.replace(/&amp;/g, "&")).replace(/&amp;/g, "&"))}  /></p>
                      <input type="file" accept="image/png, image/jpeg" className="form-control" id="exampleInputFile"   onChange={ this.onChangeFile }/>

                    </div>
                <div class="box-footer">
                    
                    <button type="submit" class="btn btn-success">Enregistrer la modification</button>
                  </div>
            </form>
             
            </div>

          </div>
        </div>


      </div>
      <div className="row">
        <div className="col-md-5">
        
        </div>
      </div>
    </section>
  </div>
   
  );
}

}
