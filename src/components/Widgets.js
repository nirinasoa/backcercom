import React from 'react';
import {Link} from 'react-router-dom';

function Widgets() {
  return (
    <div className="content-wrapper">
        
        <section className="content-header">
          <h1>
            A propos de ce site
         
          </h1>
          <ol className="breadcrumb">
          <li><Link to="/dashboard"><i className="fa fa-home"></i> Revenir à la page d'accueil</Link></li>
       
         
          </ol>
        </section>
        <section className="content">
                <h4 className="page-header">
                  
                    <small>Ce système se divise en 2 parties bien distinguées tels que : MONITEURS et MEMBRES</small>
                </h4>
                <div className="row">
                    <div className="col-xs-12">
					<section id='advice'>
            <h2 class='page-header'><span>Moniteurs et membres</span></h2>
            <ul>
              <li><b>Moniteurs:</b> C’est le moniteur ou l’administrateur qui gère les membres: les autres administrateurs eux-mêmes et les membres ou encore lecteurs. En d’autres termes, c’est celui qui fait l’inscription de ses deux côtés. 
              Après l’inscription, le nouveau membre obtient un carte membre généré automatiquement par le système, un numéro d’identification noté par ID, un code-barres.</li>
              <li><b>Membres:</b> Comme il a déjà obtenu son numéro d'identification parlé ci-dessus, alors il peut créer son propre profil avec cela. C’est-à-dire en remplissant juste Id,l'E-mail lors de l'inscription, le nouveau mot de passe s’il n’a pas déjà son compte en tant que membre.</li>
              <li><b>Autres informations:</b>Pas toutes les personnes peuvent entrer dans ce système donc il n’y a pas de lien « Pas de compte ? créer un » dans la page de LOGIN. C’est-à-dire l’inscription est strictement le rôle de l’administrateur. Pourtant la première donnée du moniteur est déjà enregistrée dans la base de données. Ensuite, c’est le premier enregistrement qui fait l’inscription des autres personnes. Et comme il a déjà son compte en premier lieu, alors il saisit juste son login et son mot de passe.</li>
              <li><b>Autres fonctionnalité: </b> Pour l'admin: gestion des ouvrages manuellement ou automatiquement, gestion des membres, et autres. Vous pouvez naviguer dans ce site pour les voir. Pour le
			  membre, il peut voir son profil et permet d’effectuer une recherche souple selon un critère défini. Il peut consulter des ouvrages, de voir les livres les plus lus.
			  </li>
              
            </ul>
  
          </section>
						
                </div>
				</div>

        </section>
</div>
  );
}

export default Widgets;
