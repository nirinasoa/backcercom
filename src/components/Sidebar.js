import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import { grey } from '@material-ui/core/colors';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Avatar from '@material-ui/core/Avatar';
import CameraAltIcon from '@material-ui/icons/CameraAlt';

export default class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.onChangeSearch=this.onChangeSearch.bind(this)
    this.onClick=this.onClick.bind(this)
    this.state={
      search:''
    }
	
    
    }
    onChangeSearch(e){
      //window.find(e.target.value)
      this.setState({
        search:e.target.value
      });
    }
    onClick(){
      window.find(this.state.search)
     
    }
  render(){
  return (
   
    <aside className="main-sidebar">
        <section className="sidebar">
          <div className="user-panel">
            <div className="pull-left image">
              <table>
                <tbody>
                 <tr>
                    <td> <Link style={{color:"white"}} to="/profil"><Avatar style={{width:50,height:50}} src={`https://res.cloudinary.com/itunivesity/image/upload/v1598902272/upload/${sessionStorage.getItem("photo")}`} className="img-circle" alt="User Image"  /></Link></td>
                    <td width="50%"> <Link style={{color:"white"}} to="/profil"><b>&nbsp;{sessionStorage.getItem("nomAdmin")}</b></Link>
                    <br/><Link style={{color:"white"}} to="/profil"><i className="fa fa-circle text-success"></i>&nbsp; En ligne</Link>
                    </td>
                 <td>
                 <div className="pull-left info">
              <p></p>

             
            </div>
                 </td>
                 </tr>
                 </tbody>
              </table>
           
             
            </div>
           
          </div>
           <div className="sidebar-form">
            <div className="input-group">
              <input value={this.state.search} onChange={this.onChangeSearch} type="text"  className="form-control" placeholder="Rechercher..."/>
              <span className="input-group-btn">
                <button onClick={this.onClick}  className="btn btn-flat"><i className="fa fa-search"></i></button>
              </span>
            </div>
          </div>
         <ul className="sidebar-menu">
            <li className="header">MENUS PRINCIPAUX</li>
           
            <li className="active">
              <Link to="/dashboard">
                <i className="fa fa-home"></i> Accueil
              </Link>
            </li>
              
            <li>
            <Link to="/lectureSP">
                <i className="fa fa-book"></i> <span>Lecture sur place</span> 
              </Link>
            </li>
            <li>
            <Link to="/barecodereader">
                <i className="fa fa-camera"></i> <span>Scanner code-barres</span> 
              </Link>
            </li>
            <li>
               <Link to="/booksearch">
                <i className="fa fa-search"></i> <span>Recherche des livres</span> 
              </Link>
            </li>
            
            <li>
               <Link to="/membre">
                <i className="fa fa-group"></i> <span>Membres</span> 
              </Link>
            </li>
            <li>
              <Link to="/admin">
                <i className="fa fa-group"></i> <span>ADMIN Membres</span> 
              </Link>
            </li>
            <li>
              <Link to="/UM">
                <i className="fa fa-group"></i> <span>Utilisateurs Membres</span> 
              </Link>
            </li>
            <li>
              <Link to="/statistique">
                <i className="fa fa-pie-chart"></i> <span>Statistiques</span> 
              </Link>
            </li>
            <li>
               <Link to="/genre">
                <i className="fa fa-book"></i> <span>Genres des ouvrages</span> 
              </Link>
            </li>
            <li>
               <Link to="/auteur">
                <i className="fa fa-user"></i> <span>Auteurs des ouvrages</span> 
              </Link>
            </li>
            <li  >
            <Accordion style={{ "backgroundColor":"#222d32","color":"#b8c7ce"}}>
        <AccordionSummary 
          expandIcon={<ExpandMoreIcon  style={{"color":"#b8c7ce"}}/>}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography  ><i className="fa fa-circle-o"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style={{"fontFamily":"Arial","fontSize":"13px"}}>Ajouter un(e)</span></Typography>
        </AccordionSummary>
        <AccordionDetails>
        <ul >
                <li><Link to="/membre"> Membre</Link></li>
                <li><Link to="/admin"> Moniteur ou Admin</Link></li>
                <li><Link to="/ouvrage"> Ouvrage</Link></li>
                <li><Link to="/lectureSP"> Lecture sur place</Link></li>
                <li><Link to="/genre"> Genre</Link></li>
                <li><Link to="/auteur"> Auteur</Link></li>
      
              </ul>
        </AccordionDetails>
      </Accordion>
            </li>
            <li>
              <Link to="/widgets">
                <i className="fa fa-info-circle"></i> <span>Autre</span> 
              </Link>
            </li>
            
            
               
          </ul>
        </section>
        
      </aside>
  );
}
}


